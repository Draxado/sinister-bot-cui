﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Windows.Forms;
using Sinister_Bot;
using Sinister_Bot.Properties;

namespace CurrencySystem
{
    public class ViewerDB //functions as interface between db and bot
    {
        public static string name = "Viewerlist.sqlite";
        public static string path = Path.Combine(Bot.app, name);
        public List<Viewer> list = new List<Viewer>();
        private static SQLiteConnection db = new SQLiteConnection("Data Source = " + path);

        //constructor for creating a new database file
        public ViewerDB()
        {
            SQLiteConnection.CreateFile(name);
            db.Open();
            string table = "create table Viewerlist (name varchar(30), currency int, rank varchar(30), status int)";
            SQLiteCommand c = new SQLiteCommand(table, db);
            c.ExecuteNonQuery();
            db.Close();
        }

        //constructor to read from an existing database file
        public ViewerDB(string p)
        {
            db.Open();
            string readData = "select * from Viewerlist";
            SQLiteCommand copyIntoBot = new SQLiteCommand(readData, db);
            SQLiteDataReader r = copyIntoBot.ExecuteReader();
            while (r.Read())
            {
                Viewer v = new Viewer(r.GetString(0), r.GetInt32(1), r.GetString(2), r.GetInt32(3));
                //check if rank and currency are still valid/correct
                if (v.currency < 0)
                {
                    MessageBox.Show("Die gesammelten {} für {v.username} sind nicht mehr gültig (< 0)! Bitte überprüfe dies und ändere diese evtl. selber!");
                    //TODO frmEditViewerpoints
                }
                //TODO correct rank
                list.Add(v); //db
            }
            db.Close();
        }

        //adds a new viewer to the db
        public void newViewer(Viewer v)
        {
            db.Open();
            SQLiteCommand register = new SQLiteCommand(db);
            register.CommandText = "insert into Viewerlist (name, currency, rank, status) values (@name, @currency, @rank, @status)";
            register.Prepare();
            register.Parameters.AddWithValue("@name", v.name);
            register.Parameters.AddWithValue("@currency", v.currency);
            register.Parameters.AddWithValue("@rank", v.rank);
            register.Parameters.AddWithValue("@status", v.status);
            register.ExecuteNonQuery();
            db.Close();
        }

        //updates the currency for a user, amount can be negative
        public void updateCurrency(Viewer user, int amount)
        {
            lock (user)
            {
                db.Open();
                string select = "select currency from Viewerlist where name='" + user.name + "'";
                SQLiteCommand sViewer = new SQLiteCommand(select, db);
                SQLiteDataReader r = sViewer.ExecuteReader();
                r.Read();
                int curr = r.GetInt32(0) + amount;
                string update = "update Viewerlist set currency='" + curr + "' where name='" + user.name + "'";
                SQLiteCommand c = new SQLiteCommand(update, db);
                c.ExecuteNonQuery();
                db.Close();
                user.currency = curr;
                updateRank(user);
            }
        }

        public static void updateRank(Viewer user)
        {
            switch (frmMain.dgvAscending)
            {
                case true:
                    int lowestRankAscending = 0;
                    int highestRankAscending = Bot.main.dgvViewerankings.RowCount - 1;
                    if (user.currency < Convert.ToInt32(Bot.main.dgvViewerankings[2, lowestRankAscending].Value)) //lowest rank
                    {
                        if (user.rank != Bot.main.dgvViewerankings[0, lowestRankAscending].Value.ToString()) //rank found, has rank changed?
                        {
                            changeViewerrankDB(user, lowestRankAscending, user.rank, Bot.main.dgvViewerankings[0, lowestRankAscending].Value.ToString()); //update rank information
                        }
                        return;
                    }
                    else if (user.currency >= Convert.ToInt32(Bot.main.dgvViewerankings[2, highestRankAscending].Value)) //highest rank
                    {
                        if (user.rank != Bot.main.dgvViewerankings[0, highestRankAscending].Value.ToString()) //rank found, has rank changed?
                        {
                            changeViewerrankDB(user, highestRankAscending, user.rank, Bot.main.dgvViewerankings[0, highestRankAscending].Value.ToString()); //update rank information
                        }
                        return;
                    }
                    else //other rank
                    {
                        for (int rank = 1; rank < highestRankAscending; rank++)
                        {
                            if (user.currency >= Convert.ToInt32(Bot.main.dgvViewerankings[2, rank].Value)) //currency higher than the minimum of rank?
                            {
                                if (user.currency < Convert.ToInt32(Bot.main.dgvViewerankings[2, rank + 1].Value)) //currency lower than minimun of next rank?
                                {
                                    if (user.rank != Bot.main.dgvViewerankings[0, rank].Value.ToString()) //rank found, has rank changed?
                                    {
                                        changeViewerrankDB(user, rank, user.rank, Bot.main.dgvViewerankings[0, rank].Value.ToString()); //update rank information
                                    }
                                    return;
                                }
                            }
                        }
                    }
                    break;
                case false:
                    int lowestRankDescending = Bot.main.dgvViewerankings.RowCount - 1;
                    int highestRankDescending = 0;
                    if (user.currency >= Convert.ToInt32(Bot.main.dgvViewerankings[2, highestRankDescending].Value)) //highest rank
                    {
                        if (user.rank != Bot.main.dgvViewerankings[2, highestRankDescending].Value.ToString()) //rank found, has rank changed?
                        {
                            changeViewerrankDB(user, highestRankDescending, user.rank, Bot.main.dgvViewerankings[0, highestRankDescending].Value.ToString()); //update rank information
                        }
                        return;
                    }
                    else if (user.currency < Convert.ToInt32(Bot.main.dgvViewerankings[2, lowestRankDescending].Value)) //lowest rank
                    {
                        if (user.rank != Bot.main.dgvViewerankings[0, lowestRankDescending].Value.ToString()) //rank found, has rank changed?
                        {
                            changeViewerrankDB(user, lowestRankDescending, user.rank, Bot.main.dgvViewerankings[0, lowestRankDescending].Value.ToString()); //update rank information
                        }
                        return;
                    }
                    else //other rank
                    {
                        for (int rank = 1; rank < lowestRankDescending; rank++)
                        {
                            if (user.currency < Convert.ToInt32(Bot.main.dgvViewerankings[2, rank].Value)) //currency lower than the minimum of rank?
                            {
                                if (user.currency >= Convert.ToInt32(Bot.main.dgvViewerankings[2, rank + 1].Value)) //currency higher than minimun of next rank?
                                {
                                    if (user.rank != Bot.main.dgvViewerankings[0, rank].Value.ToString()) //rank found, has rank changed?
                                    {
                                        changeViewerrankDB(user, rank, user.rank, Bot.main.dgvViewerankings[0, rank].Value.ToString()); //update rank information
                                    }
                                    return;
                                }
                            }
                        }
                    }
                    break;
                default:
                    Console.WriteLine("Fehler beim Aktualisiern des Rang -> updateRank(Viewer user)");
                    break;
            }
        }

        private static void changeViewerrankDB(Viewer user, int row, string oldrank, string newrank)
        {
            db.Open();
            string rankUpdate = "update Viewerlist set rank='" + newrank + "' where name='" + user.name + "'";
            SQLiteCommand cmd = new SQLiteCommand(rankUpdate, db);
            cmd.ExecuteNonQuery();
            db.Close();

            updateRankInformationBot(user, row, oldrank);
        }

        private static void updateRankInformationBot(Viewer user, int row, string oldrank)
        {
            for (int i = 0; i < Bot.main.dgvViewerankings.RowCount; i++)
            {
                if (Bot.main.dgvViewerankings.Rows[i].Cells[0].Value.ToString().Equals(oldrank))
                {
                    Bot.main.dgvViewerankings.Rows[i].Cells[2].Value = Convert.ToInt32(Bot.main.dgvViewerankings.Rows[i].Cells[2].Value) - 1; //decrease count for old rank
                }
                if (i == row)
                {
                    Bot.main.dgvViewerankings.Rows[i].Cells[2].Value = Convert.ToInt32(Bot.main.dgvViewerankings.Rows[i].Cells[2].Value) + 1; //increase count for new rank
                    user.rank = Bot.main.dgvViewerankings.Rows[row].Cells[0].Value.ToString();
                }
            }
            //settings
            Control.ControlCollection coll = Bot.main.tpRewardsystem.Controls;
            string bs = "";
            foreach (Control r in coll)
            {
                if (r is DataGridView)
                {
                    DataGridView d = (DataGridView)r;
                    int dCount = d.RowCount;
                    for (int a = 0; a < dCount; a++)
                    {
                        bs += $"{d[0, a].Value}|{d[1, a].Value}|{d[2, a].Value}#";
                    }
                    bs = (bs.Length >= 1) ? bs.Substring(0, bs.Length - 1) : "";
                    Settings.Default.gbViewerrankings = bs;
                    Settings.Default.Save();
                    if (!frmMain.alreadySaveViewerrankings)
                    {
                        frmMain.alreadySaveViewerrankings = true;
                    }
                    break;
                }
            }
        }

        //check if rank is valid & correct and will update the rank table -> needed cause streamer can edit db outside of the bot
        public static void validateViewerRanks()
        {
            foreach (Viewer user in Bot.viewer.list)
            {
                for (int i = 0; i < Bot.main.dgvViewerankings.RowCount; i++)
                {
                    if (frmMain.dgvAscending)
                    {
                        //row with rank found
                        if (Bot.main.dgvViewerankings[i, 0].Value.ToString().Equals(user.rank))
                        {
                            //row is the last on -> highest achievable rank & needed currency 
                            if (i == Bot.main.dgvViewerankings.RowCount - 1)
                            {
                                if (user.currency >= Convert.ToInt32(Bot.main.dgvViewerankings[2, i].Value))
                                {
                                    Bot.main.dgvViewerankings[i, 2].Value = Convert.ToInt32(Bot.main.dgvViewerankings[2, i].Value) + 1;
                                }
                                else
                                {
                                    updateRank(user);
                                }
                            }
                            //rank & needed/max currency match 
                            else if (user.currency >= Convert.ToInt32(Bot.main.dgvViewerankings[2, i].Value) && user.currency < Convert.ToInt32(Bot.main.dgvViewerankings[2, i + 1].Value))
                            {
                                Bot.main.dgvViewerankings[i, 2].Value = Convert.ToInt32(Bot.main.dgvViewerankings[2, i].Value) + 1;
                            }
                            else
                            {
                                updateRank(user);
                            }
                        }
                    }
                }
            }
        }
        //updates the status from a viewer (normal, sub, mod,...)
        public void updateStatus(string user, int status)
        {
            System.Threading.Thread.Sleep(1000);
            db.Open();
            lock (user)
            {
                string update = "update Viewerlist set status='" + status + "' where name='" + user + "'";
                SQLiteCommand upStatus = new SQLiteCommand(update, db);
                upStatus.ExecuteNonQuery();
            }
            db.Close();
        }
    }
}