﻿using Sinister_Bot;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Windows.Forms;

namespace CommandSystem
	{
	public class CommandDB
		{
		public static string name = "Commands.sqlite";
		public static string path = Path.Combine(Bot.app, name);
		public List<Command> list = new List<Command>();
		private SQLiteConnection db = new SQLiteConnection("Data Source = " + path);

		public CommandDB()
			{
			SQLiteConnection.CreateFile(name);
			db.Open();
			string s = "create table Commands (name varchar(30), message varchar(30), editable bool, executionerlvl int, alias varchar(50), cooldown int)";
			SQLiteCommand c = new SQLiteCommand(s, db);
			c.ExecuteNonQuery();
			db.Close();
			}

		public CommandDB(string p)
			{
			db.Open();
			string s = "select * from Commands";
			SQLiteCommand c = new SQLiteCommand(s, db);
			SQLiteDataReader reader = c.ExecuteReader();
			while (reader.Read())
				{
				string[] alias = { "" };
				if (reader.GetString(4) != null)
					{
					alias = reader.GetString(4).Split(',');
					}
				for (int i = 0; i < alias.Length; i++)
					{
					alias[i] = alias[i].Trim();
					}
				Command t = new Command(reader.GetString(0), reader.GetString(1), reader.GetBoolean(2), reader.GetInt32(3), alias, reader.GetInt32(5));
				list.Add(t);
				}
			db.Close();
			}

		////////////////////////////////////////////////////////////////////////////////////////////////
		////////							(Mod) Command Actions	 						    ////////
		////////////////////////////////////////////////////////////////////////////////////////////////

		#region (Mod) Command Actions

		#region command

		public void createCommand(string name, bool editable, int executionerlvl, string message, string[] alias, int cooldown)
			{
			bool newCommand = true;
			foreach (CommandPreset pre in Bot.commandPre.listPre)
				{
				if (pre.name.Equals(name))
					{
					newCommand = false;
					break;
					}
				}
			if (newCommand)
				{
				foreach (Command search in Bot.command.list)
					{
					if (search.name.Equals(name))
						{
						newCommand = false;
						break;
						}
					}
				}
			if (newCommand)
				{
				string listedAlias = "";
				foreach (string a in alias)
					{
					if (a != "")
						{
						listedAlias += a.Trim() + ", ";
						}
					}
				listedAlias = (listedAlias.Length > 2) ? listedAlias.Remove(listedAlias.Length - 2) : "";

				db.Open();
				SQLiteCommand newCmd = new SQLiteCommand(db);
				newCmd.CommandText = "insert into Commands (name, message, editable, executionerlvl, alias, cooldown) values (@name, @message, @editable, @executionerlvl, @alias, @cooldown)";
				newCmd.Prepare();
				newCmd.Parameters.AddWithValue("@name", name);
				newCmd.Parameters.AddWithValue("@message", message);
				newCmd.Parameters.AddWithValue("@editable", editable);
				newCmd.Parameters.AddWithValue("@executionerlvl", executionerlvl);
				newCmd.Parameters.AddWithValue("@alias", listedAlias);
				newCmd.Parameters.AddWithValue("@cooldown", cooldown);
				newCmd.ExecuteNonQuery();
				db.Close();

				Command c = new Command(name, message, editable, executionerlvl, alias, cooldown);
				Bot.command.list.Add(c);
				Bot.irc.sendChatMessage("Der Command wurde erstellt: " + c.name + ", " + c.message);
				CommandPresetDB.updateCommandlistCommand();
				Bot.main.updateCommandlistdisplay();
				}
			else
				{
				Bot.irc.sendChatMessage("Ein Command mit diesen Namen existiert bereits.Bitte wähle einen anderen.");
				Bot.main.failed = true;
				}
			}

		public void deleteCommand(Command c)
			{
			bool existCommand = false;
			foreach (Command search in Bot.command.list)
				{
				if (c.name.Equals(search.name))
					{
					existCommand = true;
					break;
					}
				}
			if (existCommand)
				{
				db.Open();
				string editable = "select editable from Commands where name='" + c.name + "'";
				SQLiteCommand check = new SQLiteCommand(editable, db);
				SQLiteDataReader reader = check.ExecuteReader();
				reader.Read();
				if (1 == reader.GetInt32(0))
					{
					string delete = "delete from Commands where name='" + c.name + "'";
					SQLiteCommand remove = new SQLiteCommand(delete, db);
					remove.ExecuteNonQuery();
					Bot.irc.sendChatMessage($"Der Command {c.name} wurde gelöscht.");
					Bot.command.list.Remove(c);
					}
				else
					{
					Bot.irc.sendChatMessage("Du kannst diesen Command nicht löschen!");
					}
				db.Close();
				CommandPresetDB.updateCommandlistCommand();
				Bot.main.updateCommandlistdisplay();
				}
			else
				{
				Bot.irc.sendChatMessage("Der Command, den du löschen möchtest exisitiert nicht!");
				}
			}

		public void editCommand(string name, string message)
			{
			bool existCommand = false;
			bool presetCmd = false;
			foreach (CommandPreset pre in Bot.commandPre.listPre)
				{
				if (pre.name.Equals(name))
					{
					Bot.irc.sendChatMessage("Du kannst diesen Command nicht ändern!");
					existCommand = true;
					presetCmd = true;
					break;
					}
				}
			if (!existCommand)
				{
				foreach (Command search in Bot.command.list)
					{
					if (search.name.Equals(name))
						{
						existCommand = true;
						break;
						}
					}
				}
			if (existCommand && !presetCmd)
				{
				db.Open();
				string editable = "select editable from Commands where name='" + name + "'";
				SQLiteCommand check = new SQLiteCommand(editable, db);
				SQLiteDataReader reader = check.ExecuteReader();
				reader.Read();
				if (1 == reader.GetInt32(0))
					{
					SQLiteCommand edit = new SQLiteCommand(db);
					edit.CommandText = "update Commands set message='" + message + "' where name='" + name + "'";
					edit.ExecuteNonQuery();
					foreach (Command search in Bot.command.list)
						{
						if (name.Equals(search.name))
							{
							search.message = message;
							Bot.irc.sendChatMessage($"Die Commandnachricht von {name} wurde geändert. (von: {search.name} nach: {search.message})");
							CommandPresetDB.updateCommandlistCommand();
							Bot.main.updateCommandlistdisplay();
							}
						}
					}
				else
					{
					Bot.irc.sendChatMessage("Du kannst diesen Command nicht ändern!");
					}
				}
			else if (!presetCmd)
				{
				Bot.irc.sendChatMessage("Dieser Command existiert nicht!");
				}
			db.Close();
			}

		public void editCommandName(string oldName, string newName)
			{
			bool existCommand = false;
			bool presetCmd = false;
			foreach (CommandPreset preCheck in Bot.commandPre.listPre)
				{
				if (preCheck.name == newName)
					{
					Bot.irc.sendChatMessage("Ein Command mit diesen Namen existiert bereits. Bitte wähle einen anderen");
					Bot.main.failed = true;
					return;
					}
				}
			foreach (Command check in Bot.command.list)
				{
				if (check.name == newName)
					{
					Bot.irc.sendChatMessage("Ein Command mit diesen Namen existiert bereits. Bitte wähle einen anderen");
					Bot.main.failed = true;
					return;
					}
				}
			foreach (CommandPreset pre in Bot.commandPre.listPre)
				{
				if (pre.name.Equals(oldName))
					{
					Bot.irc.sendChatMessage("Du kannst diesen Command nicht ändern!");
					existCommand = true;
					presetCmd = true;
					break;
					}
				}
			if (!existCommand)
				{
				foreach (Command search in Bot.command.list)
					{
					if (search.name.Equals(oldName))
						{
						existCommand = true;
						break;
						}
					}
				}
			if (existCommand && !presetCmd)
				{
				db.Open();
				string editable = "select editable from Commands where name='" + oldName + "'";
				SQLiteCommand check = new SQLiteCommand(editable, db);
				SQLiteDataReader reader = check.ExecuteReader();
				reader.Read();
				bool edit = Convert.ToBoolean(reader.GetInt32(0));
				if (edit)
					{
					SQLiteCommand editCmd = new SQLiteCommand(db);
					editCmd.CommandText = "update Commands set name='" + newName + "' where name='" + oldName + "'";
					editCmd.ExecuteNonQuery();
					foreach (Command search in Bot.command.list)
						{
						if (oldName.Equals(search.name))
							{
							search.name = newName;
							Bot.irc.sendChatMessage($"Der Command {oldName} wurde zu {newName} umbenannt.");
							CommandPresetDB.updateCommandlistCommand();
							Bot.main.updateCommandlistdisplay();
							}
						}
					}
				else
					{
					Bot.irc.sendChatMessage("Du kannst diesen Command nicht ändern!");
					}
				db.Close();
				}
			else if (!presetCmd)
				{
				Bot.irc.sendChatMessage("Der Command, den du ändern möchtest exisitiert nicht!");
				}
			}

		public void editCommand(Command editedCommand, string n, bool chk, int lvl, string m, string[] aliasFromTb, int cd)
			{
			List<string> aliasList = new List<string>();
			foreach (string s in aliasFromTb)
				{
				if (s != "")
					{
					aliasList.Add(s.Trim());
					}
				}
			string[] aliasArray = { "" };
			string alias = "";
			if (aliasList.Count > 0)
				{
				aliasArray = aliasList.ToArray();
				foreach (string a in aliasArray)
					{
					alias += a + ", ";
					}
				alias = alias.Remove(alias.LastIndexOf(','));
				}

			foreach (Command check in Bot.command.list)
				{
				if (check != editedCommand)
					{
					if (check.name == n)
						{
						MessageBox.Show("Ein Command mit diesen Namen existiert bereits. Bitte wähle einen anderen.");
						Bot.main.failed = true;
						return;
						}
					foreach (string a in check.alias)
						{
						foreach (string newalias in aliasArray)
							{
							if (newalias.Trim() == a)
								{
								MessageBox.Show($"Ein Command hat bereits den Alias \"{a}\". Bitte wähle einen anderen.");
								Bot.main.failed = true;
								return;
								}
							}
						}
					}
				}
			foreach (CommandPreset checkPre in Bot.commandPre.listPre)
				{
				if (checkPre.name == n)
					{
					MessageBox.Show("Ein Command mit diesen Namen existiert bereits. Bitte wähle einen anderen.");
					Bot.main.failed = true;
					return;
					}
				foreach (string aPre in checkPre.alias)
					{
					foreach (string newAliasPre in aliasArray)
						{
						if (newAliasPre.Trim() == aPre)
							{
							MessageBox.Show($"Ein Command hat bereits den Alias \"{aPre}\". Bitte wähle einen anderen.");
							Bot.main.failed = true;
							return;
							}
						}
					}
				}

			foreach (Command e in Bot.command.list)
				{
				if (e == editedCommand)
					{
					e.name = n;
					e.editable = (chk) ? true : false;
					e.executionerLevel = lvl;
					e.message = m;
					e.alias = aliasArray;
					e.cooldown = cd;
					Bot.irc.sendChatMessage($"Der Command {e.name} wurde {((e.name == n) ? "bearbeitet" : $"{n} umbenannt")}.");
					break;
					}
				}

			db.Open();
			string editName = $"update Commands set name='{n}' where name='{editedCommand.name}'";
			string editEditable = $"update Commands set editable='{Convert.ToInt32(chk)}' where name='{n}'";
			string editLvl = $"update Commands set executionerlvl='{lvl}' where name='{n}'";
			string editMessage = $"update Commands set message='{m}' where name='{n}'";
			string editAlias = $"update Commands set alias='{alias}' where name='{n}'";
			string editCd = $"update Commands set cooldown='{cd}' where name='{n}'";

			SQLiteCommand editNameCmd = new SQLiteCommand(editName, db);
			editNameCmd.ExecuteNonQuery();
			SQLiteCommand editEditableCmd = new SQLiteCommand(editEditable, db);
			editEditableCmd.ExecuteNonQuery();
			SQLiteCommand editLvlCmd = new SQLiteCommand(editLvl, db);
			editLvlCmd.ExecuteNonQuery();
			SQLiteCommand editMessageCmd = new SQLiteCommand(editMessage, db);
			editMessageCmd.ExecuteNonQuery();
			SQLiteCommand editAliasCmd = new SQLiteCommand(editAlias, db);
			editAliasCmd.ExecuteNonQuery();
			SQLiteCommand editCdCmd = new SQLiteCommand(editCd, db);
			editCdCmd.ExecuteNonQuery();
			db.Close();

			CommandPresetDB.updateCommandlistCommand();
			Bot.main.updateCommandlistdisplay();
			}

		#endregion command

		#region alias

		public void addAlias(string commandname, string alias)
			{
			bool commandFound = false;
			foreach (CommandPreset checkAliasPre in Bot.commandPre.listPre)
				{
				for (int i = 0; i < checkAliasPre.alias.Length; i++)
					{
					if (checkAliasPre.alias[i] == alias)
						{
						Bot.irc.sendChatMessage("Dieser Alias wird bereits verwendet. Bitte wähle einen anderen.");
						return;
						}
					}
				}
			foreach (Command checkAlias in Bot.command.list)
				{
				for (int i = 0; i < checkAlias.alias.Length; i++)
					{
					if (checkAlias.alias[i] == alias)
						{
						Bot.irc.sendChatMessage("Dieser Alias wird bereits verwendet. Bitte wähle einen anderen.");
						return;
						}
					}
				}
			foreach (CommandPreset pre in Bot.commandPre.listPre)
				{
				if (pre.name.Equals(commandname))
					{
					Bot.commandPre.addAlias(commandname, alias);
					CommandPresetDB.updateCommandlistCommand();
					commandFound = true;
					return;
					}
				}
			foreach (Command c in Bot.command.list)
				{
				if (c.name.Equals(commandname))
					{
					commandFound = true;
					db.Open();
					string editable = "select editable, alias from Commands where name='" + commandname + "'";
					SQLiteCommand check = new SQLiteCommand(editable, db);
					SQLiteDataReader reader = check.ExecuteReader();
					reader.Read();
					if (1 == reader.GetInt32(0))
						{
						string newAlias = reader.GetString(1);
						if (reader.GetString(1).Length > 0)
							{
							newAlias += ", " + alias;
							}
						else
							{
							newAlias = alias;
							}
						SQLiteCommand edit = new SQLiteCommand(db);
						edit.CommandText = "update Commands set alias='" + newAlias + "' where name='" + commandname + "'";
						edit.ExecuteNonQuery();
						c.alias = newAlias.Split(',');
						for (int i = 0; i < c.alias.Length; i++)
							{
							c.alias[i] = c.alias[i].Trim();
							}
						db.Close();
						break;
						}
					else
						{
						db.Close();
						Bot.irc.sendChatMessage("Du kannst diesen Command nicht ändern!");
						return;
						}
					}
				}
			if (!commandFound)
				{
				Bot.irc.sendChatMessage("Dieser Command existiert nicht!");
				}
			else
				{
				Bot.irc.sendChatMessage($"Der Alias \"{alias}\" wurde dem Command \"{commandname}\" hinzugefügt");
				CommandPresetDB.updateCommandlistCommand();
				Bot.main.updateCommandlistdisplay();
				}
			}

		public void editAlias(string commandname, string oldAlias, string newAlias)
			{
			bool commandFound = false;
			foreach (CommandPreset checkAliasPre in Bot.commandPre.listPre)
				{
				for (int i = 0; i < checkAliasPre.alias.Length; i++)
					{
					if (checkAliasPre.alias[i] == newAlias)
						{
						Bot.irc.sendChatMessage("Dieser Alias wird bereits verwendet. Bitte wähle einen anderen.");
						return;
						}
					}
				}
			foreach (Command checkAlias in Bot.command.list)
				{
				for (int i = 0; i < checkAlias.alias.Length; i++)
					{
					if (checkAlias.alias[i] == newAlias)
						{
						Bot.irc.sendChatMessage("Dieser Alias wird bereits verwendet. Bitte wähle einen anderen.");
						return;
						}
					}
				}
			foreach (CommandPreset pre in Bot.commandPre.listPre)
				{
				if (pre.name.Equals(commandname))
					{
					Bot.commandPre.editAlias(commandname, oldAlias, newAlias);
					commandFound = true;
					CommandPresetDB.updateCommandlistCommand();
					return;
					}
				}
			foreach (Command c in Bot.command.list)
				{
				if (c.name.Equals(commandname))
					{
					db.Open();
					string editable = "select editable, alias from Commands where name='" + commandname + "'";
					SQLiteCommand check = new SQLiteCommand(editable, db);
					SQLiteDataReader reader = check.ExecuteReader();
					reader.Read();
					if (1 == reader.GetInt32(0))
						{
						string alias = reader.GetString(1);
						alias = alias.Replace(oldAlias, newAlias);
						string[] editedAlias = { "" };
						if (alias.Contains(","))
							{
							editedAlias = alias.Split(',');
							for (int i = 0; i < editedAlias.Length; i++)
								{
								editedAlias[i] = editedAlias[i].Trim();
								}
							}
						else
							{
							editedAlias[0] = alias;
							}
						c.alias = editedAlias;
						SQLiteCommand edit = new SQLiteCommand(db);
						edit.CommandText = "update Commands set alias='" + alias + "' where name='" + commandname + "'";
						edit.ExecuteNonQuery();
						Bot.irc.sendChatMessage($"Der Alias \"{oldAlias}\" vom Command \"{commandname}\" wurde zu \"{newAlias}\" geändert");
						commandFound = true;
						CommandPresetDB.updateCommandlistCommand();
						Bot.main.updateCommandlistdisplay();
						db.Close();
						break;
						}
					else
						{
						db.Close();
						Bot.irc.sendChatMessage("Du kannst diesen Command nicht ändern!");
						return;
						}
					}
				}
			if (!commandFound)
				{
				Bot.irc.sendChatMessage("Dieser Command existiert nicht!");
				}
			}

		public void deleteAlias(string commandname, string alias)
			{
			bool commandFound = false;
			foreach (CommandPreset pre in Bot.commandPre.listPre)
				{
				if (pre.name.Equals(commandname))
					{
					Bot.commandPre.delAlias(commandname, alias);
					commandFound = true;
					CommandPresetDB.updateCommandlistCommand();
					return;
					}
				}
			foreach (Command c in Bot.command.list)
				{
				if (c.name.Equals(commandname))
					{
					db.Open();
					string delete = "select editable, alias from Commands where name='" + commandname + "'";
					SQLiteCommand check = new SQLiteCommand(delete, db);
					SQLiteDataReader reader = check.ExecuteReader();
					reader.Read();
					if (reader.GetString(1).Length == 0)
						{
						return;
						}
					if (1 == reader.GetInt32(0))
						{
						string[] nAlias = { "" };
						string newAlias = reader.GetString(1);
						if (newAlias.Contains(","))
							{
							nAlias = newAlias.Replace(" ", "").Split(',');
							for (int i = 0; i < nAlias.Length; i++)
								{
								if (nAlias[i] == alias)
									{
									nAlias[i] = "";
									}
								}
							newAlias = "";
							foreach (string a in nAlias)
								{
								if (a != "")
									{
									newAlias += a + ", ";
									}
								}
							newAlias = newAlias.Remove(newAlias.Length - 2).Trim();
							}
						else
							{
							if (newAlias.Trim() == alias)
								{
								newAlias = "";
								}
							else
								{
								return;
								}
							}
						SQLiteCommand edit = new SQLiteCommand(db);
						edit.CommandText = "update Commands set alias='" + newAlias + "' where name='" + commandname + "'";
						edit.ExecuteNonQuery();
						commandFound = true;
						break;
						}
					else
						{
						Bot.irc.sendChatMessage("Du kannst diesen Command nicht ändern!");
						return;
						}
					}
				}
			if (!commandFound)
				{
				Bot.irc.sendChatMessage("Dieser Command existiert nicht!");
				}
			else
				{
				Bot.irc.sendChatMessage($"Der Alias \"{alias}\" wurde vom Command \"{commandname}\" entfernt");
				CommandPresetDB.updateCommandlistCommand();
				Bot.main.updateCommandlistdisplay();
				}
			}

		#endregion alias

		#endregion (Mod) Command Actions
		}
	}