﻿using Sinister_Bot;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System;
using System.Windows.Forms;

namespace CommandSystem
	{

	public class CommandPresetDB
		{
		public static string name = "PresetCommands.sqlite";
		public static string path = Path.Combine(Bot.app, name);
		public List<CommandPreset> listPre = new List<CommandPreset>();
		public static SQLiteConnection db = new SQLiteConnection("Data Source =" + path);

		public CommandPresetDB()
			{
			SQLiteConnection.CreateFile(name);
			db.Open();
			string s = "create table Presetcommands (name varchar(30), message varchar(30), alias varchar(50))";
			SQLiteCommand c = new SQLiteCommand(s, db);
			c.ExecuteNonQuery();
			db.Close();
			}

		public CommandPresetDB(string p)
			{
			db.Open();
			string s = "select * from PresetCommands";
			SQLiteCommand c = new SQLiteCommand(s, db);
			SQLiteDataReader reader = c.ExecuteReader();
			while (reader.Read())
				{
				string[] alias = { "" };
				if (reader.GetString(2) != null)
					{
					alias = reader.GetString(2).Split(',');
					}
				for (int i = 0; i < alias.Length; i++)
					{
					alias[i] = alias[i].Trim();
					}
				CommandPreset preC = new CommandPreset(reader.GetString(0), reader.GetString(1), alias);
				listPre.Add(preC);
				}
			db.Close();
			}

		public static CommandPreset findCommand(string name)
			{
			foreach (CommandPreset pre in Bot.commandPre.listPre)
				{
				if (pre.name.Equals(name))
					{
					return pre;
					}
				}
			return null;
			}

		public static void updateCurrencyCommandName(string commandName)
			{
			CommandPreset temp = findCommand(commandName);
			if (temp != null)
				{
				db.Open();
				string update = "update PresetCommands set name='" + commandName + "' where name='" + temp.name + "'";
				SQLiteCommand cmd = new SQLiteCommand(update, db);
				cmd.ExecuteNonQuery();
				temp.name = commandName;
				updateCommandlistCommand();
				}
			else
				{
				MessageBox.Show("Etwas lief schief beim aktualisieren des Währungscommands!");
				}
			}

		public static void updateCommandlistCommand()
			{
			string[] oldAlias = null;
			CommandPreset find = findCommand("commandlist");
			if (find != null)
				{
				oldAlias = find.alias != null ? find.alias : null;
				}
			Bot.commandPre.listPre.Remove(find);
			string updatedList = "commandlist";
			if (oldAlias != null && oldAlias[0] != "")
				{
				updatedList += " (alias: ";
				foreach (string oA in oldAlias)
					{
					updatedList += oA + ", ";
					}
				updatedList = updatedList.Remove(updatedList.Length - 2);
				updatedList += ")";
				}
			updatedList += " / ";
			updatedList += CommandPreset.list();
			updatedList = updatedList.Replace("!", "");
			Bot.commandPre.listPre.Add(new CommandPreset("commandlist", updatedList, oldAlias));

			SQLiteConnection updateConn = new SQLiteConnection("Data Source =" + path);
			updateConn.Open();
			string sqlUpdate = "update PresetCommands set message='" + updatedList + "' where name='commandlist'";
			SQLiteCommand update = new SQLiteCommand(sqlUpdate, updateConn);
			update.ExecuteNonQuery();
			updateConn.Close();
			}

		public void createPresetCommand(string name, string message, string[] alias)
			{
			string allAlias = "";
			if (alias != null)
				{
				foreach (string a in alias)
					{
					allAlias += a + ", ";
					}
				allAlias = allAlias.Remove(allAlias.Length - 2);
				}
			db.Open();
			SQLiteCommand newPresetCmd = new SQLiteCommand(db);
			newPresetCmd.CommandText = "insert into PresetCommands (name, message, alias) values (@name, @message, @alias)";
			newPresetCmd.Prepare();
			newPresetCmd.Parameters.AddWithValue("@name", name);
			newPresetCmd.Parameters.AddWithValue("@message", message);
			newPresetCmd.Parameters.AddWithValue("@alias", allAlias);
			newPresetCmd.ExecuteNonQuery();
			db.Close();
			CommandPreset preC = new CommandPreset(name, message, alias);
			Bot.commandPre.listPre.Add(preC);
			}

		public void updateInfo()
			{
			Bot.commandPre.listPre.Remove(findCommand("info"));
			Bot.commandPre.listPre.Add(new CommandPreset("info", Sinister_Bot.Properties.Settings.Default.gbStream.Split('#')[5], null));
			}

		#region alias

		public void addAlias(string commandname, string alias)
			{
			bool commandFound = false;
			foreach (CommandPreset pre in Bot.commandPre.listPre)
				{
				if (pre.name.Equals(commandname))
					{
					commandFound = true;
					}
				}
			if (commandFound)
				{
				db.Open();
				string editable = "select alias from PresetCommands where name='" + commandname + "'";
				SQLiteCommand check = new SQLiteCommand(editable, db);
				SQLiteDataReader reader = check.ExecuteReader();
				reader.Read();
				string newAlias = "";
				if (reader.GetString(0).Length > 1)
					{
					newAlias += reader.GetString(0);
					if (!newAlias.Contains(alias))
						{
						newAlias += ", " + alias;
						}
					}
				else
					{
					newAlias += alias;
					}
				SQLiteCommand edit = new SQLiteCommand(db);
				edit.CommandText = "update PresetCommands set alias='" + newAlias + "' where name='" + commandname + "'";
				edit.ExecuteNonQuery();
				db.Close();
				Bot.irc.sendChatMessage($"Der Alias \"{alias}\" wurde dem Command \"{commandname}\" hinzugefügt");
				updateCommandlistCommand();
				}
			else
				{
				Bot.irc.sendChatMessage("Dieser Command existiert nicht!");
				}
			}

		public void editAlias(string commandname, string oldAlias, string newAlias)
			{
			bool commandFound = false;
			foreach (CommandPreset pre in Bot.commandPre.listPre)
				{
				if (pre.name.Equals(commandname))
					{
					commandFound = true;
					}

				if (commandFound)
					{
					db.Open();
					string editable = "select alias from PresetCommands where name='" + commandname + "'";
					SQLiteCommand check = new SQLiteCommand(editable, db);
					SQLiteDataReader reader = check.ExecuteReader();
					reader.Read();
					string alias = reader.GetString(0);
					alias = alias.Replace(oldAlias, newAlias);
					SQLiteCommand edit = new SQLiteCommand(db);
					edit.CommandText = "update PresetCommands set alias='" + alias + "' where name='" + commandname + "'";
					edit.ExecuteNonQuery();
					db.Close();
					Bot.irc.sendChatMessage($"Der Alias \"{oldAlias}\" vom Command \"{commandname}\" wurde zu \"{newAlias}\" geändert");
					updateCommandlistCommand();
					}
				else
					{
					Bot.irc.sendChatMessage("Dieser Command existiert nicht!");
					}
				}
			}

		public void delAlias(string commandname, string alias)
			{
			bool commandFound = false;
			foreach (CommandPreset pre in Bot.commandPre.listPre)
				{
				if (pre.name.Equals(commandname))
					{
					commandFound = true;
					}
				}
			if (commandFound)
				{
				db.Open();
				string delete = "select from alias from PresetCommands where name='" + commandname + "'";
				SQLiteCommand check = new SQLiteCommand(delete, db);
				SQLiteDataReader reader = check.ExecuteReader();
				reader.Read();

				string newAlias = reader.GetString(0);
				if ((reader.GetString(0).Split(',', ' ')).Length > 1)
					{
					newAlias = newAlias.Replace(", " + alias, "");
					}
				else
					{
					newAlias = "";
					}
				SQLiteCommand edit = new SQLiteCommand(db);
				edit.CommandText = "update PresetCommands set alias='" + newAlias + "' where name='" + commandname + "'";
				edit.ExecuteNonQuery();
				db.Close();
				Bot.irc.sendChatMessage($"Der Alias \"{alias}\" wurde vom Command \"{commandname}\" entfernt");
				updateCommandlistCommand();
				}
			}
		#endregion
		}
	}
