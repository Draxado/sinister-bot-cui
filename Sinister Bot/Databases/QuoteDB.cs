﻿using Sinister_Bot;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using Twitch;

namespace Quotesystem
	{
	public class QuoteDB //functions as interface between db and bot
		{
		public static string name = "Quotes.sqlite";
		public static string path = Path.Combine(Bot.app, name);
		public Dictionary<int, Quote> quotes = new Dictionary<int, Quote>();
		public SQLiteConnection db = new SQLiteConnection("Data Source = " + path);

		public QuoteDB()
			{
			SQLiteConnection.CreateFile(name);
			db.Open();
			string s = "create table Quotes (number int primary key, viewer varchar(30), message varchar(50), game varchar(30))";
			SQLiteCommand c = new SQLiteCommand(s, db);
			c.ExecuteNonQuery();
			db.Close();
			}

		public QuoteDB(string p)
			{
			db.Open();
			string s = "select * from Quotes";
			SQLiteCommand c = new SQLiteCommand(s, db);
			SQLiteDataReader reader = c.ExecuteReader();
			while (reader.Read())
				{
				Quote q = new Quote(reader.GetString(1), reader.GetString(2), reader.GetString(3));
				quotes.Add(reader.GetInt32(0), q);
				}
			db.Close();
			}

		public void addQuote(string viewer, string text, string game)
			{
			string g = (game == null) ? "unknown game" : game;
			bool foundEmpty = false;
			bool foundSame = false;
			foreach (KeyValuePair<int, Quote> q in Bot.quote.quotes)
				{
				if (q.Value.text == text)
					{
					foundSame = true;
					break;
					}
				}
			if (!foundSame)
				{
				foreach (KeyValuePair<int, Quote> q in Bot.quote.quotes)
					{
					if (q.Value.text == " ")
						{
						foundEmpty = true;
						q.Value.game = g;
						q.Value.viewer = viewer;
						q.Value.text = text;

						db.Open();
						string editGame = "update Quotes set game='" + g + "' where number=" + q.Key;
						string editViewer = "update Quotes set viewer='" + viewer + "' where number=" + q.Key;
						string editText = "update Quotes set message='" + text + "' where number=" + q.Key;
						SQLiteCommand editG = new SQLiteCommand(editGame, db);
						editG.ExecuteNonQuery();
						SQLiteCommand editV = new SQLiteCommand(editViewer, db);
						editV.ExecuteNonQuery();
						SQLiteCommand editT = new SQLiteCommand(editText, db);
						editT.ExecuteNonQuery();
						db.Close();
						Bot.irc.sendChatMessage($"Die Quote #{q.Key} wurde erstellt.");
						return;
						}
					else if (q.Value.text == text)
						{
						Bot.irc.sendChatMessage("Diese Quote wurde schon erstellt!");
						return;
						}
					}
				if (!foundEmpty)
					{
					db.Open();
					SQLiteCommand add = new SQLiteCommand(db);
					add.CommandText = "insert into Quotes (number, viewer, message, game) values (@number, @viewer, @message, @game)";
					add.Prepare();
					add.Parameters.AddWithValue("@number", Bot.quote.quotes.Count);
					add.Parameters.AddWithValue("@viewer", viewer);
					add.Parameters.AddWithValue("@message", text);
					add.Parameters.AddWithValue("@game", g);
					add.ExecuteNonQuery();
					db.Close();

					Quote q = new Quote(viewer, text, StreamData.game);
					Bot.quote.quotes.Add(Bot.quote.quotes.Count, q);
					Bot.irc.sendChatMessage($"Die Quote #{Bot.quote.quotes.Count - 1} wurde erstellt.");
					}
				}
			else
				{
				Bot.irc.sendChatMessage("Dieses Zitat gibt es schon!");
				}
			}

		public void editQuote(int number, string newMessage)
			{
			bool found = false;
			foreach (KeyValuePair<int, Quote> q in Bot.quote.quotes)
				{
				if (q.Value.text == newMessage)
					{
					found = true;
					break;
					}
				}
			if (!found)
				{
				Bot.quote.quotes[number].text = newMessage;

				db.Open();
				string update = "update Quotes set message='" + newMessage + "' where number='" + number + "'";
				SQLiteCommand edit = new SQLiteCommand(update, db);
				edit.ExecuteNonQuery();
				db.Close();
				Bot.irc.sendChatMessage($"Die Quote #{number} wurde geändert.");
				}
			else
				{
				Bot.irc.sendChatMessage("Es gibt dieses Zitat bereits!");
				}
			}

		/*
		 * the quote only gets overwritten with " " instead of being deleted
		 * so the indices don't get mixed up in the dictionary and are
		 * automatically reused
		 */

		public void deleteQuote(int number)
			{
			db.Open();
			string update = "update Quotes set message=' '  where number='" + number + "'";
			SQLiteCommand delete = new SQLiteCommand(update, db);
			delete.ExecuteNonQuery();
			db.Close();

			Bot.quote.quotes[number].text = " ";
			Bot.irc.sendChatMessage($"Die Quote #{number} wurde gelöscht.");
			}
		}
	}