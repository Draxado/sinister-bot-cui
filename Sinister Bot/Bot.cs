﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using CommandSystem;
using CurrencySystem;
using Quotesystem;
using Sinister_Bot.Properties;
using System.Data.SQLite;

namespace Sinister_Bot
	{
	public static class Bot
		{
		public static bool streamOffline = true;
		public static bool cancelBtnClicked = false;
		public static IrcClient irc = new IrcClient();
		public static CancellationToken tokenIrc;
		public static CancellationTokenSource sourceIrc;
		public static ViewerDB viewer;
		public static CommandDB command;
		public static QuoteDB quote;
		public static CommandPresetDB commandPre;
		public static string app = Application.StartupPath;
		public static frmMain main;
		public static bool UIinput = false;
		//oauth:o54e8ukjy0i8n40b55kz9krlvno1nt

		[STAThread]
		private static void Main()
			{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			main = new frmMain();
			loadDatabases();
			Application.Run(main);
			}

		public static void loadDatabases()
			{
			Console.WriteLine("Loading Databases");

			///////////////////////////////////////////////////////////////////////////////////////////////////////
			//////////////////////////////////////////// Preset Command ///////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////////////
			if (!File.Exists(CommandPresetDB.path))
				{
				commandPre = new CommandPresetDB();
				CommandPreset.addPresetCommands();
			
				Settings.Default.firstLaunch = true;
				Settings.Default.Save();
				}
			else
				{
				List<Tuple<string, string[]>> oldAlias = new List<Tuple<string, string[]>>();

				CommandPresetDB.db.Open();
				string sel = "select name, alias from PresetCommands";
				SQLiteCommand select = new SQLiteCommand(sel, CommandPresetDB.db);
				SQLiteDataReader reader = select.ExecuteReader();
				while (reader.Read())
					{
					if (reader.GetString(1) != "")
						{
						oldAlias.Add(new Tuple<string, string[]>(reader.GetString(0), reader.GetString(1).Replace(" ", "").Split(',')));
						}
					}
				CommandPresetDB.db.Close();

				commandPre = new CommandPresetDB();
				CommandPreset.addPresetCommands();

				foreach (Tuple<string, string[]> t in oldAlias)
					{
					for (int i = 0; i < t.Item2.Length; i++)
						{
						commandPre.addAlias(t.Item1, t.Item2[i]);
						}
					}
				}

			///////////////////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////// Command ///////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////////////
			if (!File.Exists(CommandDB.path))
				{
				command = new CommandDB();
				Settings.Default.firstLaunch = true;
				Settings.Default.Save();
				}
			else
				{
				command = new CommandDB(CommandDB.path);
				}

			///////////////////////////////////////////////////////////////////////////////////////////////////////
			//////////////////////////////////////////////// Viewer ///////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////////////
			if (!File.Exists(ViewerDB.path))
				{
				viewer = new ViewerDB();
				Settings.Default.firstLaunch = true;
				Settings.Default.Save();
				}
			else
				{
				viewer = new ViewerDB(ViewerDB.path);
				}

			///////////////////////////////////////////////////////////////////////////////////////////////////////
			///////////////////////////////////////////////// Quote ///////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////////////
			if (!File.Exists(QuoteDB.path))
				{
				quote = new QuoteDB();
				Settings.Default.firstLaunch = true;
				Settings.Default.Save();
				}
			else
				{
				quote = new QuoteDB(QuoteDB.path);
				}

			CommandPresetDB.updateCommandlistCommand();

            Settings.Default.firstLaunch = false;
			Settings.Default.Save();
			}
		}
	}