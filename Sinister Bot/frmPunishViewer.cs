﻿using Sinister_Bot.Custom_Controls;
using Sinister_Bot.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sinister_Bot
    {
    public partial class frmPunishViewer : Form
        {
        private Color _defForeColor = Color.RoyalBlue;
        private Color _defBackColor = Color.FromArgb(28, 28, 28);
        private string viewer = "";
        private int duration = -1;

        public frmPunishViewer(string type)
            {
            InitializeComponent();
            switch (type)
                {
                case "warning":
                    lblTitle.Text = "Warnung";
                    tbDuration.Text = "10";
                    tbDuration.Enabled = false;
                    cbDurationUnit.SelectedIndex = 0;
                    cbDurationUnit.Enabled = false;
                    btnPunish.Text = "Warnen";
                    break;
                case "timeout":
                    lblTitle.Text = "Timeout";
                    tbDuration.Text = "300"; //5 min
                    tbDuration.Enabled = true;
                    cbDurationUnit.SelectedIndex = 0;
                    cbDurationUnit.Enabled = true;
                    btnPunish.Text = "Timeouten";
                    break;
                case "ban":
                    lblTitle.Text = "Ban";
                    tbDuration.Text = "-1";
                    tbDuration.Enabled = false;
                    cbDurationUnit.Text = "";
                    cbDurationUnit.Enabled = false;
                    btnPunish.Text = "Bannen";
                    break;
                default:
                    Console.WriteLine("Feher bei Viewer punishen!");
                    break;
                }
            }


        #region design & control

        //////////////////////////////////////////// move frmMain /////////////////////////////////////////////
        public const int WM_NCLBUTTONDOWN = 0xA1;

        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void frmRankEdit_MouseDown(object sender, MouseEventArgs e)
            {
            if (e.Button == MouseButtons.Left)
                {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
                }
            }

        ///////////////////////////////////////////// Draw Border /////////////////////////////////////////////
        protected override void OnPaint(PaintEventArgs e)
            {
            base.OnPaint(e);
            DrawControl(e.Graphics);
            ForeColor = _defForeColor;
            }

        private void DrawControl(Graphics graphics)
            {
            if (!Visible)
                {
                return;
                }

            Rectangle MainControlArea = ClientRectangle;
            Rectangle MainArea = DisplayRectangle;

            //fill background area frmMain
            Brush b = new SolidBrush(_defBackColor);
            graphics.FillRectangle(b, MainControlArea);
            b.Dispose();

            ControlPaint.DrawBorder(graphics, MainControlArea,
            _defForeColor, 2, ButtonBorderStyle.Solid,    //left
            _defForeColor, 2, ButtonBorderStyle.Solid,    //top
            _defForeColor, 2, ButtonBorderStyle.Solid,    //right
            _defForeColor, 2, ButtonBorderStyle.Solid);   //bottonm
            }

        #endregion design & control

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnPunish_Click(object sender, EventArgs e)
        {
            switch (((Button)sender).Text)
            {
                case "Warnen":
                    Bot.irc.sendIrcMessage($":tmi.twitch.tv CLEARCHAT #{Settings.Default.gbStream.Split('#')[0]} :{viewer} @ban-duration=10;ban-reason=Du hast eine Regel verletzt und erhälst hier mit eine Warnung! Bitte halte dich in Zukunft an die Regeln oder es wird größere Timeouts geben! :tmi.twitch.tv CLEARCHAT #{Settings.Default.gbStream.Split('#')[0]} :{viewer}", true);
                    Console.WriteLine($"Warnen - {viewer} - {duration}");
                    break;
                case "Timeouten":
                    Bot.irc.sendIrcMessage($":tmi.twitch.tv CLEARCHAT #{Settings.Default.gbStream.Split('#')[0]} :{viewer} @ban-duration={duration};ban-reason=Du hast (wiederholt) eine Regel verletzt! Bitte halte dich in Zukunft an die Regeln oder es wird größere Timeouts geben! :tmi.twitch.tv CLEARCHAT #{Settings.Default.gbStream.Split('#')[0]} :{viewer}", true);
                    Console.WriteLine($"Timeout - {viewer} - {duration}");
                    break;
                case "Bannen":
                    Bot.irc.sendIrcMessage($":tmi.twitch.tv CLEARCHAT #{Settings.Default.gbStream.Split('#')[0]} :{viewer} @ban - reason =Das wars für dich! Bye bye! :tmi.twitch.tv CLEARCHAT #{Settings.Default.gbStream.Split('#')[0]} :{viewer}", true);
                    Console.WriteLine($"Bannen - {viewer} - {duration}");
                    break;
                default:
                    Console.WriteLine("Fehler beim Ausführen der Bestrafung");
                    break;
            }
            Dispose();
        }

        private void tbViewer_TextChanged(object sender, EventArgs e)
        {
            viewer = ((tb)sender).Text;
        }
        private void tbViewer_Leave(object sender, EventArgs e)
        {
            bool found = false;
            foreach(CurrencySystem.Viewer v in Bot.viewer.list)
            {
                if (v.name == ((tb)sender).Text)
                {
                    found = true;
                    break;
                }
            }
            if (!found && !btnCancel.Focused)
            {
                MessageBox.Show("Es wurde kein Viewer mit diesem Namen gefunden!", "Viewer nicht gefunden", MessageBoxButtons.OK);
                ((tb)sender).Focus();
            }
        }   

        private void tbDuration_TextChanged(object sender, EventArgs e)
        {
            editDuration();
        }

        private void cbDurationUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            editDuration();
        }

        private void editDuration()
        {
            duration = Convert.ToInt32(tbDuration.Text);
            if (cbDurationUnit.SelectedIndex == 1)
            {
                duration *= 60;
            }
        }

    }
    }