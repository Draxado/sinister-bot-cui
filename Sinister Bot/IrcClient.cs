﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using MessageHandling;
using Sinister_Bot.Properties;
using Twitch;
using CommandSystem;

namespace Sinister_Bot
{
    public class IrcClient
    {
        public List<Tuple<string, DateTime>> messageSended = new List<Tuple<string, DateTime>>();
        public static string streamchannel;
        public static string botname;
        public static string token;
        public static TcpClient tcp = new TcpClient();
        private StreamReader input;
        private StreamWriter output;
        private static Task clear;
        private AutoResetEvent bufferOutput = new AutoResetEvent(false);
        private AutoResetEvent waitBufferedOutput = new AutoResetEvent(false);

        public IrcClient()
        {
            if (!Settings.Default.firstLaunch)
            {
                string[] gbStream = Settings.Default.gbStream.Split('#');
                streamchannel = gbStream[0];
                botname = gbStream[1];
                token = gbStream[2];
            }
        }

        public void login(string ip, int port)
        {
            try
            {
                tcp.Connect(ip, port);
                if (input == null)
                {
                    input = new StreamReader(tcp.GetStream());
                }
                if (output == null)
                {
                    output = new StreamWriter(tcp.GetStream());
                }
                output.WriteLine("PASS " + token);
                output.WriteLine("NICK " + botname);
                output.WriteLine("USER " + botname + " 8 * :" + botname);
                output.Flush();
            }
            catch
            {
                MessageBox.Show("Bitte überprüfe deine Internetverbindung und versuche es erneut.");
                Bot.main.btnConnect.Enabled = true;
                Bot.main.btnDisconnect.Enabled = false;
                Bot.main.btnMessageSend.Enabled = false;
            }
        }
        public void logout()
        {
            try
            {
                tcp.Close();
                input.Close();
                output.Close();
                tcp = null;
            }
            catch
            {
                MessageBox.Show("Bitte überprüfe deine Internetverbindung und versuche es erneut.");
                throw;
            }
        }

        //joins the irc room from the twitchstreamers channel
        public void joinRoom()
        {
            try
            {
                if (tcp == null)
                {
                    Bot.irc.login("irc.chat.twitch.tv", 6667);
                }
                else
                {
                    input = new StreamReader(tcp.GetStream());
                    output = new StreamWriter(tcp.GetStream());
                }
                output.WriteLine("JOIN #" + streamchannel);
                output.Flush();
            }
            catch
            {
                MessageBox.Show("Bitte überprüfe deine Internetverbindung und versuche es erneut.");
                throw;
            }
            TwitchViewer.getViewerlistFromTwitch();
            Bot.sourceIrc = new CancellationTokenSource();
            Bot.tokenIrc = Bot.sourceIrc.Token;
            clearMessageList();
            ChatMessage.startReading();
            TwitchViewer.createTwitchViewerTask();
        }

        //leaves the room
        public void leaveRoom()
        {
            try
            {
                output.WriteLine("PART #" + streamchannel);
                output.Flush();
                logout();
            }
            catch
            {
                MessageBox.Show("Bitte überprüfe deine Internetverbindung und versuche es erneut.");
                throw;
            }
            Bot.sourceIrc.Cancel();
        }

        //sending the message via irc, used by sendChatMessage to make it easier to use and automatically convert the message into the irc format
        public void sendIrcMessage(string message, bool skipOutput = false)
        {
            try
            {
                output.WriteLine(message);
                if (!message.Contains("PONG") && !skipOutput)
                {
                    string send = $"{Settings.Default.gbStream.Split('#')[1]}: {message.Substring(message.IndexOf("PRIVMSG") + streamchannel.Length + 11)}";
                    Bot.main.updateTbChat(send);
                }
                output.Flush();
            }
            catch
            {
                MessageBox.Show("Bitte überprüfe deine Internetverbindung und versuche es erneut.");
                throw;
            }
        }

        //used for easier coding, prevents bot to be temporarily banned for spamming and buffering outgoing messages
        public void sendChatMessage(string message)
        {
            if (Bot.UIinput)
            {
                MessageBox.Show(message);
            }
            if (tcp.Connected)
            {
                if (messageSended.Count < 20)
                {
                    lock (messageSended)
                    {
                        sendIrcMessage(":" + botname + "!" + botname + "@" + botname + ".tmi.twitch.tv PRIVMSG #" + streamchannel + " :" + message);
                        messageSended.Add(new Tuple<string, DateTime>(message, DateTime.Now));
                    }
                }
                else
                {
                    bufferOutput.WaitOne();
                    lock (messageSended)
                    {
                        sendIrcMessage(":" + botname + "!" + botname + "@" + botname + ".tmi.twitch.tv PRIVMSG #" + streamchannel + " :" + message);
                        messageSended.Add(new Tuple<string, DateTime>(message, DateTime.Now));
                    }
                    waitBufferedOutput.Set();
                }
            }
        }

        //reads incoming messages
        public string readMessage()
        {
            try
            {
                if (!Bot.tokenIrc.IsCancellationRequested)
                {
                    if (input != null)
                    {
                        return input.ReadLine();
                    }
                    return null;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                MessageBox.Show("Es ist ein Fehler aufgetreten. Bitte überprüfe deine Internetverbindung und versuche es erneut. Falls diese Meldung erneut erscheint verfasse bitte einen Bug-Report.");
                return null;
            }
        }

        //updates the buffered messages and sends them if the bot is allowed to
        private void clearMessageList()
        {
            clear = Task.Factory.StartNew(() =>
            {
                while (!Bot.tokenIrc.IsCancellationRequested)
                {
                    if (messageSended.Count != 0)
                    {
                        lock (messageSended)
                        {
                            again:
                            foreach (Tuple<string, DateTime> tuple in messageSended)
                            {
                                if (tuple.Item2.AddSeconds(30) >= DateTime.Now)
                                {
                                    messageSended.Remove(tuple);
                                    goto again;
                                }
                            }
                        }
                        Thread.Sleep(1000);
                        if (messageSended.Count < 20)
                        {
                            bufferOutput.Set();
                            waitBufferedOutput.WaitOne();
                        }
                    }
                    else
                    {
                        Thread.Sleep(1000);
                    }
                }
            }, Bot.tokenIrc);
        }
    }
}