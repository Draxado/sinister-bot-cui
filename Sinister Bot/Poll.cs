﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CurrencySystem;
using Sinister_Bot;

namespace Pollsystem
	{
	public class Poll
		{
		public static Poll activePoll;
		public static Poll currentDisplayedPoll;
		public static List<Poll> recentPolls = new List<Poll>();
		public string name;
		public string question;
		public object[,] opt = new object[8, 1]; //optionname - string, optioncounter - int, optionnumber = index--
		public List<Viewer> participants = new List<Viewer>();
		public string optionMessage = "";
		public static AutoResetEvent stop;

		public Poll(string n, string q, object[,] o)
			{
			name = n;
			question = q;
			for (int i = 0; i < 8; i++)
				{
				opt[i, 0] = o[i, 0];
				opt[i, 1] = 0;
				if (o[i, 0].ToString() != "")
					{
					optionMessage += (i != 7) ? $"{i} : {o[i, 0]}" + Environment.NewLine : $"{i} : {o[i, 0]}";
					}
				}
			}

		public static void handleMessage(string message, Viewer sender)
			{
			if (!activePoll.participants.Contains(sender))
				{
				int optionnumber = Convert.ToInt32(message.Split()[1]);
				int c = Convert.ToInt32(activePoll.opt[optionnumber, 1]);
				c++;
				activePoll.opt[optionnumber, 1] = c;
				activePoll.participants.Add(sender);
				}
			}

		public static void start(Poll poll)
			{
			Task.Factory.StartNew(() =>
			{
				activePoll = poll;
				Bot.irc.sendChatMessage($"Eine neue Umfrage wurde gestartet: \"{poll.question}\" {poll.optionMessage}. Um abzustimmen schreibe bitte: \"?{poll.question} und die Nummer deiner Auswahl. (Beispiel: ?test 5");
				stop.WaitOne();
				recentPolls.Add(poll);
				activePoll = null;
			}, Bot.tokenIrc);
			pollStopped(poll);
			}

		private static void pollStopped(Poll poll)
			{
			string message = "Die Auswertung der Umfrage: ";
			for (int i = 0; i < 8; i++)
				{
				double perc = (Convert.ToDouble(poll.opt[i, 1]) / poll.participants.Count) * 100;
				message += $"{poll.opt[i, 0]}({perc}%) - ";
				}

			message = message.Remove(message.Length - 3);
			Bot.irc.sendChatMessage("Die Umfrage ist zuende." + message);
			//todo fraimPollResult
			}
		}
	}