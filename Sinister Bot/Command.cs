﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CurrencySystem;
using Sinister_Bot;

namespace CommandSystem
	{
	public class Command
		{
		public bool editable;
		public int executionerLevel;    //0 = streamer/mod/staff ect..., 1 = subscriber, 2 = follower, 3 = viewer
		public string message;
		public string name;
		public string[] alias;
		public static int defaultCooldown = 10000; //milliseconds -> 10s
		public int cooldown;

		public bool used;

		public Command(string name, string message, bool editable, int executionerLevel, string[] alias, int cooldown)
			{
			this.name = name;
			this.message = message;
			this.editable = editable;
			this.executionerLevel = executionerLevel;
			this.used = false;
			this.alias = alias;
			this.cooldown = cooldown;
			}

		public static void timer(Command c)
			{
			Task.Factory.StartNew(() =>
			{
				c.used = true;
				Thread.Sleep(c.cooldown);
				c.used = false;
			}, Bot.tokenIrc);
			}

		public static void handleMessage(string content, Viewer sender)
			{
			//check if it is just a call of an existing command
			if (content.ToLower().Equals(Bot.main.tbCurrencyCommand.Text.ToLower())) {
				Bot.irc.sendChatMessage(Bot.main.tbCurrencyCommandMessage.Text.Replace("{Name}", sender.name).Replace("{Anzahl}", sender.currency.ToString()).Replace("{Währung}", Bot.main.tbCurrencyName.Text).Replace("{Rang}", sender.rank));
				return;
			}
			string[] preCmd = CommandPreset.list().Split('/'); //PresetCommand.list() lists up all Commands!
			foreach (string cmdName in preCmd)
				{
				string search = "!" + cmdName.Trim();
				search = search.Trim();
				string[] alias = getAlias(null, null, cmdName.Trim());
				if ((search.Split()[0].Equals(content) || checkAlias(alias, content.Trim().Substring(1))) && cmdName != "")
					{
					executeCommand(search.Split()[0], sender);
					break;
					}
				}
			//then check if the command is only for mods & if the sender is allowed to use it
			if ((content.StartsWith("!add") || content.StartsWith("!edit") || content.StartsWith("!delete")) && (sender.status <= 1))
				{
				executeModCommand(content);
				}
			}

		#region execution

		private static void executeCommand(string name, Viewer sender)
			{
			bool found = false;
			foreach (CommandPreset pre in Bot.commandPre.listPre)
				{
				string[] alias = getAlias(null, pre, null);
				if (name.Equals("!" + pre.name) || checkAlias(alias, name))
					{
					Bot.irc.sendChatMessage(pre.message);
					found = true;
					break;
					}
				}
			if (!found)
				{
				foreach (Command c in Bot.command.list)
					{
					string[] alias = getAlias(c, null, null);
					if (name.Equals("!" + c.name) || checkAlias(alias, name))
						{
						if (sender.status <= c.executionerLevel)
							{
							if (!c.used)
								{
								Bot.irc.sendChatMessage(c.message);
								timer(c);
								}
							}
						else
							{
							Bot.irc.sendChatMessage("Du darfst diesen Command nicht benutzen @" + sender.name);
							}
						break;
						}
					}
				}
			}

		private static void executeModCommand(string msg)
			{
			string[] command = (msg.Split());
			string cmdName = command[0];

			#region command

			if (cmdName.Equals("!add"))
				{
				if (command.Length < 5)
					{
					Bot.irc.sendChatMessage("Versuche es bitte nochmal! -> " +
					"!add {name} {editable[0 = no / 1 = yes]} {userlevel[0 = streamer / 1 = mod / 2 = subscriber / 3 = viewer]} {message} ");
					}
				else
					{
					if (command.Length > 5)
						{
						string cmdMessage = command[4];
						for (int i = 5; i < command.Length; i++)
							{
							cmdMessage = cmdMessage + " " + command[i];
							}
						command[4] = cmdMessage;
						}
					try
						{
						string cmdOperatorAdd = command[0];
						string cmdNameAdd = command[1];
						bool cmdEditableAdd = Convert.ToBoolean(int.Parse(command[2]));
						int cmdExecutionerLevelAdd = int.Parse(command[3]);
						string cmdTextAdd = command[4];
						string[] alias = { "" };
						Bot.command.createCommand(cmdNameAdd, cmdEditableAdd, cmdExecutionerLevelAdd, cmdTextAdd, alias, defaultCooldown);
						}
					catch (FormatException)
						{
						Bot.irc.sendChatMessage("Versuche es bitte nochmal! -> " +
						"!add {name} {editable[0 = no / 1 = yes]} {userlevel[0 = streamer / 1 = mod / 2 = subscriber / 3 = viewer]} {message} ");
						}
					}
				}

			if (cmdName.Equals("!edit"))
				{
				if (command.Length < 3)
					{
					Bot.irc.sendChatMessage("Versuche es bitte nochmal! -> !edit {name} {new Message}");
					}
				else
					{
					string cmdOperatorEdit = command[0];
					string cmdNameEdit = command[1];
					string cmdTextEdit = command[2];
					if (command.Length > 3)
						{
						for (int i = 3; i < command.Length; i++)
							{
							cmdTextEdit = cmdTextEdit + " " + command[i];
							}
						}
					Bot.command.editCommand(cmdNameEdit, cmdTextEdit);
					}
				}

			if (cmdName.Equals("!editCmd"))
				{
				if (command.Length < 3)
					{
					Bot.irc.sendChatMessage("Versuche es bitte nochmal! -> !editCmd {old name} {new name}");
					}
				else if (command.Length == 3)
					{
					string cmdOperatorEditCmd = command[0];
					string cmdNameOld = command[1];
					string cmdNameNew = command[2];
					Bot.command.editCommandName(cmdNameOld, cmdNameNew);
					}
				else
					{
					Bot.irc.sendChatMessage("Commandname dürfen nur ein Wort lang sein!");
					}
				}

			if (cmdName.Equals("!delete"))
				{
				if (command.Length != 2)
					{
					Bot.irc.sendChatMessage("Versuche es bitte nochmal! -> !delete {name}");
					}
				else
					{
					string cmdOperatorDelete = command[0];
					string cmdNameDelete = command[1];
					bool foundDelete = false;
					foreach (CommandPreset pre in Bot.commandPre.listPre)
						{
						if (cmdNameDelete.Equals(pre.name))
							{
							Bot.irc.sendChatMessage("Du kannst diesen Command nicht löschen!");
							foundDelete = true;
							break;
							}
						}
					if (!foundDelete)
						{
						foreach (Command search in Bot.command.list)
							{
							if (cmdNameDelete.Equals(search.name))
								{
								Bot.command.deleteCommand(search);
								break;
								}
							}
						}
					}
				}
			
			#endregion command

			#region alias

			if (cmdName.Equals("!addAlias"))
				{
				if (command.Length < 3)
					{
					Bot.irc.sendChatMessage("Versuche es bitte nochmal! -> !addAlias {Commandname} {new Alias}");
					}
				else
					{
					string nameCommand = command[1];
					string nameAlias = command[2];
					Bot.command.addAlias(nameCommand, nameAlias);
					}
				}
			if (cmdName.Equals("!editAlias"))
				{
				if (command.Length < 4)
					{
					Bot.irc.sendChatMessage("Versuche es bitte nochmal! -> !editAlias {Commandname} {old Alias} {old Alias}");
					}
				else
					{
					string nameCommand = command[1];
					string nameAliasOld = command[2];
					string nameAliasNew = command[3];
					Bot.command.editAlias(nameCommand, nameAliasOld, nameAliasNew);
					}
				}
			if (cmdName.Equals("!deleteAlias"))
				{
				if (command.Length < 3)
					{
					Bot.irc.sendChatMessage("Versuche es bitte nochmal! -> !delAlias {Commandname} {Alias}");
					}
				else
					{
					string nameCommand = command[1];
					string nameAlias = command[2];
					Bot.command.deleteAlias(nameCommand, nameAlias);
					}
				}
			Bot.main.updateCommandlistdisplay();
			#endregion alias
			}

		#endregion execution

		#region finding command/alias

		private static string[] getAlias(Command c, CommandPreset preC, string searchAlias)
			{
			string[] a = { "" };
			if (searchAlias != null)
				{
				string sAlias = searchAlias;
				if (searchAlias.Contains("alias"))
					{
					sAlias = sAlias.Remove(searchAlias.IndexOf("(alias") - 1);
					}
				foreach (Command searchC in Bot.command.list)
					{
					if (searchC.name == sAlias)
						{
						return searchC.alias;
						}
					}
				foreach (CommandPreset searchPreC in Bot.commandPre.listPre)
					{
					if (searchPreC.name == sAlias)
						{
						return searchPreC.alias;
						}
					}
				}
			if (preC != null)
				{
				if (preC.alias != null)
					{
					foreach (string al1 in preC.alias)
						{
						if (al1 != "")
							{
							string[] temp = new string[a.Length + 1];
							a.CopyTo(temp, 0);
							temp[temp.Length - 1] = al1;
							a = temp;
							}
						}
					}
				else
					{
					string[] temp = { "" };
					a = temp;
					}
				}
			else if (c != null)
				{
				if (c.alias != null)
					{
					foreach (string al2 in c.alias)
						{
						if (al2 != "")
							{
							string[] temp = new string[a.Length];
							temp[temp.Length - 1] = al2;
							a = temp;
							}
						}
					}
				else
					{
					string[] temp = { "" };
					a = temp;
					}
				}
			return a;
			}

		private static bool checkAlias(string[] alias, string name)
			{
			if (alias != null)
				{
				foreach (string al in alias)
					{
					if (name == al)
						{
						return true;
						}
					}
				}
			return false;
			}

		#endregion finding command/alias
		}
	}