﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Sinister_Bot.Properties;

namespace Twitch
	{
	public class StreamData
		{
		private static Task update;
		public static string channelCreated;
		public static CancellationTokenSource f;
		public static int follower;
		public static string game;
		public static int nextGoal;
		public static string streamStarted;
		public static string streamtitel;
		public static CancellationToken token;
		public static string url = "https://api.twitch.tv/kraken/streams/" + Settings.Default.gbStream.Split('#')[0];
		public static WebClient w = new WebClient();

		public static void updateData(string data)
			{
			//string json = string.Empty;
			//try
			//	{
			//	json = w.DownloadString(url);
			//	RootObject temp = JsonConvert.DeserializeObject<RootObject>(json);
			//	switch (data)
			//		{
			//		case "game":
			//			game = temp.stream.game;
			//			break;

			//		case "streamtitel":
			//			streamtitel = temp.stream.channel.status;
			//			break;

			//		case "channelCreated":
			//			channelCreated = temp.stream.channel.created_at;
			//			break;

			//		case "streamStarted":
			//			streamStarted = temp.stream.created_at;
			//			break;

			//		case "follower":
			//			follower = temp.stream.channel.followers;
			//			break;

			//		case "all":
			//			game = temp.stream.game;
			//			streamtitel = temp.stream.channel.status;
			//			channelCreated = temp.stream.channel.created_at;
			//			streamStarted = temp.stream.created_at;
			//			follower = temp.stream.channel.followers;
			//			break;

			//		default:
			//			Console.WriteLine("DU HAST DICH VERSCHRIEBEN!");
			//			break;
			//			;
			//		}
			//	}
			//catch (Exception e)
			//	{
			//	Console.WriteLine(e.InnerException.Message);
			//	}
			}

		public static void updateFollower()
			{
			//update = Task.Factory.StartNew(() =>
			//{
			//	while (Program.irc != null)
			//		{
			//		updateData("follower");
			//		if (follower >= nextGoal && nextGoal != 0)
			//			{
			//			Program.irc.sendChatMessage($"Herzlichen Glückwunsch @{UserDatabase.userChannel}! Du hast die {nextGoal}-Follower erreicht!");
			//			int l = Convert.ToString(nextGoal).Length;
			//			switch (l)
			//				{
			//				case 3:
			//					if (nextGoal != 500)
			//						{
			//						nextGoal = 500;
			//						}
			//					else
			//						{
			//						nextGoal = 1000;
			//						}
			//					break;

			//				case 4:
			//					if (nextGoal != 5000)
			//						{
			//						nextGoal = 5000;
			//						}
			//					else
			//						{
			//						nextGoal = 10000;
			//						}
			//					break;

			//				case 5:
			//					if (nextGoal <= 25000)
			//						{
			//						nextGoal += 5000;
			//						}
			//					else
			//						{
			//						nextGoal += 10000;
			//						}
			//					break;

			//				case 6:
			//					nextGoal += 50000;
			//					break;

			//				default:
			//					nextGoal = 0;
			//					break;
			//				}
			//			}
			//		Thread.Sleep(1000);
			//		}
			//}, token);
			}
		}

	#region json

	public class Channel
		{
		public int _id
			{
			get; set;
			}

		public Links3 _links
			{
			get; set;
			}

		public object background
			{
			get; set;
			}

		public object banner
			{
			get; set;
			}

		public string broadcaster_language
			{
			get; set;
			}

		public string created_at
			{
			get; set;
			}

		public object delay
			{
			get; set;
			}

		public string display_name
			{
			get; set;
			}

		public int followers
			{
			get; set;
			}

		public string game
			{
			get; set;
			}

		public string language
			{
			get; set;
			}

		public string logo
			{
			get; set;
			}

		public bool mature
			{
			get; set;
			}

		public string name
			{
			get; set;
			}

		public bool partner
			{
			get; set;
			}

		public object profile_banner
			{
			get; set;
			}

		public object profile_banner_background_color
			{
			get; set;
			}

		public string status
			{
			get; set;
			}

		public string updated_at
			{
			get; set;
			}

		public string url
			{
			get; set;
			}

		public string video_banner
			{
			get; set;
			}

		public int views
			{
			get; set;
			}
		}

	public class Links2
		{
		public string self
			{
			get; set;
			}
		}

	public class Links3
		{
		public string chat
			{
			get; set;
			}

		public string commercial
			{
			get; set;
			}

		public string editors
			{
			get; set;
			}

		public string follows
			{
			get; set;
			}

		public string self
			{
			get; set;
			}

		public string stream_key
			{
			get; set;
			}

		public string subscriptions
			{
			get; set;
			}

		public string teams
			{
			get; set;
			}

		public string videos
			{
			get; set;
			}
		}

	public class Links4
		{
		public string channel
			{
			get; set;
			}

		public string self
			{
			get; set;
			}
		}

	public class Preview
		{
		public string large
			{
			get; set;
			}

		public string medium
			{
			get; set;
			}

		public string small
			{
			get; set;
			}

		public string template
			{
			get; set;
			}
		}

	public class RootObject
		{
		public Links4 _links
			{
			get; set;
			}

		public Stream stream
			{
			get; set;
			}
		}

	public class Stream
		{
		public long _id
			{
			get; set;
			}

		public Links2 _links
			{
			get; set;
			}

		public double average_fps
			{
			get; set;
			}

		public Channel channel
			{
			get; set;
			}

		public string created_at
			{
			get; set;
			}

		public int delay
			{
			get; set;
			}

		public string game
			{
			get; set;
			}

		public bool is_playlist
			{
			get; set;
			}

		public Preview preview
			{
			get; set;
			}

		public int video_height
			{
			get; set;
			}

		public int viewers
			{
			get; set;
			}
		}

	#endregion json
	}