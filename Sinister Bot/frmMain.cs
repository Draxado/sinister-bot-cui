﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using Sinister_Bot.Custom_Controls;
using Sinister_Bot.Properties;
using CommandSystem;
using Quotesystem;
using Twitch;
using Bettingsystem;
using System.Text.RegularExpressions;
using Pollsystem;
using Giveawaysystem;
using CurrencySystem;

namespace Sinister_Bot
{
    public partial class frmMain : Form
    {
        private Color _defForeColor = Color.RoyalBlue;
        private Color _defBackColor = Color.FromArgb(28, 28, 28);
        public static bool alreadySaveStream = Settings.Default.gbStream.ToString() != "";
        public static bool alreadySaveBasicSettings = Settings.Default.gbBasicSettings.ToString() != "";
        public static bool alreadySaveViewerrankings = Settings.Default.gbViewerrankings.ToString() != "";
        public static bool missingStream = false;
        public static bool missingBasicSettings = false;
        public static bool missingViewerrankings = false;
        private bool _triggerAutoChange = true;
        public bool btnStreamCancelWasClicked = false;
        private Command editedCommand;
        public bool failed = false;
        public static bool dgvAscending = true;
        public static bool initialSelecting = false;
        public static bool changedSettings = false;

        public frmMain()
        {
            InitializeComponent();

         /*
		 * this section is to load the settings from the db
		 * if the user already saved during the last time he used this bot.
		 * if he hasn't saved the settings for one of the tabs/categories
		 * the bot will load the default settingss
		 */
            if (alreadySaveStream)
            {
                restoreStreamSettings();
            }
            else
            {
                Settings.Default.gbStream = "###Momentan nicht änderbar!#Momentan noch nicht verfügbar!#keine Info";
                Settings.Default.Save();
            }
            if (alreadySaveBasicSettings)
            {
                restoreBasicSettings();
            }
            else
            {
                Settings.Default.gbBasicSettings = "Punkte#!Punkte#{Name} du hast momentan {Anzahl} {Währung}. Dein aktueller Rang ist {Rang}.#10#5#5#Minuten";
                Settings.Default.Save();
            }
            if (alreadySaveViewerrankings)
            {
                restoreViewerrankings();
            }

            if (dgvViewerankings.RowCount == 0)
            {
                dgvViewerankings.Rows.Add("Neuling", 0, 0);
                dgvViewerankings.Rows.Add("Stammgast", 200, 0);
                Settings.Default.gbViewerrankings = "Neuling|0|0#Stammgast|200|0";
                Settings.Default.Save();
            }
        }

        private delegate void SetTextCallback(string text);

        private bool firstLine = true;

        public void updateTbChat(string v)
        {
            foreach (Control c in Bot.main.gbChatDisplay.Controls)
            {
                if (c.Name == "tbChat")
                {
                    tb chat = c as tb;
                    if (chat.InvokeRequired)
                    {
                        SetTextCallback d = new SetTextCallback(updateTbChat);
                        chat.Invoke(d, new object[] { v });
                    }
                    else
                    {
                        if (firstLine)
                        {
                            chat.AppendText(v);
                            firstLine = false;
                        }
                        else
                        {
                            chat.AppendText(Environment.NewLine + v);
                        }
                    }
                    break;
                }
            }
        }

        /* OnShown(Event)
		 * bot checks if any settings are missing
		 * and immediately opens the tab with the missing settings and informs the user
		 * if no settings are missing the "chat" will be opened so that the user can
		 * immediately start the bot
		 */

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            checkForEmptySettings(gbStream);
            checkForEmptySettings(gbBasicSettings);
            checkForEmptySettings(gbViewerrankings);
            if (alreadySaveBasicSettings && alreadySaveStream && alreadySaveViewerrankings)
            {
                initialSelecting = true;
                if (missingStream)
                {
                    TabControl.SelectTab(0);
                }
                else if (missingBasicSettings || missingViewerrankings)
                {
                    TabControl.SelectTab(1);
                }
                else
                {
                    TabControl.SelectTab(2);
                }
            }
            updateCommandlistdisplay();
            updateQuoteDisplay();
            clear();
            clearCreateBet();
            cbWinnerNewWinner.SelectedIndex = 0;
            cbWinnerSecondChance.SelectedIndex = 0;
            btnViewerGiveCurrency.Text = $"Zuschauer {Settings.Default.gbBasicSettings.Split('#')[0]} geben";
            dgvAscending = Settings.Default.dgvAscending;
            btnDgvSortOrder.Text = (dgvAscending) ? "Absteigend sortieren" : "Aufsteigengd sortieren";
            sortDgvViewerrankings();
            changedSettings = false;
            ViewerDB.validateViewerRanks();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////// frmMain Design ///////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////

        #region frmMain Design

        //////////////////////////////////////////// move frmMain /////////////////////////////////////////////
        public const int WM_NCLBUTTONDOWN = 0xA1;

        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void frmMain_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        ///////////////////////////////////////////// Draw Border /////////////////////////////////////////////
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            DrawControl(e.Graphics);
            ForeColor = _defForeColor;
        }

        private void DrawControl(Graphics graphics)
        {
            if (!Visible)
            {
                return;
            }

            Rectangle MainControlArea = ClientRectangle;
            Rectangle MainArea = DisplayRectangle;

            //fill background area frmMain
            Brush b = new SolidBrush(_defBackColor);
            graphics.FillRectangle(b, MainControlArea);
            b.Dispose();

            ControlPaint.DrawBorder(graphics, MainControlArea,
            _defForeColor, 2, ButtonBorderStyle.Solid,    //left
            _defForeColor, 2, ButtonBorderStyle.Solid,    //top
            _defForeColor, 2, ButtonBorderStyle.Solid,    //right
            _defForeColor, 2, ButtonBorderStyle.Solid);   //bottonm
        }

        ///////////////////////////////////////// Control Buttons /////////////////////////////////////////////
        private void btnMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Bot.cancelBtnClicked = true;
            Application.Exit();
        }

        ////////////////////////////////////////// Ouathtoken Link ////////////////////////////////////////////
        private void llblOauthTokenHelp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LinkLabel.Link link = new LinkLabel.Link();
            link.LinkData = "http://twitchapps.com/tmi/";

            Process.Start(link.LinkData as string);
        }

        #endregion frmMain Design

        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////// general functions //////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////

        #region general functions (fore more tabs)

        /* TabControl_Deselected(Event)
		 * if the user leaves the tab the settings will get restored
		 * so any unsaved changes will get lost
		 */

        private void TabControl_Deselected(object sender, TabControlEventArgs e)
        {
            if (initialSelecting)
            {
                initialSelecting = false;
                return;
            }
            if (!changedSettings)
            {
                return;
            }
            else
            {
                DialogResult save = MessageBox.Show("Du hast noch ungespeicherte Änderungen! Möchtest du diese speichern?", "Änderungen speichern?", MessageBoxButtons.YesNo);
                if (e.TabPage.Name == "tpSetup")
                {
                    if (save == DialogResult.No)
                    {
                        restoreStreamSettings();
                    }
                    else
                    {
                        saveSettings(btnStreamSave);
                    }
                }
                else if (e.TabPage.Name == "tpRewardsystem")
                {
                    if (save == DialogResult.No)
                    {
                        restoreBasicSettings();
                        restoreViewerrankings();
                    }
                    else
                    {
                        saveSettings(btnDgvSave);
                        saveSettings(btnBSSave);
                    }
                }
                changedSettings = false;
            }
        }

        /* checkForEmptySettings() =>
		 * This method goes through the given groupbox and checks if there are any
		 * empty settings and sets the related bool variable accordingly
		 */

        public void checkForEmptySettings(gb box)
        {
            //gbViewerrankings
            if (box.Name == "gbViewerrankings")
            {
                if (dgvViewerankings.Rows.Count == 0)
                {
                    missingViewerrankings = true;
                    return;
                }
                else
                {
                    missingViewerrankings = false;
                }
            }
            //gbStream or gbBasicSettings
            foreach (Control c in box.Controls)
            {
                if (c is tb && ((tb)c).Text == "" && !(c.AccessibleName == "DBon") && !(c.AccessibleName == "DBoff")) //TODO dont forget to remove that later)
                {
                    if (box.Name == "gbStream")
                    {
                        missingStream = true;
                    }
                    else if (box.Name == "gbBasicSettings")
                    {
                        missingBasicSettings = true;
                    }
                }
                else if (c is tb_int && ((tb_int)c).Text == "")
                {
                    if (box.Name == "gbStream")
                    {
                        missingStream = true;
                    }
                    else if (box.Name == "gbBasicSettings")
                    {
                        missingBasicSettings = true;
                    }
                }
                else if (c is ComboBox)
                {
                    if (((ComboBox)c).SelectedValue != null && ((ComboBox)c).SelectedValue.ToString() == "")
                    {
                        if (box.Name == "gbStream")
                        {
                            missingStream = true;
                        }
                        else if (box.Name == "gbBasicSettings")
                        {
                            missingBasicSettings = true;
                        }
                    }
                }
            }
            if (box.Name == "gbBasicSettings" && !alreadySaveBasicSettings)
            {
                missingBasicSettings = true;
            }
        }

        /* searchForEmptySettings() =>
		 * goes through each element of the given groupbox
		 * and saves the name for every empty setting
		 * return: string array with name of empty settings
		 */

        public string[] searchForEmptySetting(gb box)
        {
            string[] missing = new string[box.Controls.Count];
            int pointer = 0;
            foreach (Control c in box.Controls)
            {
                if (c is tb)
                {
                    if (((tb)c).Text == "" && !(c.AccessibleName == "DBon") && !(c.AccessibleName == "DBoff")) //TODO dont forget to remove that later
                    {
                        missing[pointer] = ((tb)c).AccessibleName;
                        pointer++;
                    }
                }
                else if (c is tb_int)
                {
                    if (((tb_int)c).Text == "")
                    {
                        missing[pointer] = ((tb_int)c).AccessibleName;
                        pointer++;
                    }
                }
                else if (c is ComboBox)
                {
                    if (((ComboBox)c).SelectedItem.ToString() == "")
                    {
                        missing[pointer] = ((ComboBox)c).AccessibleName;
                        pointer++;
                    }
                }
            }
            return missing;
        }

        /* arrToString() =>
		 * returns a string of each string in the array
		 * formatted as list in a messagebox
		 * to display all missing empty settings
		 */

        public string arrToString(string[] arr)
        {
            string res = "";
            foreach (string s in arr)
            {
                if (s != null)
                {
                    res += s != "" ? " - " + s + Environment.NewLine : "";
                }
            }
            return res;
        }

        #endregion general functions (fore more tabs)

        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////// Settings ///////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////

        #region Settings

        //////////////////////////////////////////// Save or Reset ////////////////////////////////////////////

        #region save or reset

        /* btnBSSave_Click(Event)
		 * handles the click of a "save" button in one of the setting tabs (Stream, Rewardsystem)
		 * and saves the settings into the setting variables
		 * but only does it for the given groupbox (sender.parent)
		 */

        private void btnBSSave_Click(object sender, EventArgs e)
        {
            saveSettings(sender);
        }

        public void saveSettings(object sender)
        {
            Button b = sender as Button;
            string bs = "";
            gb parent = b.Parent as gb;
            parent.Update();
            checkForEmptySettings(parent);
            Dictionary<int, Control> ctrl = new Dictionary<int, Control>();
            bool isDgv = false;
            Control.ControlCollection coll = b.Parent.Controls;
            foreach (Control r in coll)
            {
                if (((r is tb) || (r is ComboBox)) || (r is tb_int))
                {
                    ctrl.Add(r.TabIndex, r);
                }
                else if (r is DataGridView)
                {
                    isDgv = true;
                    DataGridView d = (DataGridView)r;
                    int dCount = d.RowCount;
                    for (int i = 0; i < dCount; i++)
                    {
                        bs += $"{d[0, i].Value}|{d[1, i].Value}|{d[2, i].Value}#";
                    }
                    bs = (bs.Length >= 1) ? bs.Substring(0, bs.Length - 1) : "";
                    break;
                }
            }

            if (!isDgv)
            {
                for (int i = 0; i < ctrl.Count; i++)
                {
                    if (ctrl[i] is tb || ctrl[i] is tb_int)
                    {
                        bs += ctrl[i].Text + "#";
                        if (((gb)b.Parent) == gbStream)
                        {
                            if (((tb)ctrl[i]).TabIndex == 0)
                            {
                                IrcClient.streamchannel = ((tb)ctrl[i]).Text;
                            }
                            if (((tb)ctrl[i]).TabIndex == 1)
                            {
                                IrcClient.botname = ((tb)ctrl[i]).Text;
                            }
                            if (((tb)ctrl[i]).TabIndex == 2)
                            {
                                IrcClient.token = ((tb)ctrl[i]).Text;
                            }
                        }
                    }
                    else if (ctrl[i] is ComboBox)
                    {
                        ((ComboBox)ctrl[i]).Update();
                        if (((ComboBox)ctrl[i]).SelectedIndex != -1)
                        {
                            bs += string.IsNullOrEmpty(((ComboBox)ctrl[i]).SelectedItem.ToString()) ? "#" : ((ComboBox)ctrl[i]).SelectedItem.ToString() + "#";
                        }
                        else
                        {
                            bs += "#";
                        }
                    }
                }
                bs = bs.Substring(0, bs.Length - 1);
            }

            switch (b.Parent.Name.ToString())
            {
                case "gbStream":
                    Settings.Default.gbStream = bs;
                    Settings.Default.Save();
                    Bot.commandPre.updateInfo();
                    alreadySaveStream = true;
                    changedSettings = false;
                    break;

                case "gbBasicSettings":
                    Settings.Default.gbBasicSettings = bs;
                    Settings.Default.defaultBasicSettings = false;
                    Settings.Default.Save();
                    alreadySaveBasicSettings = true;
                    CommandPresetDB.updateCurrencyCommandName(tbCurrencyCommand.Text.Substring(1));
                    btnViewerGiveCurrency.Text = $"Zuschauer {Settings.Default.gbBasicSettings.Split('#')[0]} geben";
                    changedSettings = false;
                    break;

                case "gbViewerrankings":
                    Settings.Default.gbViewerrankings = bs;
                    Settings.Default.Save();
                    alreadySaveViewerrankings = true;
                    foreach (Viewer v in Bot.viewer.list)
                    {
                        ViewerDB.updateRank(v);
                    }
                    changedSettings = false;
                    break;

                default:
                    Console.WriteLine($"Fehler bei BSSave_Click {b.Parent.Name}");
                    break;
            }
        }

        /* btnBSCancel_Click(Event)
		 * handles the click of a "cancel" button in one of the setting tabs (Stream, Rewardsystem)
		 * and restores the settings from the setting variables
		 * but only does it for the given groupbox (sender.parent)
		 */

        private void btnBSCancel_Click(object sender, EventArgs e)
        {
            gb gb = (gb)((Button)sender).Parent;
            switch (gb.Name)
            {
                case "gbStream":
                    restoreStreamSettings();
                    break;

                case "gbBasicSettings":
                    restoreBasicSettings();
                    break;

                case "gbViewerrankings":
                    restoreViewerrankings();
                    break;

                default:
                    Console.WriteLine($"Müll programmiert! - {gb.Name} beim resetten");
                    break;
            }
        }

        /* tbToken_Leave(Event)
		 * automatically checks if the token has the correct format
		 * and opens a messagebox if it is wrong
		 */

        private void tbToken_Leave(object sender, EventArgs e)
        {
            if (!((tb)sender).Text.StartsWith("oauth:") && ((tb)sender).Text != "" && (!(ActiveControl == btnStreamCancel) == (!(ActiveControl == btnClose))))
            {
                MessageBox.Show("Bitte gebe einen gültigen Token ein!");
                ((tb)sender).Select();
                TabControl.SelectTab(0);
            }
        }

        #endregion save or reset

        //////////////////////////////////////////// restore functions ////////////////////////////////////////

        #region restore functions

        /* restoreStreamSettings() =>
		 * loads the settings for the groupbox stream (tab: stream)
		 * splits it up
		 * and enters the settings into the related controls
		 */

        public void restoreStreamSettings()
        {
            string[] ss = Settings.Default.gbStream.Split('#');
            foreach (Control ctrl in gbStream.Controls)
            {
                if (ctrl is tb)
                {
                    ((tb)ctrl).Text = ss[((tb)ctrl).TabIndex].TrimStart();
                }
            }
            gbStream.Update();
        }

        /* restoreBasicSettings() =>
		 * loads the settings for the basicSettings groupbox (tab: rewardsystem)
		 * splits it up
		 * and enters the settings into the related controls
		 */

        public void restoreBasicSettings()
        {
            string[] bs = Settings.Default.gbBasicSettings.Split('#');
            foreach (Control ctrl in gbBasicSettings.Controls)
            {
                if (ctrl is tb)
                {
                    if (((tb)ctrl).TabIndex == 0)
                    {
                        _triggerAutoChange = false;
                        ((tb)ctrl).Text = bs[((tb)ctrl).TabIndex].TrimStart();
                        _triggerAutoChange = true;
                    }
                    else
                    {
                        ((tb)ctrl).Text = bs[((tb)ctrl).TabIndex].TrimStart();
                    }
                }
                else if (ctrl is tb_int)
                {
                    ((tb_int)ctrl).Text = bs[((tb_int)ctrl).TabIndex].TrimStart();
                }
                else if (ctrl is ComboBox)
                {
                    ((ComboBox)ctrl).SelectedItem = bs[((ComboBox)ctrl).TabIndex].TrimStart();
                }
            }
            gbBasicSettings.Update();
        }

        /* restoreViewerrankings() =>
		 * loads the ranks for the viewerrankings (tab: rewardsystem)
		 * splits it up into the rows
		 * and enters the rows into the datagridview
		 */

        public void restoreViewerrankings()
        {
            string[] vrs = Settings.Default.gbViewerrankings.Split('#');
            dgvViewerankings.Rows.Clear();
            foreach (string row in vrs)
            {
                if (row != "")
                {
                    string[] split = row.Split('|');
                    dgvViewerankings.Rows.Add(split[0].TrimStart(), split[1].TrimStart(), split[2].TrimStart());
                }
            }
            gbViewerrankings.Update();
        }

        #endregion restore functions

        //////////////////////////////////////////// lower case ///////////////////////////////////////////////

        #region lower case

        /* lower Case(Event)
		 * automatically changes the channelnames to lowercase
		 * just in case someone starts with uppercase
		 * so that there won't be an error when entering the twitch chat
		 */

        private void tbStreamchannel_Leave(object sender, EventArgs e)
        {
            ((tb)sender).Text = (((tb)sender).Text).ToLower();
        }

        private void tbBotchannel_Leave(object sender, EventArgs e)
        {
            ((tb)sender).Text = (((tb)sender).Text).ToLower();
        }

        #endregion lower case

        #endregion Settings

        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////// Rewardsettings ////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////

        #region Rewardsettings

        ////////////////////////////////////////// Basic Settings /////////////////////////////////////////////

        #region gbBasicSettings

        /* tbCurrencyName(Event)
		 * function to set the commandname for the currency command
		 * to the same name as the currency
		 * user is able to change the commandname if he wants without
		 * changing the currency name
		 */

        private void tbCurrencyName_TextChanged(object sender, EventArgs e)
        {
            if (_triggerAutoChange)
            {
                tbCurrencyCommand.Text = "!" + ((tb)sender).Text.ToLower();
                lblCurrency2.Text = ((tb)sender).Text;
                lblGiveawayMinInputCurrency.Text = ((tb)sender).Text;
            }
        }

        /* tbCurrencyCommand(Event)
		 * ensures that the content of this textbox always starts with "!"
		 * so that it will be recognized as a command after saving it
		 */

        private void tbCurrencyCommand_TextChanged(object sender, EventArgs e)
        {
            tb t = sender as tb;
            if (!t.Text.StartsWith("!"))
            {
                t.Text = t.Text.Insert(0, "!");
                t.SelectionStart = 1;
                t.SelectionLength = 0;
            }
        }

        #endregion gbBasicSettings

        ////////////////////////////////////////// Viewerranking //////////////////////////////////////////////

        #region gbViewerrankings

        /* dgvViewerrankings_CellPainting(Event)
		 * draws the border of the cells and
		 * the complete dgv in the style of the
		 * bot design
		 */

        private void dgvViewerankings_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            SolidBrush back = new SolidBrush(_defBackColor);
            SolidBrush border = new SolidBrush(_defForeColor);
            Pen pBorder = new Pen(border);

            e.PaintBackground(e.ClipBounds, true);
            e.PaintContent(e.ClipBounds);
            e.Graphics.DrawLine(pBorder, new Point(e.CellBounds.Left, e.CellBounds.Bottom), new Point(e.CellBounds.Right, e.CellBounds.Bottom));
            e.Graphics.DrawLine(pBorder, new Point(e.CellBounds.Right, e.CellBounds.Top), new Point(e.CellBounds.Right, e.CellBounds.Bottom));
            e.Handled = true;
        }

        /*btnRank_Click(Event)
		 * opens a new form in which the currently
		 * selected row from the dgv is displayed
		 * and can be changed
		 */

        private void btnRank_Click(object sender, EventArgs e)
        {
            if (!(((Button)sender).Text.Contains("ändern") && dgvViewerankings.RowCount > 0))
            {
                frmRankEdit edit = new frmRankEdit((Button)sender);
                edit.Show();
            }
        }

        /* btnRank_Delete(Event)
		 * deletes the currently selected row from the dgv
		 */

        private void btnRankDelete_Click(object sender, EventArgs e)
        {
            dgvViewerankings.Rows.Remove(dgvViewerankings.SelectedRows[0]);
            sortDgvViewerrankings();
            changedSettings = true;
        }

        public void sortDgvViewerrankings()
        {
            List<object[]> sortedViewerrankings = new List<object[]>();
            int rowsLeft = dgvViewerankings.Rows.Count;
            while (rowsLeft != 0)
            {
                object[] maxValue = null;
                int rowMaxValue = 0;

                for (int i = 0; i < rowsLeft; i++)
                {
                    if (maxValue == null)
                    {
                        maxValue = new object[] { dgvViewerankings.Rows[i].Cells[0].Value, dgvViewerankings.Rows[i].Cells[1].Value, dgvViewerankings.Rows[i].Cells[2].Value };
                        rowMaxValue = i;
                    }
                    else
                    {
                        if (!dgvAscending && Convert.ToInt32(maxValue[1]) < Convert.ToInt32(dgvViewerankings.Rows[i].Cells[1].Value))
                        {
                            maxValue = new object[] { dgvViewerankings.Rows[i].Cells[0].Value, dgvViewerankings.Rows[i].Cells[1].Value, dgvViewerankings.Rows[i].Cells[2].Value };
                            rowMaxValue = i;
                        }
                        else if (dgvAscending && Convert.ToInt32(maxValue[1]) > Convert.ToInt32(dgvViewerankings.Rows[i].Cells[1].Value))
                        {
                            maxValue = new object[] { dgvViewerankings.Rows[i].Cells[0].Value, dgvViewerankings.Rows[i].Cells[1].Value, dgvViewerankings.Rows[i].Cells[2].Value };
                            rowMaxValue = i;
                        }
                    }
                }
                sortedViewerrankings.Add(maxValue);
                dgvViewerankings.Rows.RemoveAt(rowMaxValue);
                rowsLeft--;
            }
            dgvViewerankings.Rows.Clear();
            foreach (object[] o in sortedViewerrankings)
            {
                dgvViewerankings.Rows.Add(o[0], o[1], o[2]);
            }
        }

        #endregion gbViewerrankings

        #endregion Rewardsettings

        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////// Chat //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////

        #region chat

        //private void btnMessageSend_Click(object sender, EventArgs e)
        //	{
        //	Bot.streamer.sendChatMessage(tbMessageSend.Text);
        //	tbChat.Text += $"{Settings.Default.gbStream.Split('#')[0]}: {tbMessageSend.Text}";
        //	}

        #region basic buttons

        /* btnConnect_Click(Event)
		 * checks if there are any settings missing/left empty or on default
		 * if found:
		 * list up all missing settings in a messagebox
		 * and after closing the messagebox opens the tab where
		 * settings are missing (prioritizes tabs from left to right)
		 * if no settings are missing:
		 * nes new irc client and starts connecting to it
		 */

        private void btnConnect_Click(object sender, EventArgs e)
        {
            btnDisconnect.Enabled = true;
            btnConnect.Enabled = false;
            tbMessageSend.Enabled = true;
            btnMessageSend.Enabled = true;
            checkForEmptySettings(gbStream);
            checkForEmptySettings(gbBasicSettings);
            checkForEmptySettings(gbViewerrankings);
            string dot = WebUtility.HtmlEncode("&#x95"); //TODO how to display list dot

            if (Settings.Default.defaultBasicSettings)
            {
                TabControl.SelectTab(1);
                DialogResult defBS = MessageBox.Show("Momentan sind noch die Standardeinstellungen gesetzt! Möchtest du sie nicht ändern?", "Standardeinstellungen?", MessageBoxButtons.YesNo);
                if (defBS == DialogResult.No)
                {
                    TabControl.SelectTab(2);
                    Thread.Sleep(200);
                }
                Settings.Default.defaultBasicSettings = false;
                Settings.Default.Save();
            }
            if (missingStream || missingViewerrankings || missingBasicSettings)
            {
                string[] mBs = missingBasicSettings ? searchForEmptySetting(gbBasicSettings) : null;
                string[] mS = missingStream ? searchForEmptySetting(gbStream) : null;
                string[] mV = missingViewerrankings ? searchForEmptySetting(gbViewerrankings) : null;
                string miss = "Es fehlen folgende Einstellungen " + Environment.NewLine;
                miss += (missingBasicSettings) ? dot + "Basic Settings " + Environment.NewLine + arrToString(mBs) + Environment.NewLine : "";
                miss += (missingStream) ? "Stream: " + Environment.NewLine + arrToString(mS) + Environment.NewLine : "";
                miss += (missingViewerrankings) ? "Viewerrankings " + Environment.NewLine + arrToString(mV) + Environment.NewLine : "";
                miss += "Bitte trage diese Sachen ein!";
                MessageBox.Show(miss, "Einstellungen fehlen!");
                if (missingStream)
                {
                    TabControl.SelectTab(0);
                }
                else if (missingBasicSettings || missingViewerrankings)
                {
                    TabControl.SelectTab(1);
                }
            }
            else
            {
                Bot.irc.login("irc.chat.twitch.tv", 6667);
                if (IrcClient.tcp.Connected)
                {
                    Bot.irc.joinRoom();
                    Console.WriteLine("Bot has joined the chat");
                    enableAllOnlineFunctionButtons();
                    btnDisconnect.Enabled = true;
                    btnConnect.Enabled = false;
                }
            }
        }

        /* btnDisconnect_Click(Event)
		 * tries to leave the irc channel via message command
		 * if failed, removes the irc client (null) and cancel
		 * the receive message task
		 */

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            if (IrcClient.tcp.Connected)
            {
                try
                {
                    Bot.irc.sendChatMessage("Vielen Dank fürs zuschauen! Bis zum nächsten Stream!");
                    Bot.irc.leaveRoom();
                    Console.WriteLine("Bot has left the chat");
                }
                catch (WebException)
                {
                    Bot.sourceIrc.Cancel();
                    IrcClient.tcp.Close();
                }
                finishAllRunningFunctions();
                btnConnect.Enabled = true;
                btnDisconnect.Enabled = false;
            }
        }

        public void enableAllOnlineFunctionButtons()
        {
            //TODO uncomment after implementing
            //btnMessageSend.Invoke((MethodInvoker) (() => btnMessageSend.Enabled = true));
            btnViewerWarning.Invoke((MethodInvoker)(() => btnViewerWarning.Enabled = true));
            btnViewerTimeout.Invoke((MethodInvoker)(() => btnViewerTimeout.Enabled = true));
            btnViewerBan.Invoke((MethodInvoker)(() => btnViewerBan.Enabled = true));
            btnBlacklist.Invoke((MethodInvoker)(() => btnBlacklist.Enabled = true));
            btnSpam.Invoke((MethodInvoker)(() => btnSpam.Enabled = true));
            btnUndefined.Invoke((MethodInvoker)(() => btnUndefined.Enabled = true));
            btnBetCurrentStart.Invoke((MethodInvoker)(() => btnBetCurrentStart.Enabled = true));
            btnPollCurrentStart.Invoke((MethodInvoker)(() => btnPollCurrentStart.Enabled = true));
            btnGiveawayCurrentStart.Invoke((MethodInvoker)(() => btnGiveawayCurrentStart.Enabled = true));
        }

        public void finishAllRunningFunctions()
        {
            //TODO uncomment after implementing
            //btnMessageSend.Invoke((MethodInvoker) (() => btnMessageSend.Enabled = false));
            btnViewerWarning.Invoke((MethodInvoker)(() => btnViewerWarning.Enabled = false));
            btnViewerTimeout.Invoke((MethodInvoker)(() => btnViewerTimeout.Enabled = false));
            btnViewerBan.Invoke((MethodInvoker)(() => btnViewerBan.Enabled = false));
            btnBlacklist.Invoke((MethodInvoker)(() => btnBlacklist.Enabled = false));
            btnSpam.Invoke((MethodInvoker)(() => btnSpam.Enabled = false));
            btnUndefined.Invoke((MethodInvoker)(() => btnUndefined.Enabled = false));
            btnBetCurrentStart.Invoke((MethodInvoker)(() => btnBetCurrentStart.Enabled = false));
            btnPollCurrentStart.Invoke((MethodInvoker)(() => btnPollCurrentStart.Enabled = false));
            btnGiveawayCurrentStart.Invoke((MethodInvoker)(() => btnGiveawayCurrentStart.Enabled = false));
        }

        #endregion basic buttons

        private void btnGiveViewerCurrency_Click(object sender, EventArgs e)
        {
            frmRankEdit giveUser = new frmRankEdit((Button)sender);
            giveUser.lblRankName.Text = "Viewer";
            giveUser.lblPointsRequired.Text = "Punkte";
            giveUser.Show();
        }

        #endregion chat

        #region commands

        public void updateCommandlistdisplay()
        {
            string display = "";
            if (lbCommandlist.InvokeRequired)
            {
                lbCommandlist.Invoke((MethodInvoker)(() => lbCommandlist.Items.Clear()));
            }
            else
            {
                lbCommandlist.Items.Clear();
            }
            foreach (Command c in Bot.command.list)
            {
                display = c.name;
                if (c.alias[0] != "")
                {
                    display += " (";
                    foreach (string al in c.alias)
                    {
                        if (al != "")
                        {
                            display += al + ", ";
                        }
                    }
                    display = display.Remove(display.Length - 2);
                    display += ")";
                }
                if (lbCommandlist.InvokeRequired)
                {
                    lbCommandlist.Invoke((MethodInvoker)(() => lbCommandlist.Items.Add(display)));
                }
                else
                {
                    lbCommandlist.Items.Add(display);
                }
            }
        }

        private void lbCommandlist_DoubleClick(object sender, EventArgs e)
        {
            selectCommandInList();
        }

        private void btnEditSelectedCommand_Click(object sender, EventArgs e)
        {
            selectCommandInList();
        }

        private void selectCommandInList()
        {
            clear();
            if (lbCommandlist.SelectedItem == null)
            {
                return;
            }
            if (lbCommandlist.SelectedItem.ToString() != "")
            {
                string cmdName = lbCommandlist.SelectedItem.ToString();
                if (cmdName.Contains("("))
                {
                    cmdName = cmdName.Substring(0, cmdName.IndexOf("(")).Trim();
                }
                foreach (Command searchC in Bot.command.list)
                {
                    if (searchC.name == cmdName)
                    {
                        editedCommand = searchC;
                        tbCreateCommandName.Text = searchC.name;
                        tbCreateCommandMessage.Text = searchC.message;
                        int cd = searchC.cooldown / 1000;
                        if (cd < 60)
                        {
                            tbCreateCommandCooldown.Text = "" + cd;
                            cbCreateCommandUnit.SelectedItem = "Sekunden";
                        }
                        else if (cd >= 60 && cd < 3600)
                        {
                            tbCreateCommandCooldown.Text = "" + (cd / 60);
                            cbCreateCommandUnit.SelectedItem = "Minuten";
                        }
                        cbCreateCommandUserlvl.SelectedItem = searchC.executionerLevel;
                        if (searchC.alias[0] != "")
                        {
                            foreach (string al in searchC.alias)
                            {
                                if (al != "")
                                {
                                    tbCreateCommandAlias.Text += al + Environment.NewLine;
                                }
                            }
                        }
                        chkbCreateCommandEditable.Checked = (searchC.editable) ? true : false;
                        btnCommandSave.Enabled = (searchC.editable) ? true : false;
                        btnCommandSave.Text = "Speichern";
                        gbCreateCommand.Text = "Bearbeiten";
                    }
                }
            }
            else
            {
                MessageBox.Show("Bitte wähle zuerst einen Command aus!");
            }
        }

        private void btnCommandSave_Click(object sender, EventArgs e)
        {
            Bot.UIinput = true;
            bool missingName = (tbCreateCommandName.Text == "") ? true : false;
            bool missingMessage = (tbCreateCommandMessage.Text == "") ? true : false;
            bool missingCooldown = (tbCreateCommandCooldown.Text == "") ? true : false;
            bool missingUnit = (cbCreateCommandUnit.SelectedItem == null) ? true : false;
            bool missingLvl = (cbCreateCommandUserlvl.SelectedItem == null) ? true : false;
            if (!missingName && !missingMessage && !missingCooldown && !missingUnit && !missingLvl)
            {
                string n = tbCreateCommandName.Text;
                bool chk = chkbCreateCommandEditable.Checked;
                int lvl = 3;
                if (cbCreateCommandUserlvl.SelectedItem.ToString() == "Streamer")
                {
                    lvl = 0;
                }
                else if (cbCreateCommandUserlvl.SelectedItem.ToString() == "Mod")
                {
                    lvl = 1;
                }
                else if (cbCreateCommandUserlvl.SelectedItem.ToString() == "Subscriber")
                {
                    lvl = 2;
                }
                string m = tbCreateCommandMessage.Text;
                string[] aliasFromTb = tbCreateCommandAlias.Text.Split();
                int cd = (cbCreateCommandUnit.SelectedItem.ToString() == "Minuten") ? Convert.ToInt32(tbCreateCommandCooldown.Text) * 60000 : Convert.ToInt32(tbCreateCommandCooldown.Text) * 1000;

                if (((Button)sender).Text == "Erstellen")
                {
                    Bot.command.createCommand(n, chk, 0, m, aliasFromTb, cd);
                    if (!failed)
                    {
                        clear();
                        failed = false;
                    }
                }
                else if (((Button)sender).Text == "Speichern")
                {
                    Bot.command.editCommand(editedCommand, n, chk, lvl, m, aliasFromTb, cd);
                    if (!failed)
                    {
                        ((Button)sender).Text = "Erstellen";
                        clear();
                        failed = false;
                    }
                }
                updateCommandlistdisplay();
            }
            else
            {
                string message = "Du hast noch nicht: " + Environment.NewLine;
                message += (missingName) ? "Commandname, " + Environment.NewLine : "";
                message += (missingMessage) ? "Commandnachricht, " + Environment.NewLine : "";
                message += (missingCooldown) ? "Cooldown, " + Environment.NewLine : "";
                message += (missingUnit) ? "Zeiteinheit für Cooldown, " + Environment.NewLine : "";
                message += (missingLvl) ? "Benutzerlevel, " + Environment.NewLine : "";
                message = message.Remove(message.Length - 4);
                message += Environment.NewLine + "festgelegt!";
                MessageBox.Show(message);
            }
            Bot.UIinput = false;
        }

        private void btnCommandCancel_Click(object sender, EventArgs e)
        {
            clear();
        }

        //this functions resets the create/edit groupbox in the command tab so that every textbox/option is at its default state
        private void clear()
        {
            foreach (Control c in gbCreateCommand.Controls)
            {
                if (c is tb)
                {
                    ((tb)c).Text = "";
                }
                else if (c is tb_int)
                {
                    ((tb_int)c).Text = "";
                }
                else if (c is ComboBox)
                {
                    ((ComboBox)c).SelectedIndex = 0;
                }
            }
            chkbCreateCommandEditable.Checked = true;
            btnCommandSave.Text = "Erstellen";
            gbCreateCommand.Text = "Erstellen";
        }

        private void btnDeleteSelectedCommand_Click(object sender, EventArgs e)
        {
            string cmdName = "";
            if (lbCommandlist.SelectedItem.ToString() != "")
            {
                if (lbCommandlist.SelectedItem.ToString().Contains("("))
                {
                    cmdName = lbCommandlist.SelectedItem.ToString().Substring(0, lbCommandlist.SelectedItem.ToString().IndexOf("(")).Trim();
                }
                else
                {
                    cmdName = lbCommandlist.SelectedItem.ToString().Trim();
                }
                foreach (Command searchC in Bot.command.list)
                {
                    if (searchC.name == cmdName)
                    {
                        Bot.command.deleteCommand(searchC);
                        clear();
                        break;
                    }
                }
            }
            updateCommandlistdisplay();
        }

        private void chkbCreateCommandEditable_CheckedChanged(object sender, EventArgs e)
        {
            if (chkbCreateCommandEditable.Checked && btnCommandSave.Text == "Speichern")
            {
                btnCommandSave.Enabled = true;
            }
            else if (!chkbCreateCommandEditable.Checked && btnCommandSave.Text == "Speichern")
            {
                btnCommandSave.Enabled = false;
            }
            else
            {
                btnCommandSave.Enabled = true;
            }
        }

        #endregion commands

        #region quotes

        private void lbQuotes_DoubleClick(object sender, EventArgs e)
        {
            selectQuote();
        }

        private void btnEditSelectedQuote_Click(object sender, EventArgs e)
        {
            selectQuote();
        }

        private void selectQuote()
        {
            if (lbQuotes.Text != "")
            {
                int number = lbQuotes.SelectedIndex;
                string quote = lbQuotes.SelectedItem.ToString();
                if (quote.Length > 5)
                {
                    quote = quote.Substring(quote.IndexOf("-") + 1, quote.IndexOf("(") - (quote.IndexOf("-") + 1)).Trim();
                    tbEditQuoteText.Text = quote;
                }
                else
                {
                    tbEditQuoteText.Text = " ";
                }
            }
            else
            {
                MessageBox.Show("Bitte wähle zuerst ein Zitat aus!");
            }
        }

        private void btnCreateQuote_Click(object sender, EventArgs e)
        {
            if (tbCreateQuoteText.Text != "")
            {
                Bot.quote.addQuote(Settings.Default.gbStream.Split('#')[0], tbCreateQuoteText.Text, (StreamData.game) == null ? "" : StreamData.game);
                updateQuoteDisplay();
                tbEditQuoteText.Text = "";
            }
        }

        private void btnEditQuoteFinished_Click(object sender, EventArgs e)
        {
            if (tbEditQuoteText.Text != "")
            {
                int number = Convert.ToInt32(lbQuotes.Text.Substring(1, 1));
                Bot.quote.editQuote(number, tbEditQuoteText.Text);
                updateQuoteDisplay();
                tbEditQuoteText.Text = "";
            }
        }

        private void btnDeleteSelectedQuote_Click(object sender, EventArgs e)
        {
            int number = Convert.ToInt32(lbQuotes.Text.Substring(1, 1));
            Bot.quote.deleteQuote(number);
            updateQuoteDisplay();
        }

        private void updateQuoteDisplay()
        {
            lbQuotes.Items.Clear();
            foreach (KeyValuePair<int, Quote> q in Bot.quote.quotes)
            {
                if (q.Value.text != " ")
                {
                    lbQuotes.Items.Add($"#{q.Key} - {q.Value.text} ({q.Value.viewer}, {q.Value.game})");
                }
                else
                {
                    lbQuotes.Items.Add($"#{q.Key} - ");
                }
            }
        }

        #endregion quotes

        #region bet

        private void lbRecentBets_DoubleClick(object sender, EventArgs e)
        {
            Bet selectedBet = null;
            string n = "";
            if (lbRecentBets.SelectedItem == null)
            {
                return;
            }
            if (lbRecentBets.SelectedItem.ToString() != "")
            {
                n = lbRecentBets.SelectedItem.ToString();
                n = n.Remove(lbRecentBets.SelectedItem.ToString().IndexOf('(')).Trim();
            }
            foreach (Bet b in Bet.recentBets)
            {
                if (b.name == n)
                {
                    selectedBet = b;
                    break;
                }
            }
            if (tbBetCurrent.InvokeRequired)
            {
                tbBetCurrent.Invoke((MethodInvoker)(() => tbBetCurrent.Text = $"Name: {selectedBet.name} {Environment.NewLine} Wette: {selectedBet.question}{Environment.NewLine}Gewinn: {selectedBet.reward}{Environment.NewLine}Mindesteinsatz: {selectedBet.minStakeAmount} {Environment.NewLine}Bonus: " + ((selectedBet.bonus) ? "Additiv" : "Prozentual") + $"{Environment.NewLine}Subbonus: " + ((selectedBet.subBonus) ? $"Ja ({selectedBet.subBonusPerc})" : "Nein") + $"{Environment.NewLine}Dauer: " + ((selectedBet.duration == -1) ? "Beendet durch Streamer" : $"{selectedBet.duration}")));
            }
            else
            {
                tbBetCurrent.Text = $"Name: {selectedBet.name} {Environment.NewLine} Wette: {selectedBet.question}{Environment.NewLine}Gewinn: {selectedBet.reward}{Environment.NewLine}Mindesteinsatz: {selectedBet.minStakeAmount} {Environment.NewLine} Bonus: " + ((selectedBet.bonus) ? "Additiv" : "Prozentual") + $"{Environment.NewLine} Subbonus: " + ((selectedBet.subBonus) ? $"Ja ({selectedBet.subBonusPerc})" : "Nein") + $"{Environment.NewLine}Dauer: " + ((selectedBet.duration == -1) ? "Beendet durch Streamer" : $"{selectedBet.duration}");
            }
            Bet.currentDisplayedBet = selectedBet;
            Bot.main.btnBetCurrentEdit.Enabled = true;
        }

        private void btnBetCurrentStart_Click(object sender, EventArgs e)
        {
            Bet.start(Bet.currentDisplayedBet);
        }

        private void btnBetCurrentStop_Click(object sender, EventArgs e)
        {
            Bet.awaitStop.Set();
        }

        private void btnBetCurrentWon_Click(object sender, EventArgs e)
        {
            Bet.activeBet.won = true;
            Bet.awaitBetResult.Set();
        }

        private void btnBetCurrentLost_Click(object sender, EventArgs e)
        {
            Bet.activeBet.won = false;
            Bet.awaitBetResult.Set();
        }

        private void btnBetCurrentEdit_Click(object sender, EventArgs e)
        {
            gbBet.Text = "Bearbeiten";
            btnBetCreate.Text = "Speichern";
            tbBetName.Text = Bet.currentDisplayedBet.name;
            tbBetText.Text = Bet.currentDisplayedBet.question;
            tbBetWin.Text = Bet.currentDisplayedBet.reward.ToString();
            cbBetWin.SelectedItem = (Bet.currentDisplayedBet.bonus) ? "Additiv" : "Prozentual";
            chkbBetMinInput.Checked = (Bet.currentDisplayedBet.minStakeAmount == 0) ? false : true;
            tbBetMinInput.Text = Bet.currentDisplayedBet.minStakeAmount.ToString();
            chkbBetSubbonus.Checked = Bet.currentDisplayedBet.subBonus;
            tbBetSubbonusAmount.Text = Bet.currentDisplayedBet.subBonusPerc.ToString();
            chkbBetDuration.Checked = (Bet.currentDisplayedBet.duration != -1) ? true : false;
            int duration = (Bet.currentDisplayedBet.duration < 60000) ? Bet.currentDisplayedBet.duration / 1000 : Bet.currentDisplayedBet.duration / 60000;
            tbBetDuration.Text = duration.ToString();
            cbBetDurationUnit.SelectedItem = (Bet.currentDisplayedBet.duration < 60000) ? "Sekunden" : "Minuten";
        }

        private void btnBetCancel_Click(object sender, EventArgs e)
        {
            clearCreateBet();
        }

        private void clearCreateBet()
        {
            foreach (Control c in gbBet.Controls)
            {
                if (c is tb)
                {
                    ((tb)c).Text = "";
                }
                else if (c is tb_int)
                {
                    ((tb_int)c).Text = "";
                }
                else if (c is CheckBox)
                {
                    ((CheckBox)c).Checked = false;
                }
                else if (c is ComboBox)
                {
                    ((ComboBox)c).SelectedIndex = 0;
                }
            }
        }

        private void btnBetCreate_Click(object sender, EventArgs e)
        {
            bool missingName = (tbBetName.Text == "") ? true : false;
            bool missingText = (tbBetText.Text == "") ? true : false;
            bool missingWin = (tbBetWin.Text == "") ? true : false;
            bool missingBonus = (cbBetWin.SelectedItem.ToString() == "") ? true : false;
            bool missingMinInput = (chkbBetMinInput.Checked && tbBetMinInput.Text == "") ? true : false;
            bool missingSubBonusAmount = (chkbBetSubbonus.Checked && tbBetSubbonusAmount.Text == "") ? true : false;
            bool missingDuration = (chkbBetDuration.Checked && tbBetDuration.Text == "") ? true : false;
            bool missingDurationUnit = (chkbBetDuration.Checked && cbBetDurationUnit.SelectedItem.ToString() == "") ? true : false;
            if (Bet.activeBet == null)
            {
                if (!missingName && !missingText && !missingWin && !missingBonus && !missingMinInput && !missingSubBonusAmount && !missingDuration && !missingDurationUnit)
                {
                    string name = tbBetName.Text;
                    string question = tbBetText.Text;
                    int win = Convert.ToInt32(tbBetWin.Text);
                    bool bonus = (cbBetWin.SelectedItem.ToString() == "Additiv") ? true : false;
                    bool minInput = chkbBetMinInput.Checked && tbBetMinInput.Text != "0";
                    int minInputAmount = (minInput) ? Convert.ToInt32(tbBetMinInput.Text) : 0;
                    bool subbonus = chkbBetSubbonus.Checked && tbBetSubbonusAmount.Text != "0";
                    int subbonusAmount = (subbonus) ? Convert.ToInt32(tbBetSubbonusAmount.Text) : 0;
                    bool automaticStop = chkbBetDuration.Checked && tbBetDuration.Text != "0";
                    int duration = (automaticStop) ? ((cbBetDurationUnit.SelectedItem.ToString() == "Sekunden") ? Convert.ToInt32(tbBetDuration.Text) * 1000 : Convert.ToInt32(tbBetDuration.Text) * 60000) : -1;
                    if (((Button)sender).Text == "Erstellen")
                    {
                        Bet n = new Bet(name, question, win, minInputAmount, bonus, subbonus, subbonusAmount, duration);
                        tbBetCurrent.Text = $"Name: {n.name}" + Environment.NewLine + $"Wette: {n.question}" + Environment.NewLine + $"Gewinn: {n.reward}" + Environment.NewLine + $"Mindesteinsatz: {n.minStakeAmount}" + Environment.NewLine + $"Bonus: " + ((n.bonus) ? "Additiv" : "Prozentual") + "" + Environment.NewLine + $"Subbonus: " + ((n.subBonus) ? $"Ja ({n.subBonusPerc})" : "Nein") + "" + Environment.NewLine + $"Dauer: " + ((n.duration == -1) ? "Beendet durch Streamer" : $"{((n.duration < 60000) ? $"{n.duration / 1000} Sekunden" : $"{n.duration / 60000} Minuten")}");
                        Bet.currentDisplayedBet = n;
                    }
                    else
                    {
                        Bet.currentDisplayedBet.name = name;
                        Bet.currentDisplayedBet.question = question;
                        Bet.currentDisplayedBet.reward = win;
                        Bet.currentDisplayedBet.minStakeAmount = minInputAmount;
                        Bet.currentDisplayedBet.bonus = bonus;
                        Bet.currentDisplayedBet.duration = duration;
                        Bet.currentDisplayedBet.subBonus = subbonus;
                        Bet.currentDisplayedBet.subBonusPerc = subbonusAmount;
                        tbBetCurrent.Text = $"Name: {name} {Environment.NewLine} Wette: {question} {Environment.NewLine} Gewinn: Einsatz " + ((bonus) ? $"+ {win}" : $" * { win} (%)")
                        + $" {Environment.NewLine} Mindesteinsatz: {minInputAmount}" + $"{Environment.NewLine} Subbonus: " + ((subbonus) ? $"Ja ({subbonusAmount})" : "Nein") + $"{Environment.NewLine} Dauer: " + ((duration == -1) ? "Beendet durch Streamer" : $"{duration}");
                    }

                    Bot.main.btnBetCurrentEdit.Enabled = true;
                    if (IrcClient.tcp.Connected)
                    {
                        Bot.main.btnBetCurrentStart.Enabled = true;
                    }
                }
                else
                {
                    string message = "Du hast folgende Sachen noch nicht festgelegt: " + Environment.NewLine;
                    message += (missingName) ? "Name, " : "";
                    message += (missingText) ? "Wette, " : "";
                    message += (missingWin) ? "Gewinn, " : "";
                    message += (missingBonus) ? "Bonus, " : "";
                    message += (missingMinInput) ? "Mindesteinsatz, " : "";
                    message += (missingSubBonusAmount) ? "Name, " : "";
                    message += (missingDuration) ? "Dauer, " : "";
                    message += (missingDurationUnit) ? "Zeiteinheit, " : "";
                    message = message.Remove(message.Length - 2);
                    MessageBox.Show(message);
                }
            }
            else
            {
                MessageBox.Show("Es läuft bereits eine Wette! Du kannst erst eine neue erstellen, wenn die aktuelle vorbei ist!");
            }
            clearCreateBet();
        }

        private void tbBetMinInput_TextChanged(object sender, EventArgs e)
        {
            changeChkbCheckStatus(sender);
        }

        private void tbBetSubbonusAmount_TextChanged(object sender, EventArgs e)
        {
            changeChkbCheckStatus(sender);
        }

        private void tbBetDuration_TextChanged(object sender, EventArgs e)
        {
            changeChkbCheckStatus(sender);
        }

        private void changeChkbCheckStatus(object sender)
        {
            if (inputGreaterThanZero((tb_int)sender))
            {
                if (((tb_int)sender).Name == "tbBetMinInput")
                {
                    chkbBetMinInput.Checked = true;
                }
                else if (((tb_int)sender).Name == "tbBetSubbonusAmount")
                {
                    chkbBetSubbonus.Checked = true;
                }
                else if (((tb_int)sender).Name == "tbBetDuration")
                {
                    chkbBetDuration.Checked = true;
                }
            }
            else
            {
                if (((tb_int)sender).Name == "tbBetMinInput")
                {
                    chkbBetMinInput.Checked = false;
                }
                else if (((tb_int)sender).Name == "tbBetSubbonusAmount")
                {
                    chkbBetSubbonus.Checked = false;
                }
                else if (((tb_int)sender).Name == "tbBetDuration")
                {
                    chkbBetDuration.Checked = false;
                }
            }
        }

        private bool inputGreaterThanZero(tb_int s)
        {
            if (s.Text != "")
            {
                if (Regex.IsMatch(s.Text, "[0-9]"))
                {
                    if (Convert.ToInt32(s.Text) != 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion bet

        #region poll

        private void btnPollCurrentStart_Click(object sender, EventArgs e)
        {
            Poll.activePoll = Poll.currentDisplayedPoll;
            btnPollCurrentStart.Enabled = false;
            btnPollCurrentEdit.Enabled = false;
            btnPollCurrentStart.Enabled = true;
            Poll.start(Poll.activePoll);
        }

        private void btnPollCurrentStop_Click(object sender, EventArgs e)
        {
            lbRecentPolls.Text += $"{Poll.activePoll.name} - {Poll.activePoll.question}" + Environment.NewLine;
            Poll.stop.Set();
            btnPollCurrentStart.Enabled = true;
            btnPollCurrentEdit.Enabled = true;
            btnPollCurrentStop.Enabled = false;
        }

        private void btnPollCurrentEdit_Click(object sender, EventArgs e)
        {
            gbPoll.Name = "Bearbeiten";
            btnPollCreate.Text = "Speichern";
            tbPollName.Text = Poll.currentDisplayedPoll.name;
            tbPollQuestion.Text = Poll.currentDisplayedPoll.question;
            for (int i = 0; i < 8; i++)
            {
                foreach (tb option in gbPoll.Controls)
                {
                    if (option.Name.Contains((i + 1).ToString()))
                    {
                        option.Text = Poll.currentDisplayedPoll.opt[i, 0].ToString();
                    }
                }
            }
        }

        private void lbRecentPolls_DoubleClick(object sender, EventArgs e)
        {
            string name = lbRecentPolls.SelectedItem.ToString().Split()[0];
            foreach (Poll recent in Poll.recentPolls)
            {
                if (recent.name == name)
                {
                    tbPollCurrent.Text = $"Name: {recent.name}\nFrage: {recent.question}\n";
                    for (int i = 0; i < 8; i++)
                    {
                        tbPollCurrent.Text += $"Option {i}: {recent.opt[i, 0]}\n";
                    }
                }
            }
            btnPollCurrentEdit.Enabled = true;
            btnPollCurrentStart.Enabled = true;
            btnPollCurrentStop.Enabled = false;
        }

        private void btnPollDelete_Click(object sender, EventArgs e)
        {
            foreach (tb t in gbPoll.Controls)
            {
                t.Text = "";
            }
        }

        private void btnPollCreate_Click(object sender, EventArgs e)
        {
            if (tbPollName.Text == "" || tbPollQuestion.Text == "" || tbPollOption1.Text == "" || tbPollOption2.Text == "")
            {
                MessageBox.Show("Bitte fülle zuerst die alle notwendigen Felder aus! Falls du weniger als 8 Optionen hast, nutze die ersten Optionen!");
                return;
            }
            object[,] newOpt = new object[8, 2];
            newOpt[0, 0] = tbPollOption1.Text;
            newOpt[1, 0] = tbPollOption2.Text;
            newOpt[2, 0] = tbPollOption3.Text;
            newOpt[3, 0] = tbPollOption4.Text;
            newOpt[4, 0] = tbPollOption5.Text;
            newOpt[5, 0] = tbPollOption6.Text;
            newOpt[6, 0] = tbPollOption7.Text;
            newOpt[7, 0] = tbPollOption8.Text;
            newOpt[0, 1] = 0;
            newOpt[1, 1] = 0;
            newOpt[2, 1] = 0;
            newOpt[3, 1] = 0;
            newOpt[4, 1] = 0;
            newOpt[5, 1] = 0;
            newOpt[6, 1] = 0;
            newOpt[7, 1] = 0;
            if (((Button)sender).Text == "Erstellen")
            {
                if (Poll.currentDisplayedPoll != null)
                {
                    Poll.recentPolls.Add(Poll.currentDisplayedPoll);
                    lbRecentPolls.Text += $"{Poll.activePoll.name} - {Poll.activePoll.question}" + Environment.NewLine;
                }
                Poll nPoll = new Poll(tbPollName.Text, tbPollQuestion.Text, newOpt);
                Poll.currentDisplayedPoll = nPoll;
                tbPollCurrent.Text = $"Name: {nPoll.name}\nFrage: {nPoll.question}\n";
                for (int i = 0; i < 8; i++)
                {
                    tbPollCurrent.Text += $"Option {i}: {nPoll.opt[i, 0]}\n";
                }
                btnPollCurrentEdit.Enabled = true;
                btnPollCurrentStart.Enabled = true;
                btnPollCurrentStop.Enabled = false;
            }
            else if (((Button)sender).Text == "Speichern")
            {
                Poll.currentDisplayedPoll.name = tbPollName.Text;
                Poll.currentDisplayedPoll.question = tbPollQuestion.Text;
                Poll.currentDisplayedPoll.opt = newOpt;
                ((Button)sender).Text = "Erstellen";
            }
        }

        #endregion poll

        #region giveaway

        private void btnGiveawayCreate_Click(object sender, EventArgs e)
        {
            string win = tbGiveawayWin.Text;
            string codeword = tbGiveawayKeyword.Text;
            int winnerAmount = Convert.ToInt32(tbGiveawayWinners.Text);
            int minInputAmount = (chkbGiveawaMinInput.Checked) ? Convert.ToInt32(tbGiveawayMinInput.Text) : 0;
            bool subBonus = chkbGiveawaySubBonus.Checked;
            Giveaway g = new Giveaway(win, codeword, winnerAmount, minInputAmount, subBonus);
            Giveaway.current = g;
            Giveaway.updateGiveawayDisplay();
            btnGiveawayCurrentStart.Enabled = true;
            btnGiveawayCurrentStop.Enabled = false;
            btnGiveawayCurrentReset.Enabled = false;
            btnGiveawayCurrentDelete.Enabled = true;
        }

        private void btnGiveawayCurrentStart_Click(object sender, EventArgs e)
        {
            Giveaway.current.start();
        }

        private void btnGiveawayCurrentStop_Click(object sender, EventArgs e)
        {
            Giveaway.current.stop();
        }

        private void btnGiveawayCurrentReset_Click(object sender, EventArgs e)
        {
            Giveaway.current.reset();
        }

        private void btnWinnerChooseNew_Click(object sender, EventArgs e)
        {
            bool[] chooseNew = new bool[Giveaway.current.winnerAmount];
            foreach (int index in chkdLbWinnerList.CheckedIndices)
            {
                chooseNew[index] = true;
            }
            Giveaway.current.chooseNewWinner(chooseNew, (cbWinnerSecondChance.Text == "Nein") ? false : true);
        }

        private void tbGiveawayWinners_Leave(object sender, EventArgs e)
        {
            tb w = sender as tb;
            if (w.Text.StartsWith("0"))
            {
                w.Text = "1";
                MessageBox.Show("Der Wert muss mindestens 1 sein!");
                w.Select();
            }
            else
            {
                if (!Regex.IsMatch(((tb)sender).Text, @"^[0-9]*$"))
                {
                    MessageBox.Show("Bitte gebe eine Zahl ein!");
                    w.Select();
                }
            }
        }

        private void tbGiveawayMinInput_TextChanged(object sender, EventArgs e)
        {
            tb_int w = sender as tb_int;
            if (Regex.IsMatch(w.Text, @"^[0-9]*$") && !w.Text.StartsWith("0") && w.Text != "")
            {
                chkbGiveawaMinInput.Checked = true;
            }
            else if (w.Text.StartsWith("0") && w.Text.Length > 1)
            {
                MessageBox.Show("Der Wert darf nicht mit 0 beginnen!");
                w.Select();
            }
            else
            {
                chkbGiveawaMinInput.Checked = false;
            }
        }

        private void tbGiveawayMinInput_Leave(object sender, EventArgs e)
        {
            if (((tb_int)sender).Text == "")
            {
                ((tb_int)sender).Text = "0";
            }
        }

        #endregion giveaway

        private void btnDgvSortOrder_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            if (dgvAscending)
            {
                dgvAscending = false;
                Settings.Default.dgvAscending = false;
                Settings.Default.Save();
                Console.WriteLine(Settings.Default.dgvAscending.ToString() + "sollte false sein");
                b.Text = "Aufsteigend sortieren";
            }
            else
            {
                dgvAscending = true;
                Settings.Default.dgvAscending = true;
                Settings.Default.Save();
                Console.WriteLine(Settings.Default.dgvAscending.ToString() + "sollte true sein");
                b.Text = "Absteigend sortieren";
            }
            sortDgvViewerrankings();
        }

        private void cbCurrencyUpdateUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            changedSettings = true;
        }

        private void btnViewerPurge_Click(object sender, EventArgs e)
        {
            frmPunishViewer warning = new frmPunishViewer("warning");
            warning.Show();
        }

        private void btnViewerTimeout_Click(object sender, EventArgs e)
        {
            frmPunishViewer timeout = new frmPunishViewer("timeout");
            timeout.Show();
        }

        private void btnViewerBan_Click(object sender, EventArgs e)
        {
            frmPunishViewer ban = new frmPunishViewer("ban");
            ban.Show();
        }

        private void btnMessageSend_Click(object sender, EventArgs e)
        {
            if (tbChat.Text.Trim().Length >0)
            {
                Bot.irc.sendChatMessage(tbMessageSend.Text);
                tbMessageSend.Text = "";
                tbMessageSend.Focus();
            }
        }
    }
}