﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Sinister_Bot;
using CurrencySystem;
using System.Windows.Forms;

namespace Bettingsystem
	{
	public class Bet
		{
		public static Bet activeBet;
		public static Bet currentDisplayedBet;
		public static List<Bet> recentBets = new List<Bet>();
		public static AutoResetEvent awaitStop = new AutoResetEvent(false);
		public static AutoResetEvent awaitBetResult = new AutoResetEvent(false);
		public List<Tuple<Viewer, int, int>> placedBets = new List<Tuple<Viewer, int, int>>(); //name, stakeAmount, y/n
		public string name;
		public string question;
		public int reward;
		public int minStakeAmount;
		public bool bonus; //if true -> currency += stake + reward, if false -> currency += stake*reward
		public bool subBonus;
		public int subBonusPerc;
		public int duration; //if duration == -1 -> until streamer terminates it ---- in milliseconds
		public bool won;

		public Bet(string n, string q, int r, int mSA, bool b, bool sB, int sBP, int d)
			{
			name = n;
			question = q;
			reward = r;
			minStakeAmount = mSA;
			bonus = b;
			subBonus = sB;
			subBonusPerc = sBP;
			duration = d;
			}

		public static void handleMessage(string content, Viewer sender)
			{
			int betAmount = Convert.ToInt32(content.Split()[1]);
			int bet = Convert.ToInt32(content.Split()[2]);
			activeBet.placedBets.Add(new Tuple<Viewer, int, int>(sender, betAmount, bet));
			}

		public static void start(Bet bet)
			{
			Task.Factory.StartNew(() =>
			{
				activeBet = bet;
				Bot.irc.sendChatMessage($"Eine neue Wette wurde gestartet! Um mitzumachen schreibt bitte \"${bet.name}\" mit eurem Einsatz und 0 (Nein) oder 1 (Ja) für den Ausgang der Wette (Beispiel: ?lol 25 1). Jeder kann nur einmal pro Wette teilnehmen! Der Mindesteinsatz für diese Wette beträgt: {bet.minStakeAmount}");
				#region invoking UI
				if (Bot.main.btnBetCurrentStart.InvokeRequired)
					{
					Bot.main.btnBetCurrentStart.Invoke((MethodInvoker) (() => Bot.main.btnBetCurrentStart.Enabled = false));
					}
				else
					{
					Bot.main.btnBetCurrentStart.Enabled = false;
					}
				if (Bot.main.btnBetCurrentEdit.InvokeRequired)
					{
					Bot.main.btnBetCurrentEdit.Invoke((MethodInvoker) (() => Bot.main.btnBetCurrentEdit.Enabled = false));
					}
				else
					{
					Bot.main.btnBetCurrentEdit.Enabled = false;
					}
				#endregion invoking UI
				if (bet.duration >= 0)
					{
					Console.WriteLine("Now waiting for countdown");
					Thread.Sleep(bet.duration);
					Console.WriteLine("Countdown finished");
					}
				else
					{
					#region invoking UI
					if (Bot.main.btnBetCurrentStop.InvokeRequired)
						{
						Bot.main.btnBetCurrentStop.Invoke((MethodInvoker) (() => Bot.main.btnBetCurrentStop.Enabled = true));
						}
					else
						{
						Bot.main.btnBetCurrentStop.Enabled = true;
						}
					#endregion invoking UI
					Console.WriteLine("Now waiting for input");
					awaitStop.WaitOne();
					}
				betEnded(bet);
			}, Bot.tokenIrc);
			}

		private static void betEnded(Bet bet)
			{
			Task.Factory.StartNew(() =>
			{
				#region invoking UI
				if (Bot.main.btnBetCurrentStop.InvokeRequired)
					{
					Bot.main.btnBetCurrentStop.Invoke((MethodInvoker) (() => Bot.main.btnBetCurrentStop.Enabled = false));
					}
				else
					{
					Bot.main.btnBetCurrentStop.Enabled = false;
					}

				if (Bot.main.btnBetCurrentWon.InvokeRequired)
					{
					Bot.main.btnBetCurrentWon.Invoke((MethodInvoker) (() => Bot.main.btnBetCurrentWon.Enabled = true));
					}
				else
					{
					Bot.main.btnBetCurrentWon.Enabled = true;
					}

				if (Bot.main.btnBetCurrentWon.InvokeRequired)
					{
					Bot.main.btnBetCurrentWon.Invoke((MethodInvoker) (() => Bot.main.btnBetCurrentWon.Visible = true));
					}
				else
					{
					Bot.main.btnBetCurrentWon.Visible = true;
					}

				if (Bot.main.btnBetCurrentLost.InvokeRequired)
					{
					Bot.main.btnBetCurrentLost.Invoke((MethodInvoker) (() => Bot.main.btnBetCurrentLost.Enabled = true));
					}
				else
					{
					Bot.main.btnBetCurrentLost.Enabled = true;
					}

				if (Bot.main.btnBetCurrentLost.InvokeRequired)
					{
					Bot.main.btnBetCurrentLost.Invoke((MethodInvoker) (() => Bot.main.btnBetCurrentLost.Visible = true));
					}
				else
					{
					Bot.main.btnBetCurrentLost.Visible = true;
					}
				#endregion invoking UI
				awaitBetResult.WaitOne();
				activeBet = null;
				recentBets.Add(bet);
				List<Tuple<Viewer, int>> winner = new List<Tuple<Viewer, int>>();
				if (bet.won)
					{
					foreach (Tuple<Viewer, int, int> part in bet.placedBets)
						{
						if (part.Item3 == 1)
							{
							int win = part.Item2;
							if (bet.bonus)
								{
								win += bet.reward;
								}
							else
								{
								win = (win * bet.reward) / 100;
								}
							if (bet.subBonus)
								{
								win = (win * bet.subBonusPerc) / 100;
								}
							part.Item1.currency += win;
							winner.Add(new Tuple<Viewer, int>(part.Item1, win));
							}
						}
					}
				else
					{
					foreach (Tuple<Viewer, int, int> part in bet.placedBets)
						{
						if (part.Item3 == 0)
							{
							int win = part.Item2;
							if (bet.bonus)
								{
								win += bet.reward;
								}
							else
								{
								win = (win * bet.reward) / 100;
								}
							if (bet.subBonus)
								{
								win = (win * bet.subBonusPerc) / 100;
								}
							part.Item1.currency += win;
							winner.Add(new Tuple<Viewer, int>(part.Item1, win));
							}
						}
					}
				string winMessage = "";

				if (winner.Count >= 1)
					{
					winMessage += (winner.Count == 1) ? "Richtig gewettet hat: " : "Richtig gewettet haben: ";
					foreach (Tuple<Viewer, int> w in winner)
						{
						winMessage += $"{w.Item1.name} ({w.Item2}), ";
						}
					winMessage = winMessage.Remove(winMessage.Length - 2);
					winMessage += (winner.Count == 1) ? "! Glückwunsch an den Gewinner!" : "! Glückwunsch an die Gewinner!";
					}
				else
					{
					winMessage += "Leider hat niemand gewonnen. Viel Glück beim nächsten Mal!";
					}
				#region invoking UI
				if (Bot.main.btnBetCurrentStart.InvokeRequired)
					{
					Bot.main.btnBetCurrentStart.Invoke((MethodInvoker) (() => Bot.main.btnBetCurrentStart.Enabled = true));
					}
				else
					{
					Bot.main.btnBetCurrentStart.Enabled = true;
					}

				if (Bot.main.btnBetCurrentEdit.InvokeRequired)
					{
					Bot.main.btnBetCurrentEdit.Invoke((MethodInvoker) (() => Bot.main.btnBetCurrentEdit.Enabled = true));
					}
				else
					{
					Bot.main.btnBetCurrentEdit.Enabled = true;
					}

				if (Bot.main.btnBetCurrentWon.InvokeRequired)
					{
					Bot.main.btnBetCurrentWon.Invoke((MethodInvoker) (() => Bot.main.btnBetCurrentWon.Enabled = false));
					}
				else
					{
					Bot.main.btnBetCurrentWon.Enabled = false;
					}

				if (Bot.main.btnBetCurrentWon.InvokeRequired)
					{
					Bot.main.btnBetCurrentWon.Invoke((MethodInvoker) (() => Bot.main.btnBetCurrentWon.Visible = false));
					}
				else
					{
					Bot.main.btnBetCurrentWon.Visible = false;
					}

				if (Bot.main.btnBetCurrentLost.InvokeRequired)
					{
					Bot.main.btnBetCurrentLost.Invoke((MethodInvoker) (() => Bot.main.btnBetCurrentLost.Enabled = false));
					}
				else
					{
					Bot.main.btnBetCurrentLost.Enabled = false;
					}

				if (Bot.main.btnBetCurrentLost.InvokeRequired)
					{
					Bot.main.btnBetCurrentLost.Invoke((MethodInvoker) (() => Bot.main.btnBetCurrentLost.Visible = false));
					}
				else
					{
					Bot.main.btnBetCurrentLost.Visible = false;
					}
				if (Bot.main.lbRecentBets.InvokeRequired)
					{
					Bot.main.lbRecentBets.Invoke((MethodInvoker) (() => Bot.main.lbRecentBets.Items.Add($"{bet.name} ({bet.question} - Ergebnis: {((bet.won) ? "gewonnen" : "verloren")}) - Teilnehmer: {bet.placedBets.Count} - Gewinner: {winner.Count}")));
					}
				else
					{
					Bot.main.lbRecentBets.Items.Add($"{bet.name} ({bet.question} - Ergebnis: {((bet.won) ? "gewonnen" : "verloren")}) - Teilnehmer: {bet.placedBets.Count} - Gewinner: {winner.Count}");
					}
				if (Bot.main.lbRecentBets.InvokeRequired)
					{
					Bot.main.lbRecentBets.Invoke((MethodInvoker) (() => Bot.main.tbBetCurrent.Text = ""));
					}
				else
					{
					Bot.main.tbBetCurrent.Text = "";
					}
				#endregion invoking UI
				Bot.irc.sendChatMessage(winMessage);
			}, Bot.tokenIrc);
			}
		}
	}