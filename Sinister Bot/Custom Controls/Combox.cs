﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Sinister_Bot.Custom_Controls
	{
	public class cb : ComboBox
		{
		public cb()
			{
			}
			
		protected override void OnPaint(PaintEventArgs e)
			{
			base.OnPaint(e);
			DrawDropDown(this, e.Graphics, Color.RoyalBlue, Color.FromArgb(28, 28, 28));
			}

		private void DrawDropDown(ComboBox c, Graphics g, Color basicColor, Color backColor)
			{
			Brush textBrush = new SolidBrush(basicColor);
			Brush borderBrush = new SolidBrush(basicColor);
			Pen borderPen = new Pen(borderBrush);
			SizeF strSize = g.MeasureString(c.Text, c.Font);
			Rectangle rect = new Rectangle(c.ClientRectangle.X,
										   c.ClientRectangle.Y + (int) (strSize.Height / 2),
										   c.ClientRectangle.Width - 1,
										   c.ClientRectangle.Height - (int) (strSize.Height / 2) - 1);
			g.Clear(Color.Black);

			g.DrawString(c.Text, c.Font, textBrush, c.Padding.Left, 0);
			// Drawing Border
			//Left
			g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
			//Right
			g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
			//Bottom
			g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
			//Top1
			g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + c.Padding.Left, rect.Y));
			//Top2
			g.DrawLine(borderPen, new Point(rect.X + c.Padding.Left + (int) (strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));

			this.BackColor = Color.FromArgb(28, 28, 28);
			this.ForeColor = Color.RoyalBlue;
			}
		}
	}