﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Sinister_Bot;
using Sinister_Bot.Properties;

namespace Sinister_Bot.Custom_Controls
	{
	public class tb_int : TextBox
		{
		private Regex _integer = new Regex(@"^[0-9]*$");
		private string lastValue = "";
		private bool changed = false;

		public tb_int()
			{
			lastValue = this.Text.ToString();
			}

		protected override void OnTextChanged(EventArgs e)
			{
			base.OnTextChanged(e);
			if (lastValue == "")
				{
				lastValue = this.Text.ToString();
				}
			changed = true;
			}

		///////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////// Functionality ////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////

		#region functionality

		protected override void OnLeave(EventArgs e)
			{
			base.OnLeave(e);

			if (Parent is Form)
				{
				Form p = Parent as Form;
				if (p.ActiveControl is Button)
					{
					if ((((Button) (p.ActiveControl)).Text != "Abbrechen") == (((Button) (p.ActiveControl)).Text != "X"))
						{
						if (!_integer.IsMatch(Text))
							{
							MessageBox.Show("Bitte gebe eine Zahl ein!");
							Select();
							}
						}
					}
				}
			else if (Parent.Parent.Parent.Parent is Form)
				{
				Form p = Parent.Parent.Parent.Parent as Form;
				if (p.ActiveControl is Button)
					{
					if ((((Button) (p.ActiveControl)).Text != "Abbrechen") == (((Button) (p.ActiveControl)).Text != "X"))
						{
						if (!_integer.IsMatch(Text))
							{
							MessageBox.Show("Bitte gebe eine Zahl ein!");
							Select();
							}
						}
					}
				else
					{
					if (!_integer.IsMatch(Text))
						{
						MessageBox.Show("Bitte gebe eine Zahl ein!");
						Select();
						}
					}
				}
			else
				{
				if (!_integer.IsMatch(Text))
					{
					MessageBox.Show("Bitte gebe eine Zahl ein!");
					Select();
					}
				}
			if (this.Parent.Parent is TabPage)
				{
				string page = ((TabPage) this.Parent.Parent).Name;
				if (page != "tpSetup" && page != "tpRewardsystem")
					{
					changed = false;
					return;
					}
				if (changed)
					{
					frmMain.changedSettings = true;
					changed = false;
					}
				}
			}

		#endregion functionality

		///////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////// Desgin ////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////

		#region design

		protected override void OnPaint(PaintEventArgs e)
			{
			base.OnPaint(e);
			DrawBorder(this, e.Graphics, Color.RoyalBlue, Color.RoyalBlue);
			}

		private void DrawBorder(TextBox box, Graphics g, Color textColor, Color borderColor)
			{
			Brush textBrush = new SolidBrush(textColor);
			Brush borderBrush = new SolidBrush(borderColor);
			Pen borderPen = new Pen(borderBrush);
			SizeF strSize = g.MeasureString(box.Text, box.Font);
			Rectangle rect = new Rectangle(box.ClientRectangle.X,
										   box.ClientRectangle.Y + (int) (strSize.Height / 2),
										   box.ClientRectangle.Width - 1,
										   box.ClientRectangle.Height - (int) (strSize.Height / 2) - 1);
			g.Clear(Color.Black);

			g.DrawString(box.Text, box.Font, textBrush, box.Padding.Left, 0);

			// Drawing Border
			//Left
			g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
			//Right
			g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
			//Bottom
			g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
			//Top1
			g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
			//Top2
			g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int) (strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));

			this.BackColor = Color.FromArgb(28, 28, 28);
			}

		#endregion design
		}
	}