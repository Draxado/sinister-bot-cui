﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Dotnetrix_Samples;

namespace Sinister_Bot.Custom_Controls
	{
	public class tb : TextBox
		{
		private string lastValue = "";
		private bool changed = false;

		public tb()
			{
			lastValue = this.Text.ToString();
			}

		protected override void OnTextChanged(EventArgs e)
			{
			base.OnTextChanged(e);
			if (lastValue == "")
				{
				lastValue = this.Text.ToString();
				}
			changed = true;
			}

		protected override void OnPaint(PaintEventArgs e)
			{
			base.OnPaint(e);
			DrawBorder(this, e.Graphics, Color.RoyalBlue, Color.RoyalBlue);
			}

		protected override void OnLeave(EventArgs e)
			{
			base.OnLeave(e);
			if (this.Parent.Parent is TabPage)
				{
				string page = ((TabPage) this.Parent.Parent).Name;
				if (page != "tpSetup" && page != "tpRewardsystem")
					{
					changed = false;
					return;
					}
				if (changed)
					{
					if (lastValue != this.Text.ToString())
						{
						frmMain.changedSettings = true;
						lastValue = this.Text.ToString();
						}
					changed = false;
					}
				}
			}

		private void DrawBorder(TextBox box, Graphics g, Color textColor, Color borderColor)
			{
			Brush textBrush = new SolidBrush(textColor);
			Brush borderBrush = new SolidBrush(borderColor);
			Pen borderPen = new Pen(borderBrush);
			SizeF strSize = g.MeasureString(box.Text, box.Font);
			Rectangle rect = new Rectangle(box.ClientRectangle.X,
										   box.ClientRectangle.Y + (int) (strSize.Height / 2),
										   box.ClientRectangle.Width - 1,
										   box.ClientRectangle.Height - (int) (strSize.Height / 2) - 1);
			g.Clear(Color.Black);

			g.DrawString(box.Text, box.Font, textBrush, box.Padding.Left, 0);

			// Drawing Border
			//Left
			g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
			//Right
			g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
			//Bottom
			g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
			//Top1
			g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
			//Top2
			g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int) (strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));

			this.BackColor = Color.FromArgb(28, 28, 28);
			}
		}
	}