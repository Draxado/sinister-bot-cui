﻿using Sinister_Bot;
using Sinister_Bot.Properties;
using CurrencySystem;
using System.Collections.Generic;

namespace CommandSystem
	{
	public class CommandPreset
		{
		public string message;
		public string name;
		public bool used;
		public string[] alias;

		public CommandPreset(string name, string message, string[] alias)
			{
			this.name = name;
			this.message = message;
			this.used = false;
			this.alias = alias;
			}

		public static void addPresetCommands()
			{
			string l = list();
			string[] infoAlias = { "command", "commands" };
			Bot.commandPre.createPresetCommand("botinfo", "Hey zusammen! HeyGuys Ich bin ein Bot und wurde von Draxado programmiert und befinde mich" +
			" noch in der Testphase. Bei Bugs oder Fehlern schreibt bitte eine Nachricht an Draxado (je genauer ihr den Bug beschreibt, desto einfacher kann man den Fehler finden)." + "Viel Spaß beim Stream!", null);
			Bot.commandPre.createPresetCommand("commandlist", l, infoAlias);
			Bot.commandPre.createPresetCommand("info", Settings.Default.gbStream.Split('#')[5], null);
			Bot.commandPre.createPresetCommand("Punkte", "", null);
			}

		public static string list()
			{
			string list = "";
			foreach (CommandPreset preC in Bot.commandPre.listPre)
				{
				list = list + preC.name;
				if (preC.alias != null && preC.alias[0] != "")
					{
					list += " (alias: ";
					foreach (string a in preC.alias)
						{
						list += a + ", ";
						}
					list = list.Remove(list.Length - 2);
					list += ")";
					}
				list += " / ";
				}
			if (Bot.command != null)
				{
				foreach (Command c in Bot.command.list)
					{
					list += c.name;
					if (c.alias != null && c.alias[0] != "")
						{
						list += " (alias: ";
						foreach (string a in c.alias)
							{
							list += a + ", ";
							}
						list = list.Remove(list.Length - 2);
						list += ")";
						}
					list += " / ";
					}
				}
			if (list != "")
				{
				if (list.Length >= 2)
					{
					list = list.Remove(list.Length - 2);
					}
				list = list.Trim();
				}
			return list;
			}
		}
	}