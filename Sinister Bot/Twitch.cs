﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using CurrencySystem;
using Newtonsoft.Json;
using Sinister_Bot;
using Sinister_Bot.Properties;

namespace Twitch
{
    public static class TwitchViewer
    {
        public static string url = "http://tmi.twitch.tv/group/user/" + Settings.Default.gbStream.Split('#')[0] + "/chatters";
        public static ChannelAttributes c;
        public static WebClient w = new WebClient();
        public static Task updateViewerlistFromTwitch;

        public static void createTwitchViewerTask()
        {
            updateViewerlistFromTwitch = new Task(() =>
            {
                while (!Bot.sourceIrc.IsCancellationRequested)
                {
                    string json = string.Empty;
                    try
                    {
                        json = w.DownloadString(url);
                        ChannelAttributes temp = new ChannelAttributes();
                        temp = JsonConvert.DeserializeObject<ChannelAttributes>(json);
                        bool foundViewer = false;
                        foreach (string v in temp.chatters.viewers)
                        {
                            foreach (Viewer checkV in Bot.viewer.list) //Viewer = 3
                            {
                                if (checkV.name.Equals(v))
                                {
                                    foundViewer = true;
                                    if (!checkV.inChat)
                                    {
                                        checkV.inChat = true;
                                        checkV.increaseCurrency.Start(); 
                                    }
                                    break;
                                }
                            }
                            if (!foundViewer)
                            {
                                Viewer newViewer = new Viewer(v, Convert.ToInt32(Settings.Default.gbBasicSettings.Split('#')[3]), null, 3);
                                ViewerDB.updateRank(newViewer);
                                Bot.viewer.newViewer(newViewer); //db
                                Bot.viewer.list.Add(newViewer); //db
                                newViewer.inChat = true;
                                newViewer.increaseCurrency.Start();
                                foundViewer = true;
                            }
                        }
                        if (!foundViewer)
                        {
                            foreach (string v in temp.chatters.moderators) //Mod = 0
                            {
                                foreach (Viewer checkM in Bot.viewer.list)
                                {
                                    if (checkM.name.Equals(v))
                                    {
                                        foundViewer = true;
                                        if (!checkM.inChat)
                                        {
                                            checkM.inChat = true;
                                            checkM.increaseCurrency.Start(); 
                                        }
                                        break;
                                    }
                                }
                                if (!foundViewer)
                                {
                                    Viewer newMod = new Viewer(v, Convert.ToInt32(Settings.Default.gbBasicSettings.Split('#')[3]), null, 0);
                                    ViewerDB.updateRank(newMod);
                                    Bot.viewer.newViewer(newMod); //db
                                    Bot.viewer.list.Add(newMod); //db
                                    newMod.inChat = true;
                                    newMod.increaseCurrency.Start();
                                }
                            }
                        }
                        Thread.Sleep(30000);
                    }
                    catch
                    {
                        throw;
                    }
                    Thread.Sleep(30000);
                }
            }, Bot.tokenIrc);
            updateViewerlistFromTwitch.Start();
        }

        public static void getViewerlistFromTwitch()
        {
            string json = string.Empty;
            try
            {
                json = w.DownloadString(url);
                c = JsonConvert.DeserializeObject<ChannelAttributes>(json);
                bool found = false;

                foreach (string v in c.chatters.viewers)
                {
                    foreach (Viewer loaded in Bot.viewer.list)
                    {
                        if (loaded.name == v)
                        {
                            found = true;
                        }
                    }
                    if (!found)
                    {
                        Viewer newV = new Viewer(v, Convert.ToInt32(Settings.Default.gbBasicSettings.Split('#')[3]), null, 3);
                        ViewerDB.updateRank(newV);
                        Bot.viewer.newViewer(newV); //db
                        Bot.viewer.list.Add(newV); //db
                    }
                }
                foreach (string v in c.chatters.moderators)
                {
                    foreach (Viewer loaded in Bot.viewer.list)
                    {
                        if (loaded.name == v)
                        {
                            found = true;
                        }
                    }
                    if (!found)
                    {
                        Viewer newV = new Viewer(v, Convert.ToInt32(Settings.Default.gbBasicSettings.Split('#')[3]), null, 0);
                        ViewerDB.updateRank(newV);
                        Bot.viewer.newViewer(newV); //db
                        Bot.viewer.list.Add(newV); //db
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public static int updateViewerStatus(string nameViewer)
        {
            if (nameViewer.Equals(Settings.Default.gbStream.Split('#')[0]))
            {
                Bot.viewer.updateStatus(nameViewer, 0);
                return 0;
            }
            else if ((nameViewer.Equals(Settings.Default.gbStream.Split('#')[1])))
            {
                Bot.viewer.updateStatus(nameViewer, 1);
                return 1;
            }
            else
            {
                string json = string.Empty;
                try
                {
                    json = w.DownloadString(url);
                    c = JsonConvert.DeserializeObject<ChannelAttributes>(json);
                    foreach (string v in c.chatters.moderators)
                    {
                        if (v.Equals(nameViewer))
                        {
                            Bot.viewer.updateStatus(nameViewer, 1);
                            return 1;
                        }
                    }
                }
                catch
                {
                    foreach (Viewer searchV in Bot.viewer.list)
                    {
                        if (searchV.name == nameViewer)
                        {
                            return searchV.status;
                        }
                    }
                }
                foreach (Viewer searchV in Bot.viewer.list)
                {
                    if (searchV.name == nameViewer)
                    {
                        if (searchV.status == 0)
                        {
                            Bot.viewer.updateStatus(nameViewer, 3);
                            return 3;
                        }
                        else
                        {
                            return searchV.status;
                        }
                    }
                }
            }
            return 3;
        }
    }

    #region deserialization classes

    public class ChannelAttributes
    {
        public Links _links
        {
            get; set;
        }

        public int chatter_count
        {
            get; set;
        }

        public Chatters chatters
        {
            get; set;
        }
    }

    public class Links
    {
    }

    public class Chatters
    {
        public List<string> moderators
        {
            get; set;
        }

        public List<string> staff
        {
            get; set;
        }

        public List<string> admins
        {
            get; set;
        }

        public List<object> global_mods
        {
            get; set;
        }

        public List<string> viewers
        {
            get; set;
        }
    }

    #endregion deserialization classes
}