﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Bettingsystem;
using CommandSystem;
using CurrencySystem;
using Quotesystem;
using Sinister_Bot;
using Sinister_Bot.Properties;
using Pollsystem;
using Twitch;
using Giveawaysystem;

namespace MessageHandling
	{
	public class ChatMessage
		{
		public bool blocked = false;
		public string content = "";
		public long timestamp;
		public bool found = false;
		public Viewer sender;
		private static Task read;
		public CancellationTokenSource source;
		public static bool firstLoginMessage = true;

		public ChatMessage(string irc)
			{
			string name = Regex.Replace(irc, @"[:!@]", " ").Split()[1];
			if (name.Contains("tmi.twitch.tv") || irc.Contains("JOIN #") || irc.Contains("PART #"))
				{
				if (firstLoginMessage)
					{
					Bot.main.updateTbChat("Entering chat...");
					firstLoginMessage = false;
					}
				if (irc.Contains("End"))
					{
					Bot.irc.sendChatMessage($"Hallo! HeyGuys Willkommen beim Stream von {IrcClient.streamchannel[0].ToString().ToUpper() + IrcClient.streamchannel.Substring(1, IrcClient.streamchannel.Length - 1)}. Ich wünsche euch viel Spaß!");
					}
				return;
				}
			if (irc.Contains("PING"))
				{
				Bot.irc.sendIrcMessage("PONG");
				return;
				}

			content = irc.Substring(29 + name.Length * 3 + Settings.Default.gbStream.Split('#')[0].Length);
			timestamp = DateTime.Now.Ticks;
			//find out if viewer is already registered and update status
			foreach (Viewer v in Bot.viewer.list)
				{
				if (name.Equals(v.name))
					{
					sender = v;
					sender.status = TwitchViewer.updateViewerStatus(v.name);
					found = true;
					sender.messagesSended.Add(this);
					break;
					}
				}
			if (!found && name != Settings.Default.gbStream.Split('#')[1] && name != null && name != "")
				{
				Viewer v = new Viewer(name, Convert.ToInt32(Settings.Default.gbBasicSettings.Split('#')[3]), null, 3);
				ViewerDB.updateRank(v);
				Bot.viewer.newViewer(v); //db
				Bot.viewer.list.Add(v); //db
				sender = v;
				sender.status = TwitchViewer.updateViewerStatus(v.name);
				sender.messagesSended.Add(this);
				}
			found = false;
			Bot.main.updateTbChat($"{name}:  {content}");

			#region filter messages

			if (content != "")
				{
				//TODO later + add a new tab
				//foreach (KeyValuePair<string, string> q in Bot.faq.list)
				//	{
				//	if (content.ToLower().Contains(q.Key))
				//		{
				//		Bot.irc.sendChatMessage(q.Value);
				//		}
				//	}
				if (Regex.IsMatch(content, @"^!\w")) //Command
					{
					Command.handleMessage(content, sender);
					}
				if (Regex.IsMatch(content, @"^#\w")) //Quote
					{
					Quote.handleMessage(content, sender);
					}
				if (Regex.IsMatch(content, @"^?\w")) //Poll
					{
					if (Poll.activePoll != null)
						{
						Poll.handleMessage(content, sender);
						}
					}
				if (content.StartsWith("$"))
					{
					if (Regex.IsMatch(content.Substring(1), @"^\w\ \w")) //Bet
						{
						if (Bet.activeBet != null)
							{
							Bet.handleMessage(content, sender);
							}
						} 
					}
				if (Regex.IsMatch(content, @"^&\w"))
					{
					if (Giveaway.current.active && Giveaway.current.codeword.Equals(content.Substring(1).Split()[0]))
						{
						Giveaway.current.enter(sender);
						}
					}
				}
			#endregion filter messages
			}

		////////////////////////////////// Reading Messages (Task) //////////////////////////////////

		#region Task reading Messages

		public static void startReading()
			{
			read = Task.Factory.StartNew(() =>
			{
				while (!Bot.tokenIrc.IsCancellationRequested)
					{
					string s = Bot.irc.readMessage();
					if (s != null)
						{
						ChatMessage msg = new ChatMessage(s);
						}
					Thread.Sleep(100);
					}
			}, Bot.tokenIrc);
			}
		#endregion Task reading Messages
		}
	}