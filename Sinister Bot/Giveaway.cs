﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using CurrencySystem;
using Sinister_Bot;

namespace Giveawaysystem
	{
	public class Giveaway
		{

		public string win;
		public int winnerAmount;
		public string codeword;
		public int minInputAmount;
		public bool subscriberBonus; //subscriber get an extra entry
		public List<Viewer> participants;
		public Viewer[] winner;
		public bool active;
		public static Giveaway current;
		public static AutoResetEvent stopGiveaway;

		public Giveaway(string w, string cw, int wA, int minInA, bool subB)
			{
			win = w;
			codeword = cw;
			winnerAmount = wA;
			winner = new Viewer[wA];
			minInputAmount = minInA;
			subscriberBonus = subB;
			}

		public void start()
			{
			Task.Factory.StartNew(() =>
			{
				current.active = true;
				Bot.main.btnGiveawayCurrentStart.Enabled = false;
				Bot.main.btnGiveawayCurrentStop.Enabled = true;
				Bot.main.btnGiveawayCurrentReset.Enabled = false;
				Bot.main.btnGiveawayCurrentDelete.Enabled = false;
				Bot.irc.sendChatMessage($"Ein neues Giveaway wurde gestartet! Zu gewinnen gibt es: {current.win}. Um teilzunehmen schreibe bitte: \"&{current.codeword})");
				stopGiveaway.WaitOne();
			}, Bot.tokenIrc);
			stop();
			}

		public void stop()
			{
			string message = $"Gewonnen {((current.winnerAmount > 1) ? "haben" : "hat")}: ";
			for (int i = 0; i < current.winnerAmount; i++)
				{
				Random number = new Random();
				int n = Convert.ToInt32(number) % current.participants.Count;
				winner[i] = current.participants[n];
				Bot.main.chkdLbWinnerList.Items.Add(winner[i].name);
				if (i != current.winnerAmount - 1)
					{
					message += winner[i].name + ", ";
					}
				else
					{
					message += winner[i].name + ". Herzlichen Glückwunsch!";
					}
				}
			Bot.main.btnGiveawayCurrentStart.Enabled = true;
			Bot.main.btnGiveawayCurrentStop.Enabled = false;
			Bot.main.btnGiveawayCurrentReset.Enabled = true;
			Bot.main.btnGiveawayCurrentDelete.Enabled = true;
			updateGiveawayDisplay();
			}

		public void enter(Viewer v)
			{
			lock (v)
				{
				if (v.currency - current.minInputAmount >= 0)
					{
					current.participants.Add(v);
					v.currency -= current.minInputAmount;
					updateGiveawayDisplay();
					}
				}
			}

		public void reset()
			{
			current.winner = new Viewer[current.winnerAmount];
			current.participants.Clear();
			Bot.main.btnGiveawayCurrentStart.Enabled = true;
			Bot.main.btnGiveawayCurrentStop.Enabled = false;
			Bot.main.btnGiveawayCurrentReset.Enabled = false;
			Bot.main.btnGiveawayCurrentDelete.Enabled = true;
			}

		public void chooseNewWinner(bool[] forNewChooseSelected, bool secondChance)
			{
			for (int i = 0; i < current.winnerAmount; i++)
				{
				if (forNewChooseSelected[i])
					{
					if (!secondChance)
						{
						current.participants.Remove(winner[i]);
						}
					Bot.main.chkdLbWinnerList.Items.Remove(winner[i].name);
					Random number = new Random();
					int n = Convert.ToInt32(number) % current.participants.Count;
					winner[i] = current.participants[n];
					Bot.main.chkdLbWinnerList.Items.Add(winner[i].name);
					}
				}
			string message = $"{((current.winnerAmount > 1) ? "Die Gewinner wurden" : "Der Gewinner wurde")} neu bestimmmt. Gewonnen {((current.winnerAmount > 1) ? "haben" : "hat")}: ";
			for (int i = 0; i < current.winnerAmount; i++)
				{
				if (i != current.winnerAmount - 1)
					{
					message += winner[i].name + ", ";
					}
				else
					{
					message += winner[i].name + ". Herzlichen Glückwunsch!";
					}
				}
			}

		public static void updateGiveawayDisplay()
			{
			Bot.main.tbGiveawayCurrent.Text += $"Gewinn: {current.win + Environment.NewLine} Anzahl der Gewinner: {current.winnerAmount + Environment.NewLine} Codewort: {current.codeword + Environment.NewLine} Mindesteinsatz: {current.minInputAmount + Environment.NewLine} Teilnehmeranzahl: {current.participants.Count}";
			}
		}
	}
