﻿using System.Windows.Forms;
using Sinister_Bot.Custom_Controls;

namespace Sinister_Bot {
	public partial class frmMain {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && ( components != null )) {
				components.Dispose();
				}
			base.Dispose(disposing);
			}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblCreator = new System.Windows.Forms.Label();
            this.lblMain = new System.Windows.Forms.Label();
            this.ttHelp = new System.Windows.Forms.ToolTip(this.components);
            this.llblOauthTokenHelp = new System.Windows.Forms.LinkLabel();
            this.btnValueEdit = new System.Windows.Forms.Button();
            this.chkbGiveawaySubBonus = new System.Windows.Forms.CheckBox();
            this.btnBSCancel = new System.Windows.Forms.Button();
            this.TabControl = new Dotnetrix_Samples.tc();
            this.tpSetup = new System.Windows.Forms.TabPage();
            this.gbStream = new Sinister_Bot.Custom_Controls.gb();
            this.btnStreamSave = new System.Windows.Forms.Button();
            this.btnStreamCancel = new System.Windows.Forms.Button();
            this.tbInfo = new Sinister_Bot.Custom_Controls.tb();
            this.lblInfo = new System.Windows.Forms.Label();
            this.tbDBon = new Sinister_Bot.Custom_Controls.tb();
            this.tbDBoff = new Sinister_Bot.Custom_Controls.tb();
            this.tbToken = new Sinister_Bot.Custom_Controls.tb();
            this.tbBotchannel = new Sinister_Bot.Custom_Controls.tb();
            this.tbStreamchannel = new Sinister_Bot.Custom_Controls.tb();
            this.lblDatabaseWeb = new System.Windows.Forms.Label();
            this.lblDatabaseFile = new System.Windows.Forms.Label();
            this.lblOauthToken = new System.Windows.Forms.Label();
            this.lblBotchannel = new System.Windows.Forms.Label();
            this.lblStreamchannel = new System.Windows.Forms.Label();
            this.tpRewardsystem = new System.Windows.Forms.TabPage();
            this.gbViewerrankings = new Sinister_Bot.Custom_Controls.gb();
            this.btnDgvSortOrder = new System.Windows.Forms.Button();
            this.btnDgvSave = new System.Windows.Forms.Button();
            this.btnDgvCancel = new System.Windows.Forms.Button();
            this.btnRankDelete = new System.Windows.Forms.Button();
            this.btnRankEdit = new System.Windows.Forms.Button();
            this.btnRankAdd = new System.Windows.Forms.Button();
            this.dgvViewerankings = new System.Windows.Forms.DataGridView();
            this.clmnRank = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnPointsNeeded = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbBasicSettings = new Sinister_Bot.Custom_Controls.gb();
            this.btnBSSave = new System.Windows.Forms.Button();
            this.cbCurrencyUpdateUnit = new System.Windows.Forms.ComboBox();
            this.tbCurrencyCommandMessage = new Sinister_Bot.Custom_Controls.tb();
            this.lblCommandAnswer = new System.Windows.Forms.Label();
            this.tbCurrencyCommand = new Sinister_Bot.Custom_Controls.tb();
            this.lblCurrencyCommand = new System.Windows.Forms.Label();
            this.lblCurrency2 = new System.Windows.Forms.Label();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.lblCurrencyReward = new System.Windows.Forms.Label();
            this.lblCurrencyUpdate = new System.Windows.Forms.Label();
            this.lblCurrencyStart = new System.Windows.Forms.Label();
            this.lblCurrencyName = new System.Windows.Forms.Label();
            this.tbCurrencyIncrease = new Sinister_Bot.Custom_Controls.tb_int();
            this.tbCurrencyUpdate = new Sinister_Bot.Custom_Controls.tb_int();
            this.tbCurrencyStart = new Sinister_Bot.Custom_Controls.tb_int();
            this.tbCurrencyName = new Sinister_Bot.Custom_Controls.tb();
            this.tpChat = new System.Windows.Forms.TabPage();
            this.gbControl = new Sinister_Bot.Custom_Controls.gb();
            this.btnViewerUnban = new System.Windows.Forms.Button();
            this.btnViewerGiveCurrency = new System.Windows.Forms.Button();
            this.btnUndefined = new System.Windows.Forms.Button();
            this.btnSpam = new System.Windows.Forms.Button();
            this.btnBlacklist = new System.Windows.Forms.Button();
            this.btnViewerBan = new System.Windows.Forms.Button();
            this.btnViewerTimeout = new System.Windows.Forms.Button();
            this.btnViewerWarning = new System.Windows.Forms.Button();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.gbChatDisplay = new Sinister_Bot.Custom_Controls.gb();
            this.tbChat = new Sinister_Bot.Custom_Controls.tb();
            this.btnMessageSend = new System.Windows.Forms.Button();
            this.tbMessageSend = new Sinister_Bot.Custom_Controls.tb();
            this.tpCommands = new System.Windows.Forms.TabPage();
            this.gbCreateCommand = new Sinister_Bot.Custom_Controls.gb();
            this.cbCreateCommandUserlvl = new System.Windows.Forms.ComboBox();
            this.cbCreateCommandUnit = new System.Windows.Forms.ComboBox();
            this.chkbCreateCommandEditable = new System.Windows.Forms.CheckBox();
            this.tbCreateCommandAlias = new Sinister_Bot.Custom_Controls.tb();
            this.lblCreateCommandAlias = new System.Windows.Forms.Label();
            this.btnCommandCancel = new System.Windows.Forms.Button();
            this.btnCommandSave = new System.Windows.Forms.Button();
            this.lblCreateCommandUserlvl = new System.Windows.Forms.Label();
            this.tbCreateCommandCooldown = new Sinister_Bot.Custom_Controls.tb_int();
            this.lblCreateCommandCooldown = new System.Windows.Forms.Label();
            this.tbCreateCommandMessage = new Sinister_Bot.Custom_Controls.tb();
            this.lblCreateCommandMessage = new System.Windows.Forms.Label();
            this.tbCreateCommandName = new Sinister_Bot.Custom_Controls.tb();
            this.lblCreateCommandName = new System.Windows.Forms.Label();
            this.gbCommandList = new Sinister_Bot.Custom_Controls.gb();
            this.lbCommandlist = new System.Windows.Forms.ListBox();
            this.btnDeleteSelectedCommand = new System.Windows.Forms.Button();
            this.btnEditSelectedCommand = new System.Windows.Forms.Button();
            this.tpQuote = new System.Windows.Forms.TabPage();
            this.gbEditQuote = new Sinister_Bot.Custom_Controls.gb();
            this.btnEditQuoteFinished = new System.Windows.Forms.Button();
            this.tbEditQuoteText = new Sinister_Bot.Custom_Controls.tb();
            this.lblEditQuoteText = new System.Windows.Forms.Label();
            this.gbCreateQuote = new Sinister_Bot.Custom_Controls.gb();
            this.btnCreateQuote = new System.Windows.Forms.Button();
            this.tbCreateQuoteText = new Sinister_Bot.Custom_Controls.tb();
            this.lblCreateQuoteText = new System.Windows.Forms.Label();
            this.gbQuotes = new Sinister_Bot.Custom_Controls.gb();
            this.lbQuotes = new System.Windows.Forms.ListBox();
            this.btnDeleteSelectedQuote = new System.Windows.Forms.Button();
            this.btnEditSelectedQuote = new System.Windows.Forms.Button();
            this.tpBet = new System.Windows.Forms.TabPage();
            this.gbBetPast = new Sinister_Bot.Custom_Controls.gb();
            this.lbRecentBets = new System.Windows.Forms.ListBox();
            this.gbBet = new Sinister_Bot.Custom_Controls.gb();
            this.cbBetWin = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbBetSubbonusAmount = new Sinister_Bot.Custom_Controls.tb_int();
            this.cbBetDurationUnit = new System.Windows.Forms.ComboBox();
            this.tbBetDuration = new Sinister_Bot.Custom_Controls.tb_int();
            this.chkbBetDuration = new System.Windows.Forms.CheckBox();
            this.chkbBetSubbonus = new System.Windows.Forms.CheckBox();
            this.btnBetDelete = new System.Windows.Forms.Button();
            this.chkbBetMinInput = new System.Windows.Forms.CheckBox();
            this.tbBetMinInput = new Sinister_Bot.Custom_Controls.tb_int();
            this.btnBetCreate = new System.Windows.Forms.Button();
            this.tbBetWin = new Sinister_Bot.Custom_Controls.tb_int();
            this.lblBetWin = new System.Windows.Forms.Label();
            this.tbBetText = new Sinister_Bot.Custom_Controls.tb();
            this.lblBetText = new System.Windows.Forms.Label();
            this.tbBetName = new Sinister_Bot.Custom_Controls.tb();
            this.lblBetName = new System.Windows.Forms.Label();
            this.gbBetCurrent = new Sinister_Bot.Custom_Controls.gb();
            this.tbBetCurrent = new System.Windows.Forms.TextBox();
            this.btnBetCurrentWon = new System.Windows.Forms.Button();
            this.btnBetCurrentEdit = new System.Windows.Forms.Button();
            this.btnBetCurrentStart = new System.Windows.Forms.Button();
            this.btnBetCurrentLost = new System.Windows.Forms.Button();
            this.btnBetCurrentStop = new System.Windows.Forms.Button();
            this.tpPoll = new System.Windows.Forms.TabPage();
            this.gbPollPrevious = new Sinister_Bot.Custom_Controls.gb();
            this.lbRecentPolls = new System.Windows.Forms.ListBox();
            this.gbPoll = new Sinister_Bot.Custom_Controls.gb();
            this.tbPollOption1 = new Sinister_Bot.Custom_Controls.tb();
            this.lblPollOption1 = new System.Windows.Forms.Label();
            this.tbPollOption2 = new Sinister_Bot.Custom_Controls.tb();
            this.lblPollOption2 = new System.Windows.Forms.Label();
            this.tbPollOption8 = new Sinister_Bot.Custom_Controls.tb();
            this.tbPollOption6 = new Sinister_Bot.Custom_Controls.tb();
            this.lblPollOption8 = new System.Windows.Forms.Label();
            this.lblPollOption6 = new System.Windows.Forms.Label();
            this.tbPollOption7 = new Sinister_Bot.Custom_Controls.tb();
            this.lblPollOption7 = new System.Windows.Forms.Label();
            this.tbPollOption5 = new Sinister_Bot.Custom_Controls.tb();
            this.lblPollOption5 = new System.Windows.Forms.Label();
            this.tbPollOption4 = new Sinister_Bot.Custom_Controls.tb();
            this.lblPollOption4 = new System.Windows.Forms.Label();
            this.tbPollOption3 = new Sinister_Bot.Custom_Controls.tb();
            this.lblPollOption3 = new System.Windows.Forms.Label();
            this.btnPollDelete = new System.Windows.Forms.Button();
            this.btnPollCreate = new System.Windows.Forms.Button();
            this.tbPollQuestion = new Sinister_Bot.Custom_Controls.tb();
            this.lblPollQuestion = new System.Windows.Forms.Label();
            this.tbPollName = new Sinister_Bot.Custom_Controls.tb();
            this.lblPollName = new System.Windows.Forms.Label();
            this.gbPollCurrent = new Sinister_Bot.Custom_Controls.gb();
            this.tbPollCurrent = new System.Windows.Forms.TextBox();
            this.btnPollCurrentEdit = new System.Windows.Forms.Button();
            this.btnPollCurrentStart = new System.Windows.Forms.Button();
            this.btnPollCurrentStop = new System.Windows.Forms.Button();
            this.tpGiveaway = new System.Windows.Forms.TabPage();
            this.gbGiveawayWinner = new Sinister_Bot.Custom_Controls.gb();
            this.cbWinnerSecondChance = new System.Windows.Forms.ComboBox();
            this.lblsecondChance = new System.Windows.Forms.Label();
            this.chkdLbWinnerList = new System.Windows.Forms.CheckedListBox();
            this.cbWinnerNewWinner = new System.Windows.Forms.ComboBox();
            this.lblnewWinner = new System.Windows.Forms.Label();
            this.btnWinnerChooseNew = new System.Windows.Forms.Button();
            this.gbGiveaway = new Sinister_Bot.Custom_Controls.gb();
            this.lblGiveawayMinInputCurrency = new System.Windows.Forms.Label();
            this.tbGiveawayWin = new Sinister_Bot.Custom_Controls.tb();
            this.lblGiveawayWinners = new System.Windows.Forms.Label();
            this.tbGiveawayKeyword = new Sinister_Bot.Custom_Controls.tb();
            this.lblGiveawayKeyword = new System.Windows.Forms.Label();
            this.btnGiveawayCreate = new System.Windows.Forms.Button();
            this.chkbGiveawaMinInput = new System.Windows.Forms.CheckBox();
            this.tbGiveawayWinners = new Sinister_Bot.Custom_Controls.tb();
            this.tbGiveawayMinInput = new Sinister_Bot.Custom_Controls.tb_int();
            this.lblGiveawayText = new System.Windows.Forms.Label();
            this.gbGiveawayCurrent = new Sinister_Bot.Custom_Controls.gb();
            this.tbGiveawayCurrent = new System.Windows.Forms.TextBox();
            this.btnGiveawayCurrentStart = new System.Windows.Forms.Button();
            this.btnGiveawayCurrentReset = new System.Windows.Forms.Button();
            this.btnGiveawayCurrentStop = new System.Windows.Forms.Button();
            this.btnGiveawayCurrentDelete = new System.Windows.Forms.Button();
            this.TabControl.SuspendLayout();
            this.tpSetup.SuspendLayout();
            this.gbStream.SuspendLayout();
            this.tpRewardsystem.SuspendLayout();
            this.gbViewerrankings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvViewerankings)).BeginInit();
            this.gbBasicSettings.SuspendLayout();
            this.tpChat.SuspendLayout();
            this.gbControl.SuspendLayout();
            this.gbChatDisplay.SuspendLayout();
            this.tpCommands.SuspendLayout();
            this.gbCreateCommand.SuspendLayout();
            this.gbCommandList.SuspendLayout();
            this.tpQuote.SuspendLayout();
            this.gbEditQuote.SuspendLayout();
            this.gbCreateQuote.SuspendLayout();
            this.gbQuotes.SuspendLayout();
            this.tpBet.SuspendLayout();
            this.gbBetPast.SuspendLayout();
            this.gbBet.SuspendLayout();
            this.gbBetCurrent.SuspendLayout();
            this.tpPoll.SuspendLayout();
            this.gbPollPrevious.SuspendLayout();
            this.gbPoll.SuspendLayout();
            this.gbPollCurrent.SuspendLayout();
            this.tpGiveaway.SuspendLayout();
            this.gbGiveawayWinner.SuspendLayout();
            this.gbGiveaway.SuspendLayout();
            this.gbGiveawayCurrent.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackColor = System.Drawing.Color.Transparent;
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnMinimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimize.Location = new System.Drawing.Point(741, 9);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(23, 23);
            this.btnMinimize.TabIndex = 13;
            this.btnMinimize.Text = "_";
            this.btnMinimize.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnMinimize.UseVisualStyleBackColor = false;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            // 
            // btnClose
            // 
            this.btnClose.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(767, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(23, 23);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "X";
            this.btnClose.UseMnemonic = false;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblCreator
            // 
            this.lblCreator.AutoSize = true;
            this.lblCreator.BackColor = System.Drawing.Color.Transparent;
            this.lblCreator.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblCreator.Font = new System.Drawing.Font("Segoe Script", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreator.Location = new System.Drawing.Point(626, 514);
            this.lblCreator.Name = "lblCreator";
            this.lblCreator.Size = new System.Drawing.Size(168, 17);
            this.lblCreator.TabIndex = 11;
            this.lblCreator.Text = "v.1.0.1 - made by Draxado";
            // 
            // lblMain
            // 
            this.lblMain.AutoSize = true;
            this.lblMain.BackColor = System.Drawing.Color.Transparent;
            this.lblMain.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblMain.Font = new System.Drawing.Font("Segoe Script", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMain.Location = new System.Drawing.Point(335, 3);
            this.lblMain.Name = "lblMain";
            this.lblMain.Size = new System.Drawing.Size(135, 31);
            this.lblMain.TabIndex = 10;
            this.lblMain.Text = "Sinister Bot";
            // 
            // llblOauthTokenHelp
            // 
            this.llblOauthTokenHelp.ActiveLinkColor = System.Drawing.Color.RoyalBlue;
            this.llblOauthTokenHelp.AutoSize = true;
            this.llblOauthTokenHelp.LinkColor = System.Drawing.Color.RoyalBlue;
            this.llblOauthTokenHelp.Location = new System.Drawing.Point(681, 108);
            this.llblOauthTokenHelp.Name = "llblOauthTokenHelp";
            this.llblOauthTokenHelp.Size = new System.Drawing.Size(47, 19);
            this.llblOauthTokenHelp.TabIndex = 15;
            this.llblOauthTokenHelp.TabStop = true;
            this.llblOauthTokenHelp.Text = "Hilfe?";
            this.ttHelp.SetToolTip(this.llblOauthTokenHelp, "Bitte trage hier den Ouathtoken deines Bots ein. Falls du nicht weißt, woher du d" +
        "en bekommst, klicke einfach!");
            this.llblOauthTokenHelp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llblOauthTokenHelp_LinkClicked);
            // 
            // btnValueEdit
            // 
            this.btnValueEdit.AutoSize = true;
            this.btnValueEdit.Enabled = false;
            this.btnValueEdit.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnValueEdit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnValueEdit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnValueEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnValueEdit.Location = new System.Drawing.Point(587, 128);
            this.btnValueEdit.Name = "btnValueEdit";
            this.btnValueEdit.Size = new System.Drawing.Size(147, 31);
            this.btnValueEdit.TabIndex = 23;
            this.btnValueEdit.Text = "Einheit ändern";
            this.ttHelp.SetToolTip(this.btnValueEdit, "Du kannst deine Viewer nicht nach folgenden Aspekten in Ränge einstufen:\r\n-aktuel" +
        "le Punktzahl\r\n-maximale erreichte Punktzahl\r\n-Zeit im Stream verbracht");
            this.btnValueEdit.UseVisualStyleBackColor = true;
            // 
            // chkbGiveawaySubBonus
            // 
            this.chkbGiveawaySubBonus.AutoSize = true;
            this.chkbGiveawaySubBonus.Location = new System.Drawing.Point(10, 249);
            this.chkbGiveawaySubBonus.Name = "chkbGiveawaySubBonus";
            this.chkbGiveawaySubBonus.Size = new System.Drawing.Size(145, 23);
            this.chkbGiveawaySubBonus.TabIndex = 9;
            this.chkbGiveawaySubBonus.Text = "Subscriberbonus ?";
            this.ttHelp.SetToolTip(this.chkbGiveawaySubBonus, "Subscriber erhalten bei Teilnahme automatisch eine extra Teilnahme");
            this.chkbGiveawaySubBonus.UseVisualStyleBackColor = true;
            // 
            // btnBSCancel
            // 
            this.btnBSCancel.AutoSize = true;
            this.btnBSCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBSCancel.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnBSCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnBSCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnBSCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBSCancel.Location = new System.Drawing.Point(436, 219);
            this.btnBSCancel.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnBSCancel.Name = "btnBSCancel";
            this.btnBSCancel.Size = new System.Drawing.Size(146, 31);
            this.btnBSCancel.TabIndex = 7;
            this.btnBSCancel.Text = "Abbrechen";
            this.btnBSCancel.UseVisualStyleBackColor = true;
            this.btnBSCancel.Click += new System.EventHandler(this.btnBSCancel_Click);
            // 
            // TabControl
            // 
            this.TabControl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TabControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.TabControl.Controls.Add(this.tpSetup);
            this.TabControl.Controls.Add(this.tpRewardsystem);
            this.TabControl.Controls.Add(this.tpChat);
            this.TabControl.Controls.Add(this.tpCommands);
            this.TabControl.Controls.Add(this.tpQuote);
            this.TabControl.Controls.Add(this.tpBet);
            this.TabControl.Controls.Add(this.tpPoll);
            this.TabControl.Controls.Add(this.tpGiveaway);
            this.TabControl.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.TabControl.Font = new System.Drawing.Font("Segoe Script", 9F);
            this.TabControl.Location = new System.Drawing.Point(6, 37);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(788, 478);
            this.TabControl.TabIndex = 14;
            this.TabControl.Deselected += new System.Windows.Forms.TabControlEventHandler(this.TabControl_Deselected);
            // 
            // tpSetup
            // 
            this.tpSetup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tpSetup.Controls.Add(this.gbStream);
            this.tpSetup.Location = new System.Drawing.Point(4, 25);
            this.tpSetup.Name = "tpSetup";
            this.tpSetup.Padding = new System.Windows.Forms.Padding(3);
            this.tpSetup.Size = new System.Drawing.Size(780, 449);
            this.tpSetup.TabIndex = 0;
            this.tpSetup.Text = "Setup";
            // 
            // gbStream
            // 
            this.gbStream.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbStream.Controls.Add(this.btnStreamSave);
            this.gbStream.Controls.Add(this.btnStreamCancel);
            this.gbStream.Controls.Add(this.tbInfo);
            this.gbStream.Controls.Add(this.lblInfo);
            this.gbStream.Controls.Add(this.llblOauthTokenHelp);
            this.gbStream.Controls.Add(this.tbDBon);
            this.gbStream.Controls.Add(this.tbDBoff);
            this.gbStream.Controls.Add(this.tbToken);
            this.gbStream.Controls.Add(this.tbBotchannel);
            this.gbStream.Controls.Add(this.tbStreamchannel);
            this.gbStream.Controls.Add(this.lblDatabaseWeb);
            this.gbStream.Controls.Add(this.lblDatabaseFile);
            this.gbStream.Controls.Add(this.lblOauthToken);
            this.gbStream.Controls.Add(this.lblBotchannel);
            this.gbStream.Controls.Add(this.lblStreamchannel);
            this.gbStream.Location = new System.Drawing.Point(5, 5);
            this.gbStream.Name = "gbStream";
            this.gbStream.Size = new System.Drawing.Size(740, 438);
            this.gbStream.TabIndex = 2;
            this.gbStream.TabStop = false;
            this.gbStream.Text = "Stream";
            // 
            // btnStreamSave
            // 
            this.btnStreamSave.AutoSize = true;
            this.btnStreamSave.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnStreamSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnStreamSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnStreamSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStreamSave.Location = new System.Drawing.Point(588, 404);
            this.btnStreamSave.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnStreamSave.Name = "btnStreamSave";
            this.btnStreamSave.Size = new System.Drawing.Size(146, 31);
            this.btnStreamSave.TabIndex = 7;
            this.btnStreamSave.Text = "Speichern";
            this.btnStreamSave.UseVisualStyleBackColor = false;
            this.btnStreamSave.Click += new System.EventHandler(this.btnBSSave_Click);
            // 
            // btnStreamCancel
            // 
            this.btnStreamCancel.AutoSize = true;
            this.btnStreamCancel.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnStreamCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnStreamCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnStreamCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStreamCancel.Location = new System.Drawing.Point(436, 404);
            this.btnStreamCancel.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnStreamCancel.Name = "btnStreamCancel";
            this.btnStreamCancel.Size = new System.Drawing.Size(146, 31);
            this.btnStreamCancel.TabIndex = 6;
            this.btnStreamCancel.Text = "Abbrechen";
            this.btnStreamCancel.UseVisualStyleBackColor = false;
            this.btnStreamCancel.Click += new System.EventHandler(this.btnBSCancel_Click);
            // 
            // tbInfo
            // 
            this.tbInfo.AccessibleName = "Info";
            this.tbInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbInfo.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbInfo.Location = new System.Drawing.Point(182, 232);
            this.tbInfo.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbInfo.Multiline = true;
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.Size = new System.Drawing.Size(552, 163);
            this.tbInfo.TabIndex = 5;
            this.tbInfo.Text = "keine Info";
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Location = new System.Drawing.Point(8, 234);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(61, 19);
            this.lblInfo.TabIndex = 16;
            this.lblInfo.Text = "Bio/Info";
            // 
            // tbDBon
            // 
            this.tbDBon.AccessibleName = "DBon";
            this.tbDBon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbDBon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDBon.Enabled = false;
            this.tbDBon.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbDBon.Location = new System.Drawing.Point(182, 190);
            this.tbDBon.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbDBon.Name = "tbDBon";
            this.tbDBon.ReadOnly = true;
            this.tbDBon.Size = new System.Drawing.Size(552, 27);
            this.tbDBon.TabIndex = 4;
            this.tbDBon.Text = "Momentan noch nicht verfügbar!";
            this.tbDBon.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbDBoff
            // 
            this.tbDBoff.AccessibleName = "DBoff";
            this.tbDBoff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbDBoff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDBoff.Enabled = false;
            this.tbDBoff.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbDBoff.Location = new System.Drawing.Point(182, 149);
            this.tbDBoff.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbDBoff.Name = "tbDBoff";
            this.tbDBoff.ReadOnly = true;
            this.tbDBoff.Size = new System.Drawing.Size(552, 27);
            this.tbDBoff.TabIndex = 3;
            this.tbDBoff.Text = "Momentan nicht veränderbar!";
            this.tbDBoff.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbToken
            // 
            this.tbToken.AccessibleName = "Token";
            this.tbToken.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbToken.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbToken.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbToken.Location = new System.Drawing.Point(182, 105);
            this.tbToken.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbToken.Name = "tbToken";
            this.tbToken.Size = new System.Drawing.Size(494, 27);
            this.tbToken.TabIndex = 2;
            this.tbToken.Leave += new System.EventHandler(this.tbToken_Leave);
            // 
            // tbBotchannel
            // 
            this.tbBotchannel.AccessibleName = "Botchannel";
            this.tbBotchannel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbBotchannel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbBotchannel.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbBotchannel.Location = new System.Drawing.Point(182, 67);
            this.tbBotchannel.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbBotchannel.Name = "tbBotchannel";
            this.tbBotchannel.Size = new System.Drawing.Size(552, 27);
            this.tbBotchannel.TabIndex = 1;
            this.tbBotchannel.Leave += new System.EventHandler(this.tbBotchannel_Leave);
            // 
            // tbStreamchannel
            // 
            this.tbStreamchannel.AccessibleName = "Streamchannel";
            this.tbStreamchannel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbStreamchannel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbStreamchannel.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbStreamchannel.Location = new System.Drawing.Point(182, 25);
            this.tbStreamchannel.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbStreamchannel.Name = "tbStreamchannel";
            this.tbStreamchannel.Size = new System.Drawing.Size(552, 27);
            this.tbStreamchannel.TabIndex = 0;
            this.tbStreamchannel.Leave += new System.EventHandler(this.tbStreamchannel_Leave);
            // 
            // lblDatabaseWeb
            // 
            this.lblDatabaseWeb.AutoSize = true;
            this.lblDatabaseWeb.Location = new System.Drawing.Point(8, 192);
            this.lblDatabaseWeb.Name = "lblDatabaseWeb";
            this.lblDatabaseWeb.Size = new System.Drawing.Size(135, 19);
            this.lblDatabaseWeb.TabIndex = 4;
            this.lblDatabaseWeb.Text = "Datenbank (online)";
            // 
            // lblDatabaseFile
            // 
            this.lblDatabaseFile.AutoSize = true;
            this.lblDatabaseFile.Location = new System.Drawing.Point(8, 150);
            this.lblDatabaseFile.Name = "lblDatabaseFile";
            this.lblDatabaseFile.Size = new System.Drawing.Size(136, 19);
            this.lblDatabaseFile.TabIndex = 3;
            this.lblDatabaseFile.Text = "Datenbank (offline)";
            // 
            // lblOauthToken
            // 
            this.lblOauthToken.AutoSize = true;
            this.lblOauthToken.Location = new System.Drawing.Point(8, 108);
            this.lblOauthToken.Name = "lblOauthToken";
            this.lblOauthToken.Size = new System.Drawing.Size(116, 19);
            this.lblOauthToken.TabIndex = 2;
            this.lblOauthToken.Text = "Bot Oauth Token";
            // 
            // lblBotchannel
            // 
            this.lblBotchannel.AutoSize = true;
            this.lblBotchannel.Location = new System.Drawing.Point(8, 68);
            this.lblBotchannel.Name = "lblBotchannel";
            this.lblBotchannel.Size = new System.Drawing.Size(81, 19);
            this.lblBotchannel.TabIndex = 1;
            this.lblBotchannel.Text = "Botchannel";
            // 
            // lblStreamchannel
            // 
            this.lblStreamchannel.AutoSize = true;
            this.lblStreamchannel.Location = new System.Drawing.Point(8, 25);
            this.lblStreamchannel.Name = "lblStreamchannel";
            this.lblStreamchannel.Size = new System.Drawing.Size(105, 19);
            this.lblStreamchannel.TabIndex = 0;
            this.lblStreamchannel.Text = "Streamchannel";
            // 
            // tpRewardsystem
            // 
            this.tpRewardsystem.AutoScroll = true;
            this.tpRewardsystem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tpRewardsystem.Controls.Add(this.gbViewerrankings);
            this.tpRewardsystem.Controls.Add(this.gbBasicSettings);
            this.tpRewardsystem.Location = new System.Drawing.Point(4, 25);
            this.tpRewardsystem.Name = "tpRewardsystem";
            this.tpRewardsystem.Size = new System.Drawing.Size(780, 449);
            this.tpRewardsystem.TabIndex = 7;
            this.tpRewardsystem.Text = "Rewardsystem";
            // 
            // gbViewerrankings
            // 
            this.gbViewerrankings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbViewerrankings.Controls.Add(this.btnDgvSortOrder);
            this.gbViewerrankings.Controls.Add(this.btnDgvSave);
            this.gbViewerrankings.Controls.Add(this.btnValueEdit);
            this.gbViewerrankings.Controls.Add(this.btnDgvCancel);
            this.gbViewerrankings.Controls.Add(this.btnRankDelete);
            this.gbViewerrankings.Controls.Add(this.btnRankEdit);
            this.gbViewerrankings.Controls.Add(this.btnRankAdd);
            this.gbViewerrankings.Controls.Add(this.dgvViewerankings);
            this.gbViewerrankings.Location = new System.Drawing.Point(5, 262);
            this.gbViewerrankings.Name = "gbViewerrankings";
            this.gbViewerrankings.Size = new System.Drawing.Size(740, 288);
            this.gbViewerrankings.TabIndex = 9;
            this.gbViewerrankings.TabStop = false;
            this.gbViewerrankings.Text = "Viewerrankings";
            // 
            // btnDgvSortOrder
            // 
            this.btnDgvSortOrder.AutoSize = true;
            this.btnDgvSortOrder.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnDgvSortOrder.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnDgvSortOrder.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnDgvSortOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDgvSortOrder.Font = new System.Drawing.Font("Segoe Script", 8F);
            this.btnDgvSortOrder.Location = new System.Drawing.Point(587, 165);
            this.btnDgvSortOrder.Name = "btnDgvSortOrder";
            this.btnDgvSortOrder.Size = new System.Drawing.Size(147, 31);
            this.btnDgvSortOrder.TabIndex = 32;
            this.btnDgvSortOrder.Text = "Absteigend sortieren";
            this.btnDgvSortOrder.UseVisualStyleBackColor = true;
            this.btnDgvSortOrder.Click += new System.EventHandler(this.btnDgvSortOrder_Click);
            // 
            // btnDgvSave
            // 
            this.btnDgvSave.AutoSize = true;
            this.btnDgvSave.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnDgvSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnDgvSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnDgvSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDgvSave.Location = new System.Drawing.Point(587, 254);
            this.btnDgvSave.Name = "btnDgvSave";
            this.btnDgvSave.Size = new System.Drawing.Size(147, 31);
            this.btnDgvSave.TabIndex = 31;
            this.btnDgvSave.Text = "Speichern";
            this.btnDgvSave.UseVisualStyleBackColor = true;
            this.btnDgvSave.Click += new System.EventHandler(this.btnBSSave_Click);
            // 
            // btnDgvCancel
            // 
            this.btnDgvCancel.AutoSize = true;
            this.btnDgvCancel.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnDgvCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnDgvCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnDgvCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDgvCancel.Location = new System.Drawing.Point(587, 220);
            this.btnDgvCancel.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnDgvCancel.Name = "btnDgvCancel";
            this.btnDgvCancel.Size = new System.Drawing.Size(147, 31);
            this.btnDgvCancel.TabIndex = 30;
            this.btnDgvCancel.Text = "Abbrechen";
            this.btnDgvCancel.UseVisualStyleBackColor = true;
            this.btnDgvCancel.Click += new System.EventHandler(this.btnBSCancel_Click);
            // 
            // btnRankDelete
            // 
            this.btnRankDelete.AutoSize = true;
            this.btnRankDelete.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnRankDelete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnRankDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnRankDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRankDelete.Location = new System.Drawing.Point(587, 91);
            this.btnRankDelete.Name = "btnRankDelete";
            this.btnRankDelete.Size = new System.Drawing.Size(147, 31);
            this.btnRankDelete.TabIndex = 22;
            this.btnRankDelete.Text = "Rang entfernen";
            this.btnRankDelete.UseVisualStyleBackColor = true;
            this.btnRankDelete.Click += new System.EventHandler(this.btnRankDelete_Click);
            // 
            // btnRankEdit
            // 
            this.btnRankEdit.AutoSize = true;
            this.btnRankEdit.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnRankEdit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnRankEdit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnRankEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRankEdit.Location = new System.Drawing.Point(587, 54);
            this.btnRankEdit.Name = "btnRankEdit";
            this.btnRankEdit.Size = new System.Drawing.Size(147, 31);
            this.btnRankEdit.TabIndex = 21;
            this.btnRankEdit.Text = "Rang bearbeiten";
            this.btnRankEdit.UseVisualStyleBackColor = true;
            this.btnRankEdit.Click += new System.EventHandler(this.btnRank_Click);
            // 
            // btnRankAdd
            // 
            this.btnRankAdd.AutoSize = true;
            this.btnRankAdd.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnRankAdd.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnRankAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnRankAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRankAdd.Location = new System.Drawing.Point(587, 17);
            this.btnRankAdd.Name = "btnRankAdd";
            this.btnRankAdd.Size = new System.Drawing.Size(147, 31);
            this.btnRankAdd.TabIndex = 1;
            this.btnRankAdd.Text = "Rang hinzufügen";
            this.btnRankAdd.UseVisualStyleBackColor = true;
            this.btnRankAdd.Click += new System.EventHandler(this.btnRank_Click);
            // 
            // dgvViewerankings
            // 
            this.dgvViewerankings.AllowUserToAddRows = false;
            this.dgvViewerankings.AllowUserToDeleteRows = false;
            this.dgvViewerankings.AllowUserToResizeColumns = false;
            this.dgvViewerankings.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe Script", 9F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.dgvViewerankings.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvViewerankings.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvViewerankings.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvViewerankings.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.dgvViewerankings.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvViewerankings.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgvViewerankings.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Segoe Script", 9F);
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvViewerankings.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvViewerankings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvViewerankings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmnRank,
            this.clmnPointsNeeded,
            this.clmnCount});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Segoe Script", 9F);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvViewerankings.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgvViewerankings.EnableHeadersVisualStyles = false;
            this.dgvViewerankings.GridColor = System.Drawing.Color.RoyalBlue;
            this.dgvViewerankings.Location = new System.Drawing.Point(6, 17);
            this.dgvViewerankings.MultiSelect = false;
            this.dgvViewerankings.Name = "dgvViewerankings";
            this.dgvViewerankings.ReadOnly = true;
            this.dgvViewerankings.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Segoe Script", 9F);
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvViewerankings.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvViewerankings.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.dgvViewerankings.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvViewerankings.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgvViewerankings.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.dgvViewerankings.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Segoe Script", 9F);
            this.dgvViewerankings.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.RoyalBlue;
            this.dgvViewerankings.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.dgvViewerankings.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgvViewerankings.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvViewerankings.RowTemplate.ReadOnly = true;
            this.dgvViewerankings.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvViewerankings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvViewerankings.Size = new System.Drawing.Size(575, 239);
            this.dgvViewerankings.TabIndex = 20;
            this.dgvViewerankings.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvViewerankings_CellPainting);
            // 
            // clmnRank
            // 
            this.clmnRank.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe Script", 9F);
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.clmnRank.DefaultCellStyle = dataGridViewCellStyle11;
            this.clmnRank.HeaderText = "Rang";
            this.clmnRank.Name = "clmnRank";
            this.clmnRank.ReadOnly = true;
            this.clmnRank.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // clmnPointsNeeded
            // 
            this.clmnPointsNeeded.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Segoe Script", 9F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.clmnPointsNeeded.DefaultCellStyle = dataGridViewCellStyle12;
            this.clmnPointsNeeded.HeaderText = "benötigte Punkte";
            this.clmnPointsNeeded.Name = "clmnPointsNeeded";
            this.clmnPointsNeeded.ReadOnly = true;
            this.clmnPointsNeeded.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // clmnCount
            // 
            this.clmnCount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Segoe Script", 9F);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.clmnCount.DefaultCellStyle = dataGridViewCellStyle13;
            this.clmnCount.HeaderText = "Anzahl";
            this.clmnCount.Name = "clmnCount";
            this.clmnCount.ReadOnly = true;
            this.clmnCount.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // gbBasicSettings
            // 
            this.gbBasicSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbBasicSettings.Controls.Add(this.btnBSSave);
            this.gbBasicSettings.Controls.Add(this.btnBSCancel);
            this.gbBasicSettings.Controls.Add(this.cbCurrencyUpdateUnit);
            this.gbBasicSettings.Controls.Add(this.tbCurrencyCommandMessage);
            this.gbBasicSettings.Controls.Add(this.lblCommandAnswer);
            this.gbBasicSettings.Controls.Add(this.tbCurrencyCommand);
            this.gbBasicSettings.Controls.Add(this.lblCurrencyCommand);
            this.gbBasicSettings.Controls.Add(this.lblCurrency2);
            this.gbBasicSettings.Controls.Add(this.lblCurrency);
            this.gbBasicSettings.Controls.Add(this.lblCurrencyReward);
            this.gbBasicSettings.Controls.Add(this.lblCurrencyUpdate);
            this.gbBasicSettings.Controls.Add(this.lblCurrencyStart);
            this.gbBasicSettings.Controls.Add(this.lblCurrencyName);
            this.gbBasicSettings.Controls.Add(this.tbCurrencyIncrease);
            this.gbBasicSettings.Controls.Add(this.tbCurrencyUpdate);
            this.gbBasicSettings.Controls.Add(this.tbCurrencyStart);
            this.gbBasicSettings.Controls.Add(this.tbCurrencyName);
            this.gbBasicSettings.Location = new System.Drawing.Point(5, 5);
            this.gbBasicSettings.Name = "gbBasicSettings";
            this.gbBasicSettings.Size = new System.Drawing.Size(740, 253);
            this.gbBasicSettings.TabIndex = 8;
            this.gbBasicSettings.TabStop = false;
            this.gbBasicSettings.Text = "Grundeinstellungen";
            // 
            // btnBSSave
            // 
            this.btnBSSave.AutoSize = true;
            this.btnBSSave.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBSSave.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnBSSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnBSSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnBSSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBSSave.Location = new System.Drawing.Point(588, 219);
            this.btnBSSave.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnBSSave.Name = "btnBSSave";
            this.btnBSSave.Size = new System.Drawing.Size(146, 31);
            this.btnBSSave.TabIndex = 8;
            this.btnBSSave.Text = "Speichern";
            this.btnBSSave.UseVisualStyleBackColor = true;
            this.btnBSSave.Click += new System.EventHandler(this.btnBSSave_Click);
            // 
            // cbCurrencyUpdateUnit
            // 
            this.cbCurrencyUpdateUnit.AccessibleName = "Aktualisierungseinheit";
            this.cbCurrencyUpdateUnit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.cbCurrencyUpdateUnit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbCurrencyUpdateUnit.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cbCurrencyUpdateUnit.FormattingEnabled = true;
            this.cbCurrencyUpdateUnit.Items.AddRange(new object[] {
            "Sekunden",
            "Minuten"});
            this.cbCurrencyUpdateUnit.Location = new System.Drawing.Point(296, 183);
            this.cbCurrencyUpdateUnit.Name = "cbCurrencyUpdateUnit";
            this.cbCurrencyUpdateUnit.Size = new System.Drawing.Size(121, 27);
            this.cbCurrencyUpdateUnit.TabIndex = 6;
            this.cbCurrencyUpdateUnit.SelectedIndexChanged += new System.EventHandler(this.cbCurrencyUpdateUnit_SelectedIndexChanged);
            // 
            // tbCurrencyCommandMessage
            // 
            this.tbCurrencyCommandMessage.AccessibleName = "Commandnachricht";
            this.tbCurrencyCommandMessage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbCurrencyCommandMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCurrencyCommandMessage.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbCurrencyCommandMessage.Location = new System.Drawing.Point(179, 64);
            this.tbCurrencyCommandMessage.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbCurrencyCommandMessage.Multiline = true;
            this.tbCurrencyCommandMessage.Name = "tbCurrencyCommandMessage";
            this.tbCurrencyCommandMessage.Size = new System.Drawing.Size(503, 62);
            this.tbCurrencyCommandMessage.TabIndex = 2;
            this.tbCurrencyCommandMessage.Text = "{Name} du hast momentan {Anzahl} {Währung}. Dein aktueller Rang ist {Rang}.";
            // 
            // lblCommandAnswer
            // 
            this.lblCommandAnswer.AutoSize = true;
            this.lblCommandAnswer.Location = new System.Drawing.Point(8, 66);
            this.lblCommandAnswer.Name = "lblCommandAnswer";
            this.lblCommandAnswer.Size = new System.Drawing.Size(138, 19);
            this.lblCommandAnswer.TabIndex = 27;
            this.lblCommandAnswer.Text = "Commandnachricht";
            // 
            // tbCurrencyCommand
            // 
            this.tbCurrencyCommand.AccessibleName = "Command";
            this.tbCurrencyCommand.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbCurrencyCommand.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCurrencyCommand.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbCurrencyCommand.Location = new System.Drawing.Point(491, 23);
            this.tbCurrencyCommand.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbCurrencyCommand.Name = "tbCurrencyCommand";
            this.tbCurrencyCommand.Size = new System.Drawing.Size(191, 27);
            this.tbCurrencyCommand.TabIndex = 1;
            this.tbCurrencyCommand.Text = "!Punkte";
            this.tbCurrencyCommand.TextChanged += new System.EventHandler(this.tbCurrencyCommand_TextChanged);
            // 
            // lblCurrencyCommand
            // 
            this.lblCurrencyCommand.AutoSize = true;
            this.lblCurrencyCommand.Location = new System.Drawing.Point(412, 25);
            this.lblCurrencyCommand.Name = "lblCurrencyCommand";
            this.lblCurrencyCommand.Size = new System.Drawing.Size(73, 19);
            this.lblCurrencyCommand.TabIndex = 25;
            this.lblCurrencyCommand.Text = "Command";
            // 
            // lblCurrency2
            // 
            this.lblCurrency2.AutoSize = true;
            this.lblCurrency2.Location = new System.Drawing.Point(526, 144);
            this.lblCurrency2.Name = "lblCurrency2";
            this.lblCurrency2.Size = new System.Drawing.Size(55, 19);
            this.lblCurrency2.TabIndex = 24;
            this.lblCurrency2.Text = "Punkte";
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.Location = new System.Drawing.Point(183, 144);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(55, 19);
            this.lblCurrency.TabIndex = 23;
            this.lblCurrency.Text = "Punkte";
            // 
            // lblCurrencyReward
            // 
            this.lblCurrencyReward.AutoSize = true;
            this.lblCurrencyReward.Location = new System.Drawing.Point(305, 144);
            this.lblCurrencyReward.Name = "lblCurrencyReward";
            this.lblCurrencyReward.Size = new System.Drawing.Size(77, 19);
            this.lblCurrencyReward.TabIndex = 22;
            this.lblCurrencyReward.Text = "Belohnung";
            // 
            // lblCurrencyUpdate
            // 
            this.lblCurrencyUpdate.AutoSize = true;
            this.lblCurrencyUpdate.Location = new System.Drawing.Point(8, 186);
            this.lblCurrencyUpdate.Name = "lblCurrencyUpdate";
            this.lblCurrencyUpdate.Size = new System.Drawing.Size(167, 19);
            this.lblCurrencyUpdate.TabIndex = 21;
            this.lblCurrencyUpdate.Text = "Aktualisierungsintervall";
            // 
            // lblCurrencyStart
            // 
            this.lblCurrencyStart.AutoSize = true;
            this.lblCurrencyStart.Location = new System.Drawing.Point(8, 144);
            this.lblCurrencyStart.Name = "lblCurrencyStart";
            this.lblCurrencyStart.Size = new System.Drawing.Size(67, 19);
            this.lblCurrencyStart.TabIndex = 20;
            this.lblCurrencyStart.Text = "Startwert";
            // 
            // lblCurrencyName
            // 
            this.lblCurrencyName.AutoSize = true;
            this.lblCurrencyName.Location = new System.Drawing.Point(8, 25);
            this.lblCurrencyName.Name = "lblCurrencyName";
            this.lblCurrencyName.Size = new System.Drawing.Size(155, 19);
            this.lblCurrencyName.TabIndex = 19;
            this.lblCurrencyName.Text = "Name deiner Währung";
            // 
            // tbCurrencyIncrease
            // 
            this.tbCurrencyIncrease.AccessibleName = "Belohnung";
            this.tbCurrencyIncrease.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbCurrencyIncrease.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCurrencyIncrease.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbCurrencyIncrease.Location = new System.Drawing.Point(388, 142);
            this.tbCurrencyIncrease.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbCurrencyIncrease.Name = "tbCurrencyIncrease";
            this.tbCurrencyIncrease.Size = new System.Drawing.Size(122, 27);
            this.tbCurrencyIncrease.TabIndex = 4;
            this.tbCurrencyIncrease.Text = "5";
            // 
            // tbCurrencyUpdate
            // 
            this.tbCurrencyUpdate.AccessibleName = "Aktualisierungsintervall";
            this.tbCurrencyUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbCurrencyUpdate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCurrencyUpdate.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbCurrencyUpdate.Location = new System.Drawing.Point(179, 184);
            this.tbCurrencyUpdate.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbCurrencyUpdate.Name = "tbCurrencyUpdate";
            this.tbCurrencyUpdate.Size = new System.Drawing.Size(105, 27);
            this.tbCurrencyUpdate.TabIndex = 5;
            this.tbCurrencyUpdate.Text = "5";
            // 
            // tbCurrencyStart
            // 
            this.tbCurrencyStart.AccessibleName = "Startwert";
            this.tbCurrencyStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbCurrencyStart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCurrencyStart.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbCurrencyStart.Location = new System.Drawing.Point(81, 142);
            this.tbCurrencyStart.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbCurrencyStart.Name = "tbCurrencyStart";
            this.tbCurrencyStart.Size = new System.Drawing.Size(96, 27);
            this.tbCurrencyStart.TabIndex = 3;
            this.tbCurrencyStart.Text = "10";
            // 
            // tbCurrencyName
            // 
            this.tbCurrencyName.AccessibleName = "Name deiner Währung";
            this.tbCurrencyName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbCurrencyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCurrencyName.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbCurrencyName.Location = new System.Drawing.Point(179, 23);
            this.tbCurrencyName.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbCurrencyName.Name = "tbCurrencyName";
            this.tbCurrencyName.Size = new System.Drawing.Size(191, 27);
            this.tbCurrencyName.TabIndex = 0;
            this.tbCurrencyName.Text = "Punkte";
            this.tbCurrencyName.TextChanged += new System.EventHandler(this.tbCurrencyName_TextChanged);
            // 
            // tpChat
            // 
            this.tpChat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tpChat.Controls.Add(this.gbControl);
            this.tpChat.Controls.Add(this.gbChatDisplay);
            this.tpChat.Location = new System.Drawing.Point(4, 25);
            this.tpChat.Name = "tpChat";
            this.tpChat.Padding = new System.Windows.Forms.Padding(3);
            this.tpChat.Size = new System.Drawing.Size(780, 449);
            this.tpChat.TabIndex = 1;
            this.tpChat.Text = "Chat";
            // 
            // gbControl
            // 
            this.gbControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbControl.Controls.Add(this.btnViewerUnban);
            this.gbControl.Controls.Add(this.btnViewerGiveCurrency);
            this.gbControl.Controls.Add(this.btnUndefined);
            this.gbControl.Controls.Add(this.btnSpam);
            this.gbControl.Controls.Add(this.btnBlacklist);
            this.gbControl.Controls.Add(this.btnViewerBan);
            this.gbControl.Controls.Add(this.btnViewerTimeout);
            this.gbControl.Controls.Add(this.btnViewerWarning);
            this.gbControl.Controls.Add(this.btnDisconnect);
            this.gbControl.Controls.Add(this.btnConnect);
            this.gbControl.Location = new System.Drawing.Point(543, 15);
            this.gbControl.Name = "gbControl";
            this.gbControl.Size = new System.Drawing.Size(195, 428);
            this.gbControl.TabIndex = 21;
            this.gbControl.TabStop = false;
            // 
            // btnViewerUnban
            // 
            this.btnViewerUnban.AutoSize = true;
            this.btnViewerUnban.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnViewerUnban.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnViewerUnban.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnViewerUnban.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewerUnban.Location = new System.Drawing.Point(6, 122);
            this.btnViewerUnban.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnViewerUnban.Name = "btnViewerUnban";
            this.btnViewerUnban.Size = new System.Drawing.Size(183, 31);
            this.btnViewerUnban.TabIndex = 12;
            this.btnViewerUnban.Text = "Viewer unbannen";
            this.btnViewerUnban.UseVisualStyleBackColor = true;
            // 
            // btnViewerGiveCurrency
            // 
            this.btnViewerGiveCurrency.AutoSize = true;
            this.btnViewerGiveCurrency.Enabled = false;
            this.btnViewerGiveCurrency.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnViewerGiveCurrency.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnViewerGiveCurrency.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnViewerGiveCurrency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewerGiveCurrency.Location = new System.Drawing.Point(6, 300);
            this.btnViewerGiveCurrency.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnViewerGiveCurrency.Name = "btnViewerGiveCurrency";
            this.btnViewerGiveCurrency.Size = new System.Drawing.Size(183, 31);
            this.btnViewerGiveCurrency.TabIndex = 11;
            this.btnViewerGiveCurrency.Text = "Viewer Punkte geben";
            this.btnViewerGiveCurrency.UseVisualStyleBackColor = true;
            this.btnViewerGiveCurrency.Click += new System.EventHandler(this.btnGiveViewerCurrency_Click);
            // 
            // btnUndefined
            // 
            this.btnUndefined.AutoSize = true;
            this.btnUndefined.Enabled = false;
            this.btnUndefined.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnUndefined.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnUndefined.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnUndefined.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUndefined.Location = new System.Drawing.Point(6, 249);
            this.btnUndefined.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnUndefined.Name = "btnUndefined";
            this.btnUndefined.Size = new System.Drawing.Size(183, 31);
            this.btnUndefined.TabIndex = 8;
            this.btnUndefined.Text = "??? anschalten";
            this.btnUndefined.UseVisualStyleBackColor = true;
            // 
            // btnSpam
            // 
            this.btnSpam.AutoSize = true;
            this.btnSpam.Enabled = false;
            this.btnSpam.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnSpam.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnSpam.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnSpam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSpam.Location = new System.Drawing.Point(6, 211);
            this.btnSpam.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnSpam.Name = "btnSpam";
            this.btnSpam.Size = new System.Drawing.Size(183, 31);
            this.btnSpam.TabIndex = 7;
            this.btnSpam.Text = "Spamfilter anschalten";
            this.btnSpam.UseVisualStyleBackColor = true;
            // 
            // btnBlacklist
            // 
            this.btnBlacklist.AutoSize = true;
            this.btnBlacklist.Enabled = false;
            this.btnBlacklist.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnBlacklist.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnBlacklist.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnBlacklist.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBlacklist.Location = new System.Drawing.Point(6, 173);
            this.btnBlacklist.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnBlacklist.Name = "btnBlacklist";
            this.btnBlacklist.Size = new System.Drawing.Size(183, 31);
            this.btnBlacklist.TabIndex = 6;
            this.btnBlacklist.Text = "Blacklist anschalten";
            this.btnBlacklist.UseVisualStyleBackColor = true;
            // 
            // btnViewerBan
            // 
            this.btnViewerBan.AutoSize = true;
            this.btnViewerBan.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnViewerBan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnViewerBan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnViewerBan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewerBan.Location = new System.Drawing.Point(6, 81);
            this.btnViewerBan.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnViewerBan.Name = "btnViewerBan";
            this.btnViewerBan.Size = new System.Drawing.Size(183, 31);
            this.btnViewerBan.TabIndex = 5;
            this.btnViewerBan.Text = "Viewer bannen";
            this.btnViewerBan.UseVisualStyleBackColor = true;
            this.btnViewerBan.Click += new System.EventHandler(this.btnViewerBan_Click);
            // 
            // btnViewerTimeout
            // 
            this.btnViewerTimeout.AutoSize = true;
            this.btnViewerTimeout.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnViewerTimeout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnViewerTimeout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnViewerTimeout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewerTimeout.Location = new System.Drawing.Point(6, 43);
            this.btnViewerTimeout.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnViewerTimeout.Name = "btnViewerTimeout";
            this.btnViewerTimeout.Size = new System.Drawing.Size(183, 31);
            this.btnViewerTimeout.TabIndex = 4;
            this.btnViewerTimeout.Text = "Viewer timeouten";
            this.btnViewerTimeout.UseVisualStyleBackColor = true;
            this.btnViewerTimeout.Click += new System.EventHandler(this.btnViewerTimeout_Click);
            // 
            // btnViewerWarning
            // 
            this.btnViewerWarning.AutoSize = true;
            this.btnViewerWarning.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnViewerWarning.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnViewerWarning.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnViewerWarning.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewerWarning.Location = new System.Drawing.Point(6, 5);
            this.btnViewerWarning.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnViewerWarning.Name = "btnViewerWarning";
            this.btnViewerWarning.Size = new System.Drawing.Size(183, 31);
            this.btnViewerWarning.TabIndex = 3;
            this.btnViewerWarning.Text = "Viewer warnen";
            this.btnViewerWarning.UseVisualStyleBackColor = true;
            this.btnViewerWarning.Click += new System.EventHandler(this.btnViewerPurge_Click);
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Enabled = false;
            this.btnDisconnect.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnDisconnect.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnDisconnect.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnDisconnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDisconnect.Location = new System.Drawing.Point(101, 394);
            this.btnDisconnect.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(88, 31);
            this.btnDisconnect.TabIndex = 10;
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.AutoSize = true;
            this.btnConnect.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnConnect.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnConnect.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnConnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConnect.Location = new System.Drawing.Point(6, 394);
            this.btnConnect.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(88, 31);
            this.btnConnect.TabIndex = 9;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // gbChatDisplay
            // 
            this.gbChatDisplay.AccessibleName = "gbChatDisplay";
            this.gbChatDisplay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbChatDisplay.Controls.Add(this.tbChat);
            this.gbChatDisplay.Controls.Add(this.btnMessageSend);
            this.gbChatDisplay.Controls.Add(this.tbMessageSend);
            this.gbChatDisplay.Location = new System.Drawing.Point(5, 5);
            this.gbChatDisplay.Name = "gbChatDisplay";
            this.gbChatDisplay.Size = new System.Drawing.Size(532, 438);
            this.gbChatDisplay.TabIndex = 20;
            this.gbChatDisplay.TabStop = false;
            this.gbChatDisplay.Text = "Chat";
            // 
            // tbChat
            // 
            this.tbChat.AccessibleName = "tbChat";
            this.tbChat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbChat.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbChat.Location = new System.Drawing.Point(6, 15);
            this.tbChat.Multiline = true;
            this.tbChat.Name = "tbChat";
            this.tbChat.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbChat.Size = new System.Drawing.Size(520, 385);
            this.tbChat.TabIndex = 0;
            // 
            // btnMessageSend
            // 
            this.btnMessageSend.AutoSize = true;
            this.btnMessageSend.Enabled = false;
            this.btnMessageSend.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnMessageSend.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnMessageSend.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnMessageSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMessageSend.Location = new System.Drawing.Point(433, 404);
            this.btnMessageSend.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnMessageSend.Name = "btnMessageSend";
            this.btnMessageSend.Size = new System.Drawing.Size(93, 31);
            this.btnMessageSend.TabIndex = 3;
            this.btnMessageSend.Text = "Senden";
            this.btnMessageSend.UseVisualStyleBackColor = true;
            this.btnMessageSend.Click += new System.EventHandler(this.btnMessageSend_Click);
            // 
            // tbMessageSend
            // 
            this.tbMessageSend.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbMessageSend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMessageSend.Enabled = false;
            this.tbMessageSend.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbMessageSend.Location = new System.Drawing.Point(6, 408);
            this.tbMessageSend.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbMessageSend.Name = "tbMessageSend";
            this.tbMessageSend.Size = new System.Drawing.Size(411, 27);
            this.tbMessageSend.TabIndex = 1;
            // 
            // tpCommands
            // 
            this.tpCommands.Controls.Add(this.gbCreateCommand);
            this.tpCommands.Controls.Add(this.gbCommandList);
            this.tpCommands.Location = new System.Drawing.Point(4, 25);
            this.tpCommands.Name = "tpCommands";
            this.tpCommands.Size = new System.Drawing.Size(780, 449);
            this.tpCommands.TabIndex = 2;
            this.tpCommands.Text = "Commands";
            // 
            // gbCreateCommand
            // 
            this.gbCreateCommand.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbCreateCommand.Controls.Add(this.cbCreateCommandUserlvl);
            this.gbCreateCommand.Controls.Add(this.cbCreateCommandUnit);
            this.gbCreateCommand.Controls.Add(this.chkbCreateCommandEditable);
            this.gbCreateCommand.Controls.Add(this.tbCreateCommandAlias);
            this.gbCreateCommand.Controls.Add(this.lblCreateCommandAlias);
            this.gbCreateCommand.Controls.Add(this.btnCommandCancel);
            this.gbCreateCommand.Controls.Add(this.btnCommandSave);
            this.gbCreateCommand.Controls.Add(this.lblCreateCommandUserlvl);
            this.gbCreateCommand.Controls.Add(this.tbCreateCommandCooldown);
            this.gbCreateCommand.Controls.Add(this.lblCreateCommandCooldown);
            this.gbCreateCommand.Controls.Add(this.tbCreateCommandMessage);
            this.gbCreateCommand.Controls.Add(this.lblCreateCommandMessage);
            this.gbCreateCommand.Controls.Add(this.tbCreateCommandName);
            this.gbCreateCommand.Controls.Add(this.lblCreateCommandName);
            this.gbCreateCommand.Location = new System.Drawing.Point(455, 5);
            this.gbCreateCommand.Name = "gbCreateCommand";
            this.gbCreateCommand.Size = new System.Drawing.Size(283, 438);
            this.gbCreateCommand.TabIndex = 23;
            this.gbCreateCommand.TabStop = false;
            this.gbCreateCommand.Text = "Erstellen";
            // 
            // cbCreateCommandUserlvl
            // 
            this.cbCreateCommandUserlvl.AccessibleName = "";
            this.cbCreateCommandUserlvl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.cbCreateCommandUserlvl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbCreateCommandUserlvl.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cbCreateCommandUserlvl.FormattingEnabled = true;
            this.cbCreateCommandUserlvl.Items.AddRange(new object[] {
            "Viewer",
            "Subscriber",
            "Mod",
            "Streamer"});
            this.cbCreateCommandUserlvl.Location = new System.Drawing.Point(110, 337);
            this.cbCreateCommandUserlvl.Name = "cbCreateCommandUserlvl";
            this.cbCreateCommandUserlvl.Size = new System.Drawing.Size(166, 27);
            this.cbCreateCommandUserlvl.TabIndex = 8;
            // 
            // cbCreateCommandUnit
            // 
            this.cbCreateCommandUnit.AccessibleName = "";
            this.cbCreateCommandUnit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.cbCreateCommandUnit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbCreateCommandUnit.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cbCreateCommandUnit.FormattingEnabled = true;
            this.cbCreateCommandUnit.Items.AddRange(new object[] {
            "Sekunden",
            "Minuten"});
            this.cbCreateCommandUnit.Location = new System.Drawing.Point(155, 301);
            this.cbCreateCommandUnit.Name = "cbCreateCommandUnit";
            this.cbCreateCommandUnit.Size = new System.Drawing.Size(121, 27);
            this.cbCreateCommandUnit.TabIndex = 7;
            // 
            // chkbCreateCommandEditable
            // 
            this.chkbCreateCommandEditable.AutoSize = true;
            this.chkbCreateCommandEditable.Location = new System.Drawing.Point(12, 368);
            this.chkbCreateCommandEditable.Name = "chkbCreateCommandEditable";
            this.chkbCreateCommandEditable.Size = new System.Drawing.Size(102, 23);
            this.chkbCreateCommandEditable.TabIndex = 9;
            this.chkbCreateCommandEditable.Text = "Editierbar?";
            this.chkbCreateCommandEditable.UseVisualStyleBackColor = true;
            this.chkbCreateCommandEditable.CheckedChanged += new System.EventHandler(this.chkbCreateCommandEditable_CheckedChanged);
            // 
            // tbCreateCommandAlias
            // 
            this.tbCreateCommandAlias.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbCreateCommandAlias.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCreateCommandAlias.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbCreateCommandAlias.Location = new System.Drawing.Point(85, 57);
            this.tbCreateCommandAlias.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbCreateCommandAlias.Multiline = true;
            this.tbCreateCommandAlias.Name = "tbCreateCommandAlias";
            this.tbCreateCommandAlias.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbCreateCommandAlias.Size = new System.Drawing.Size(191, 103);
            this.tbCreateCommandAlias.TabIndex = 4;
            // 
            // lblCreateCommandAlias
            // 
            this.lblCreateCommandAlias.AutoSize = true;
            this.lblCreateCommandAlias.Location = new System.Drawing.Point(8, 59);
            this.lblCreateCommandAlias.Name = "lblCreateCommandAlias";
            this.lblCreateCommandAlias.Size = new System.Drawing.Size(43, 19);
            this.lblCreateCommandAlias.TabIndex = 36;
            this.lblCreateCommandAlias.Text = "Alias";
            // 
            // btnCommandCancel
            // 
            this.btnCommandCancel.AutoSize = true;
            this.btnCommandCancel.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnCommandCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnCommandCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnCommandCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCommandCancel.Location = new System.Drawing.Point(30, 404);
            this.btnCommandCancel.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnCommandCancel.Name = "btnCommandCancel";
            this.btnCommandCancel.Size = new System.Drawing.Size(120, 31);
            this.btnCommandCancel.TabIndex = 10;
            this.btnCommandCancel.Text = "Abbrechen";
            this.btnCommandCancel.UseVisualStyleBackColor = true;
            this.btnCommandCancel.Click += new System.EventHandler(this.btnCommandCancel_Click);
            // 
            // btnCommandSave
            // 
            this.btnCommandSave.AutoSize = true;
            this.btnCommandSave.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnCommandSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnCommandSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnCommandSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCommandSave.Location = new System.Drawing.Point(156, 404);
            this.btnCommandSave.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnCommandSave.Name = "btnCommandSave";
            this.btnCommandSave.Size = new System.Drawing.Size(120, 31);
            this.btnCommandSave.TabIndex = 11;
            this.btnCommandSave.Text = "Erstellen";
            this.btnCommandSave.UseVisualStyleBackColor = true;
            this.btnCommandSave.Click += new System.EventHandler(this.btnCommandSave_Click);
            // 
            // lblCreateCommandUserlvl
            // 
            this.lblCreateCommandUserlvl.AutoSize = true;
            this.lblCreateCommandUserlvl.Location = new System.Drawing.Point(8, 340);
            this.lblCreateCommandUserlvl.Name = "lblCreateCommandUserlvl";
            this.lblCreateCommandUserlvl.Size = new System.Drawing.Size(96, 19);
            this.lblCreateCommandUserlvl.TabIndex = 34;
            this.lblCreateCommandUserlvl.Text = "Benutzerlevel";
            // 
            // tbCreateCommandCooldown
            // 
            this.tbCreateCommandCooldown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbCreateCommandCooldown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCreateCommandCooldown.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbCreateCommandCooldown.Location = new System.Drawing.Point(85, 301);
            this.tbCreateCommandCooldown.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbCreateCommandCooldown.Name = "tbCreateCommandCooldown";
            this.tbCreateCommandCooldown.Size = new System.Drawing.Size(64, 27);
            this.tbCreateCommandCooldown.TabIndex = 6;
            // 
            // lblCreateCommandCooldown
            // 
            this.lblCreateCommandCooldown.AutoSize = true;
            this.lblCreateCommandCooldown.Location = new System.Drawing.Point(8, 303);
            this.lblCreateCommandCooldown.Name = "lblCreateCommandCooldown";
            this.lblCreateCommandCooldown.Size = new System.Drawing.Size(71, 19);
            this.lblCreateCommandCooldown.TabIndex = 31;
            this.lblCreateCommandCooldown.Text = "Cooldown";
            // 
            // tbCreateCommandMessage
            // 
            this.tbCreateCommandMessage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbCreateCommandMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCreateCommandMessage.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbCreateCommandMessage.Location = new System.Drawing.Point(85, 168);
            this.tbCreateCommandMessage.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbCreateCommandMessage.Multiline = true;
            this.tbCreateCommandMessage.Name = "tbCreateCommandMessage";
            this.tbCreateCommandMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbCreateCommandMessage.Size = new System.Drawing.Size(191, 126);
            this.tbCreateCommandMessage.TabIndex = 5;
            // 
            // lblCreateCommandMessage
            // 
            this.lblCreateCommandMessage.AutoSize = true;
            this.lblCreateCommandMessage.Location = new System.Drawing.Point(8, 170);
            this.lblCreateCommandMessage.Name = "lblCreateCommandMessage";
            this.lblCreateCommandMessage.Size = new System.Drawing.Size(75, 19);
            this.lblCreateCommandMessage.TabIndex = 29;
            this.lblCreateCommandMessage.Text = "Nachricht";
            // 
            // tbCreateCommandName
            // 
            this.tbCreateCommandName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbCreateCommandName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCreateCommandName.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbCreateCommandName.Location = new System.Drawing.Point(85, 24);
            this.tbCreateCommandName.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbCreateCommandName.Name = "tbCreateCommandName";
            this.tbCreateCommandName.Size = new System.Drawing.Size(191, 27);
            this.tbCreateCommandName.TabIndex = 3;
            // 
            // lblCreateCommandName
            // 
            this.lblCreateCommandName.AutoSize = true;
            this.lblCreateCommandName.Location = new System.Drawing.Point(8, 26);
            this.lblCreateCommandName.Name = "lblCreateCommandName";
            this.lblCreateCommandName.Size = new System.Drawing.Size(44, 19);
            this.lblCreateCommandName.TabIndex = 27;
            this.lblCreateCommandName.Text = "Name";
            // 
            // gbCommandList
            // 
            this.gbCommandList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbCommandList.Controls.Add(this.lbCommandlist);
            this.gbCommandList.Controls.Add(this.btnDeleteSelectedCommand);
            this.gbCommandList.Controls.Add(this.btnEditSelectedCommand);
            this.gbCommandList.Location = new System.Drawing.Point(5, 5);
            this.gbCommandList.Name = "gbCommandList";
            this.gbCommandList.Size = new System.Drawing.Size(444, 438);
            this.gbCommandList.TabIndex = 22;
            this.gbCommandList.TabStop = false;
            this.gbCommandList.Text = "Command Liste";
            // 
            // lbCommandlist
            // 
            this.lbCommandlist.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.lbCommandlist.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbCommandlist.FormattingEnabled = true;
            this.lbCommandlist.ItemHeight = 19;
            this.lbCommandlist.Location = new System.Drawing.Point(6, 21);
            this.lbCommandlist.Name = "lbCommandlist";
            this.lbCommandlist.Size = new System.Drawing.Size(432, 365);
            this.lbCommandlist.TabIndex = 25;
            this.lbCommandlist.UseTabStops = false;
            this.lbCommandlist.DoubleClick += new System.EventHandler(this.lbCommandlist_DoubleClick);
            // 
            // btnDeleteSelectedCommand
            // 
            this.btnDeleteSelectedCommand.AutoSize = true;
            this.btnDeleteSelectedCommand.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnDeleteSelectedCommand.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnDeleteSelectedCommand.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnDeleteSelectedCommand.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteSelectedCommand.Location = new System.Drawing.Point(318, 404);
            this.btnDeleteSelectedCommand.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnDeleteSelectedCommand.Name = "btnDeleteSelectedCommand";
            this.btnDeleteSelectedCommand.Size = new System.Drawing.Size(120, 31);
            this.btnDeleteSelectedCommand.TabIndex = 2;
            this.btnDeleteSelectedCommand.Text = "Löschen";
            this.btnDeleteSelectedCommand.UseVisualStyleBackColor = true;
            this.btnDeleteSelectedCommand.Click += new System.EventHandler(this.btnDeleteSelectedCommand_Click);
            // 
            // btnEditSelectedCommand
            // 
            this.btnEditSelectedCommand.AutoSize = true;
            this.btnEditSelectedCommand.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnEditSelectedCommand.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnEditSelectedCommand.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnEditSelectedCommand.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditSelectedCommand.Location = new System.Drawing.Point(192, 404);
            this.btnEditSelectedCommand.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnEditSelectedCommand.Name = "btnEditSelectedCommand";
            this.btnEditSelectedCommand.Size = new System.Drawing.Size(120, 31);
            this.btnEditSelectedCommand.TabIndex = 1;
            this.btnEditSelectedCommand.Text = "Bearbeiten";
            this.btnEditSelectedCommand.UseVisualStyleBackColor = true;
            this.btnEditSelectedCommand.Click += new System.EventHandler(this.btnEditSelectedCommand_Click);
            // 
            // tpQuote
            // 
            this.tpQuote.Controls.Add(this.gbEditQuote);
            this.tpQuote.Controls.Add(this.gbCreateQuote);
            this.tpQuote.Controls.Add(this.gbQuotes);
            this.tpQuote.Location = new System.Drawing.Point(4, 25);
            this.tpQuote.Name = "tpQuote";
            this.tpQuote.Size = new System.Drawing.Size(780, 449);
            this.tpQuote.TabIndex = 6;
            this.tpQuote.Text = "Quotes";
            // 
            // gbEditQuote
            // 
            this.gbEditQuote.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbEditQuote.Controls.Add(this.btnEditQuoteFinished);
            this.gbEditQuote.Controls.Add(this.tbEditQuoteText);
            this.gbEditQuote.Controls.Add(this.lblEditQuoteText);
            this.gbEditQuote.Location = new System.Drawing.Point(455, 215);
            this.gbEditQuote.Name = "gbEditQuote";
            this.gbEditQuote.Size = new System.Drawing.Size(283, 228);
            this.gbEditQuote.TabIndex = 27;
            this.gbEditQuote.TabStop = false;
            this.gbEditQuote.Text = "Bearbeiten";
            // 
            // btnEditQuoteFinished
            // 
            this.btnEditQuoteFinished.AutoSize = true;
            this.btnEditQuoteFinished.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnEditQuoteFinished.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnEditQuoteFinished.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnEditQuoteFinished.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditQuoteFinished.Location = new System.Drawing.Point(157, 194);
            this.btnEditQuoteFinished.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnEditQuoteFinished.Name = "btnEditQuoteFinished";
            this.btnEditQuoteFinished.Size = new System.Drawing.Size(120, 31);
            this.btnEditQuoteFinished.TabIndex = 36;
            this.btnEditQuoteFinished.Text = "Ändern";
            this.btnEditQuoteFinished.UseVisualStyleBackColor = true;
            this.btnEditQuoteFinished.Click += new System.EventHandler(this.btnEditQuoteFinished_Click);
            // 
            // tbEditQuoteText
            // 
            this.tbEditQuoteText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbEditQuoteText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbEditQuoteText.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Italic);
            this.tbEditQuoteText.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbEditQuoteText.Location = new System.Drawing.Point(83, 23);
            this.tbEditQuoteText.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbEditQuoteText.Multiline = true;
            this.tbEditQuoteText.Name = "tbEditQuoteText";
            this.tbEditQuoteText.Size = new System.Drawing.Size(191, 160);
            this.tbEditQuoteText.TabIndex = 30;
            // 
            // lblEditQuoteText
            // 
            this.lblEditQuoteText.AutoSize = true;
            this.lblEditQuoteText.Location = new System.Drawing.Point(6, 25);
            this.lblEditQuoteText.Name = "lblEditQuoteText";
            this.lblEditQuoteText.Size = new System.Drawing.Size(35, 19);
            this.lblEditQuoteText.TabIndex = 29;
            this.lblEditQuoteText.Text = "Text";
            // 
            // gbCreateQuote
            // 
            this.gbCreateQuote.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbCreateQuote.Controls.Add(this.btnCreateQuote);
            this.gbCreateQuote.Controls.Add(this.tbCreateQuoteText);
            this.gbCreateQuote.Controls.Add(this.lblCreateQuoteText);
            this.gbCreateQuote.Location = new System.Drawing.Point(455, 5);
            this.gbCreateQuote.Name = "gbCreateQuote";
            this.gbCreateQuote.Size = new System.Drawing.Size(283, 210);
            this.gbCreateQuote.TabIndex = 26;
            this.gbCreateQuote.TabStop = false;
            this.gbCreateQuote.Text = "Erstellen";
            // 
            // btnCreateQuote
            // 
            this.btnCreateQuote.AutoSize = true;
            this.btnCreateQuote.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnCreateQuote.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnCreateQuote.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnCreateQuote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreateQuote.Location = new System.Drawing.Point(157, 176);
            this.btnCreateQuote.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnCreateQuote.Name = "btnCreateQuote";
            this.btnCreateQuote.Size = new System.Drawing.Size(120, 31);
            this.btnCreateQuote.TabIndex = 36;
            this.btnCreateQuote.Text = "Erstellen";
            this.btnCreateQuote.UseVisualStyleBackColor = true;
            this.btnCreateQuote.Click += new System.EventHandler(this.btnCreateQuote_Click);
            // 
            // tbCreateQuoteText
            // 
            this.tbCreateQuoteText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbCreateQuoteText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCreateQuoteText.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Italic);
            this.tbCreateQuoteText.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbCreateQuoteText.Location = new System.Drawing.Point(83, 24);
            this.tbCreateQuoteText.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbCreateQuoteText.Multiline = true;
            this.tbCreateQuoteText.Name = "tbCreateQuoteText";
            this.tbCreateQuoteText.Size = new System.Drawing.Size(191, 137);
            this.tbCreateQuoteText.TabIndex = 30;
            // 
            // lblCreateQuoteText
            // 
            this.lblCreateQuoteText.AutoSize = true;
            this.lblCreateQuoteText.Location = new System.Drawing.Point(6, 28);
            this.lblCreateQuoteText.Name = "lblCreateQuoteText";
            this.lblCreateQuoteText.Size = new System.Drawing.Size(35, 19);
            this.lblCreateQuoteText.TabIndex = 29;
            this.lblCreateQuoteText.Text = "Text";
            // 
            // gbQuotes
            // 
            this.gbQuotes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbQuotes.Controls.Add(this.lbQuotes);
            this.gbQuotes.Controls.Add(this.btnDeleteSelectedQuote);
            this.gbQuotes.Controls.Add(this.btnEditSelectedQuote);
            this.gbQuotes.Location = new System.Drawing.Point(5, 5);
            this.gbQuotes.Name = "gbQuotes";
            this.gbQuotes.Size = new System.Drawing.Size(444, 438);
            this.gbQuotes.TabIndex = 25;
            this.gbQuotes.TabStop = false;
            this.gbQuotes.Text = "Quotes";
            // 
            // lbQuotes
            // 
            this.lbQuotes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.lbQuotes.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbQuotes.FormattingEnabled = true;
            this.lbQuotes.ItemHeight = 19;
            this.lbQuotes.Location = new System.Drawing.Point(6, 21);
            this.lbQuotes.Name = "lbQuotes";
            this.lbQuotes.Size = new System.Drawing.Size(432, 365);
            this.lbQuotes.TabIndex = 24;
            this.lbQuotes.UseTabStops = false;
            this.lbQuotes.DoubleClick += new System.EventHandler(this.lbQuotes_DoubleClick);
            // 
            // btnDeleteSelectedQuote
            // 
            this.btnDeleteSelectedQuote.AutoSize = true;
            this.btnDeleteSelectedQuote.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnDeleteSelectedQuote.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnDeleteSelectedQuote.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnDeleteSelectedQuote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteSelectedQuote.Location = new System.Drawing.Point(318, 404);
            this.btnDeleteSelectedQuote.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnDeleteSelectedQuote.Name = "btnDeleteSelectedQuote";
            this.btnDeleteSelectedQuote.Size = new System.Drawing.Size(120, 31);
            this.btnDeleteSelectedQuote.TabIndex = 23;
            this.btnDeleteSelectedQuote.Text = "Löschen";
            this.btnDeleteSelectedQuote.UseVisualStyleBackColor = true;
            this.btnDeleteSelectedQuote.Click += new System.EventHandler(this.btnDeleteSelectedQuote_Click);
            // 
            // btnEditSelectedQuote
            // 
            this.btnEditSelectedQuote.AutoSize = true;
            this.btnEditSelectedQuote.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnEditSelectedQuote.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnEditSelectedQuote.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnEditSelectedQuote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditSelectedQuote.Location = new System.Drawing.Point(192, 404);
            this.btnEditSelectedQuote.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnEditSelectedQuote.Name = "btnEditSelectedQuote";
            this.btnEditSelectedQuote.Size = new System.Drawing.Size(120, 31);
            this.btnEditSelectedQuote.TabIndex = 22;
            this.btnEditSelectedQuote.Text = "Bearbeiten";
            this.btnEditSelectedQuote.UseVisualStyleBackColor = true;
            this.btnEditSelectedQuote.Click += new System.EventHandler(this.btnEditSelectedQuote_Click);
            // 
            // tpBet
            // 
            this.tpBet.Controls.Add(this.gbBetPast);
            this.tpBet.Controls.Add(this.gbBet);
            this.tpBet.Controls.Add(this.gbBetCurrent);
            this.tpBet.Location = new System.Drawing.Point(4, 25);
            this.tpBet.Name = "tpBet";
            this.tpBet.Size = new System.Drawing.Size(780, 449);
            this.tpBet.TabIndex = 3;
            this.tpBet.Text = "Wetten";
            // 
            // gbBetPast
            // 
            this.gbBetPast.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbBetPast.Controls.Add(this.lbRecentBets);
            this.gbBetPast.Location = new System.Drawing.Point(5, 235);
            this.gbBetPast.Name = "gbBetPast";
            this.gbBetPast.Size = new System.Drawing.Size(444, 207);
            this.gbBetPast.TabIndex = 30;
            this.gbBetPast.TabStop = false;
            this.gbBetPast.Text = "Vergangene Wetten";
            // 
            // lbRecentBets
            // 
            this.lbRecentBets.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.lbRecentBets.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbRecentBets.FormattingEnabled = true;
            this.lbRecentBets.ItemHeight = 19;
            this.lbRecentBets.Location = new System.Drawing.Point(6, 26);
            this.lbRecentBets.Name = "lbRecentBets";
            this.lbRecentBets.Size = new System.Drawing.Size(432, 175);
            this.lbRecentBets.TabIndex = 2;
            this.lbRecentBets.UseTabStops = false;
            this.lbRecentBets.DoubleClick += new System.EventHandler(this.lbRecentBets_DoubleClick);
            // 
            // gbBet
            // 
            this.gbBet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbBet.Controls.Add(this.cbBetWin);
            this.gbBet.Controls.Add(this.label1);
            this.gbBet.Controls.Add(this.tbBetSubbonusAmount);
            this.gbBet.Controls.Add(this.cbBetDurationUnit);
            this.gbBet.Controls.Add(this.tbBetDuration);
            this.gbBet.Controls.Add(this.chkbBetDuration);
            this.gbBet.Controls.Add(this.chkbBetSubbonus);
            this.gbBet.Controls.Add(this.btnBetDelete);
            this.gbBet.Controls.Add(this.chkbBetMinInput);
            this.gbBet.Controls.Add(this.tbBetMinInput);
            this.gbBet.Controls.Add(this.btnBetCreate);
            this.gbBet.Controls.Add(this.tbBetWin);
            this.gbBet.Controls.Add(this.lblBetWin);
            this.gbBet.Controls.Add(this.tbBetText);
            this.gbBet.Controls.Add(this.lblBetText);
            this.gbBet.Controls.Add(this.tbBetName);
            this.gbBet.Controls.Add(this.lblBetName);
            this.gbBet.Location = new System.Drawing.Point(455, 5);
            this.gbBet.Name = "gbBet";
            this.gbBet.Size = new System.Drawing.Size(283, 438);
            this.gbBet.TabIndex = 29;
            this.gbBet.TabStop = false;
            this.gbBet.Text = "Erstellen";
            // 
            // cbBetWin
            // 
            this.cbBetWin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.cbBetWin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbBetWin.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cbBetWin.FormattingEnabled = true;
            this.cbBetWin.Items.AddRange(new object[] {
            "Additiv",
            "Prozent"});
            this.cbBetWin.Location = new System.Drawing.Point(151, 196);
            this.cbBetWin.Name = "cbBetWin";
            this.cbBetWin.Size = new System.Drawing.Size(121, 27);
            this.cbBetWin.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(221, 286);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 19);
            this.label1.TabIndex = 48;
            this.label1.Text = "Prozent";
            // 
            // tbBetSubbonusAmount
            // 
            this.tbBetSubbonusAmount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbBetSubbonusAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbBetSubbonusAmount.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbBetSubbonusAmount.Location = new System.Drawing.Point(151, 281);
            this.tbBetSubbonusAmount.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbBetSubbonusAmount.Name = "tbBetSubbonusAmount";
            this.tbBetSubbonusAmount.Size = new System.Drawing.Size(64, 27);
            this.tbBetSubbonusAmount.TabIndex = 13;
            this.tbBetSubbonusAmount.TextChanged += new System.EventHandler(this.tbBetSubbonusAmount_TextChanged);
            // 
            // cbBetDurationUnit
            // 
            this.cbBetDurationUnit.AccessibleName = "";
            this.cbBetDurationUnit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.cbBetDurationUnit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbBetDurationUnit.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cbBetDurationUnit.FormattingEnabled = true;
            this.cbBetDurationUnit.Items.AddRange(new object[] {
            "Sekunden",
            "Minuten"});
            this.cbBetDurationUnit.Location = new System.Drawing.Point(151, 353);
            this.cbBetDurationUnit.Name = "cbBetDurationUnit";
            this.cbBetDurationUnit.Size = new System.Drawing.Size(121, 27);
            this.cbBetDurationUnit.TabIndex = 16;
            // 
            // tbBetDuration
            // 
            this.tbBetDuration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbBetDuration.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbBetDuration.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbBetDuration.Location = new System.Drawing.Point(81, 353);
            this.tbBetDuration.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbBetDuration.Name = "tbBetDuration";
            this.tbBetDuration.Size = new System.Drawing.Size(64, 27);
            this.tbBetDuration.TabIndex = 15;
            this.tbBetDuration.TextChanged += new System.EventHandler(this.tbBetDuration_TextChanged);
            // 
            // chkbBetDuration
            // 
            this.chkbBetDuration.AutoSize = true;
            this.chkbBetDuration.Location = new System.Drawing.Point(10, 325);
            this.chkbBetDuration.Name = "chkbBetDuration";
            this.chkbBetDuration.Size = new System.Drawing.Size(143, 23);
            this.chkbBetDuration.TabIndex = 14;
            this.chkbBetDuration.Text = "festgelegte Dauer ?";
            this.chkbBetDuration.UseVisualStyleBackColor = true;
            // 
            // chkbBetSubbonus
            // 
            this.chkbBetSubbonus.AutoSize = true;
            this.chkbBetSubbonus.Location = new System.Drawing.Point(10, 285);
            this.chkbBetSubbonus.Name = "chkbBetSubbonus";
            this.chkbBetSubbonus.Size = new System.Drawing.Size(145, 23);
            this.chkbBetSubbonus.TabIndex = 12;
            this.chkbBetSubbonus.Text = "Subscriberbonus ?";
            this.chkbBetSubbonus.UseVisualStyleBackColor = true;
            // 
            // btnBetDelete
            // 
            this.btnBetDelete.AutoSize = true;
            this.btnBetDelete.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnBetDelete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnBetDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnBetDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBetDelete.Location = new System.Drawing.Point(154, 404);
            this.btnBetDelete.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnBetDelete.Name = "btnBetDelete";
            this.btnBetDelete.Size = new System.Drawing.Size(120, 31);
            this.btnBetDelete.TabIndex = 18;
            this.btnBetDelete.Text = "Abbrechen";
            this.btnBetDelete.UseVisualStyleBackColor = true;
            this.btnBetDelete.Click += new System.EventHandler(this.btnBetCancel_Click);
            // 
            // chkbBetMinInput
            // 
            this.chkbBetMinInput.AutoSize = true;
            this.chkbBetMinInput.Location = new System.Drawing.Point(10, 242);
            this.chkbBetMinInput.Name = "chkbBetMinInput";
            this.chkbBetMinInput.Size = new System.Drawing.Size(138, 23);
            this.chkbBetMinInput.TabIndex = 10;
            this.chkbBetMinInput.Text = "Mindesteinsatz ?";
            this.chkbBetMinInput.UseVisualStyleBackColor = true;
            // 
            // tbBetMinInput
            // 
            this.tbBetMinInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbBetMinInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbBetMinInput.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbBetMinInput.Location = new System.Drawing.Point(151, 241);
            this.tbBetMinInput.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbBetMinInput.Name = "tbBetMinInput";
            this.tbBetMinInput.Size = new System.Drawing.Size(64, 27);
            this.tbBetMinInput.TabIndex = 11;
            this.tbBetMinInput.TextChanged += new System.EventHandler(this.tbBetMinInput_TextChanged);
            // 
            // btnBetCreate
            // 
            this.btnBetCreate.AutoSize = true;
            this.btnBetCreate.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnBetCreate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnBetCreate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnBetCreate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBetCreate.Location = new System.Drawing.Point(28, 404);
            this.btnBetCreate.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnBetCreate.Name = "btnBetCreate";
            this.btnBetCreate.Size = new System.Drawing.Size(120, 31);
            this.btnBetCreate.TabIndex = 17;
            this.btnBetCreate.Text = "Erstellen";
            this.btnBetCreate.UseVisualStyleBackColor = true;
            this.btnBetCreate.Click += new System.EventHandler(this.btnBetCreate_Click);
            // 
            // tbBetWin
            // 
            this.tbBetWin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbBetWin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbBetWin.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbBetWin.Location = new System.Drawing.Point(83, 196);
            this.tbBetWin.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbBetWin.Name = "tbBetWin";
            this.tbBetWin.Size = new System.Drawing.Size(62, 27);
            this.tbBetWin.TabIndex = 8;
            // 
            // lblBetWin
            // 
            this.lblBetWin.AutoSize = true;
            this.lblBetWin.Location = new System.Drawing.Point(6, 198);
            this.lblBetWin.Name = "lblBetWin";
            this.lblBetWin.Size = new System.Drawing.Size(60, 19);
            this.lblBetWin.TabIndex = 31;
            this.lblBetWin.Text = "Gewinn";
            // 
            // tbBetText
            // 
            this.tbBetText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbBetText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbBetText.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Italic);
            this.tbBetText.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbBetText.Location = new System.Drawing.Point(83, 71);
            this.tbBetText.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbBetText.Multiline = true;
            this.tbBetText.Name = "tbBetText";
            this.tbBetText.Size = new System.Drawing.Size(191, 105);
            this.tbBetText.TabIndex = 7;
            // 
            // lblBetText
            // 
            this.lblBetText.AutoSize = true;
            this.lblBetText.Location = new System.Drawing.Point(6, 73);
            this.lblBetText.Name = "lblBetText";
            this.lblBetText.Size = new System.Drawing.Size(42, 19);
            this.lblBetText.TabIndex = 29;
            this.lblBetText.Text = "Wette";
            // 
            // tbBetName
            // 
            this.tbBetName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbBetName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbBetName.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbBetName.Location = new System.Drawing.Point(83, 24);
            this.tbBetName.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbBetName.Name = "tbBetName";
            this.tbBetName.Size = new System.Drawing.Size(191, 27);
            this.tbBetName.TabIndex = 6;
            // 
            // lblBetName
            // 
            this.lblBetName.AutoSize = true;
            this.lblBetName.Location = new System.Drawing.Point(6, 26);
            this.lblBetName.Name = "lblBetName";
            this.lblBetName.Size = new System.Drawing.Size(44, 19);
            this.lblBetName.TabIndex = 27;
            this.lblBetName.Text = "Name";
            // 
            // gbBetCurrent
            // 
            this.gbBetCurrent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbBetCurrent.Controls.Add(this.tbBetCurrent);
            this.gbBetCurrent.Controls.Add(this.btnBetCurrentWon);
            this.gbBetCurrent.Controls.Add(this.btnBetCurrentEdit);
            this.gbBetCurrent.Controls.Add(this.btnBetCurrentStart);
            this.gbBetCurrent.Controls.Add(this.btnBetCurrentLost);
            this.gbBetCurrent.Controls.Add(this.btnBetCurrentStop);
            this.gbBetCurrent.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.gbBetCurrent.Location = new System.Drawing.Point(5, 5);
            this.gbBetCurrent.Name = "gbBetCurrent";
            this.gbBetCurrent.Size = new System.Drawing.Size(444, 225);
            this.gbBetCurrent.TabIndex = 28;
            this.gbBetCurrent.TabStop = false;
            this.gbBetCurrent.Text = "Aktuelle Wette";
            // 
            // tbBetCurrent
            // 
            this.tbBetCurrent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbBetCurrent.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbBetCurrent.Location = new System.Drawing.Point(6, 18);
            this.tbBetCurrent.Multiline = true;
            this.tbBetCurrent.Name = "tbBetCurrent";
            this.tbBetCurrent.ReadOnly = true;
            this.tbBetCurrent.Size = new System.Drawing.Size(306, 199);
            this.tbBetCurrent.TabIndex = 1;
            this.tbBetCurrent.TabStop = false;
            // 
            // btnBetCurrentWon
            // 
            this.btnBetCurrentWon.AutoSize = true;
            this.btnBetCurrentWon.Enabled = false;
            this.btnBetCurrentWon.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnBetCurrentWon.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnBetCurrentWon.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnBetCurrentWon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBetCurrentWon.Location = new System.Drawing.Point(318, 151);
            this.btnBetCurrentWon.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnBetCurrentWon.Name = "btnBetCurrentWon";
            this.btnBetCurrentWon.Size = new System.Drawing.Size(120, 31);
            this.btnBetCurrentWon.TabIndex = 4;
            this.btnBetCurrentWon.Text = "Gewonnen";
            this.btnBetCurrentWon.UseVisualStyleBackColor = true;
            this.btnBetCurrentWon.Visible = false;
            this.btnBetCurrentWon.Click += new System.EventHandler(this.btnBetCurrentWon_Click);
            // 
            // btnBetCurrentEdit
            // 
            this.btnBetCurrentEdit.AutoSize = true;
            this.btnBetCurrentEdit.Enabled = false;
            this.btnBetCurrentEdit.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnBetCurrentEdit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnBetCurrentEdit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnBetCurrentEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBetCurrentEdit.Location = new System.Drawing.Point(318, 100);
            this.btnBetCurrentEdit.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnBetCurrentEdit.Name = "btnBetCurrentEdit";
            this.btnBetCurrentEdit.Size = new System.Drawing.Size(120, 31);
            this.btnBetCurrentEdit.TabIndex = 3;
            this.btnBetCurrentEdit.Text = "Bearbeiten";
            this.btnBetCurrentEdit.UseVisualStyleBackColor = true;
            this.btnBetCurrentEdit.Click += new System.EventHandler(this.btnBetCurrentEdit_Click);
            // 
            // btnBetCurrentStart
            // 
            this.btnBetCurrentStart.AutoSize = true;
            this.btnBetCurrentStart.Enabled = false;
            this.btnBetCurrentStart.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnBetCurrentStart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnBetCurrentStart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnBetCurrentStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBetCurrentStart.Location = new System.Drawing.Point(318, 20);
            this.btnBetCurrentStart.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnBetCurrentStart.Name = "btnBetCurrentStart";
            this.btnBetCurrentStart.Size = new System.Drawing.Size(120, 31);
            this.btnBetCurrentStart.TabIndex = 1;
            this.btnBetCurrentStart.Text = "Starten";
            this.btnBetCurrentStart.UseVisualStyleBackColor = true;
            this.btnBetCurrentStart.Click += new System.EventHandler(this.btnBetCurrentStart_Click);
            // 
            // btnBetCurrentLost
            // 
            this.btnBetCurrentLost.AutoSize = true;
            this.btnBetCurrentLost.Enabled = false;
            this.btnBetCurrentLost.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnBetCurrentLost.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnBetCurrentLost.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnBetCurrentLost.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBetCurrentLost.Location = new System.Drawing.Point(318, 186);
            this.btnBetCurrentLost.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnBetCurrentLost.Name = "btnBetCurrentLost";
            this.btnBetCurrentLost.Size = new System.Drawing.Size(120, 31);
            this.btnBetCurrentLost.TabIndex = 5;
            this.btnBetCurrentLost.Text = "Verloren";
            this.btnBetCurrentLost.UseVisualStyleBackColor = true;
            this.btnBetCurrentLost.Visible = false;
            this.btnBetCurrentLost.Click += new System.EventHandler(this.btnBetCurrentLost_Click);
            // 
            // btnBetCurrentStop
            // 
            this.btnBetCurrentStop.AutoSize = true;
            this.btnBetCurrentStop.Enabled = false;
            this.btnBetCurrentStop.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnBetCurrentStop.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnBetCurrentStop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnBetCurrentStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBetCurrentStop.Location = new System.Drawing.Point(318, 60);
            this.btnBetCurrentStop.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnBetCurrentStop.Name = "btnBetCurrentStop";
            this.btnBetCurrentStop.Size = new System.Drawing.Size(120, 31);
            this.btnBetCurrentStop.TabIndex = 2;
            this.btnBetCurrentStop.Text = "Beenden";
            this.btnBetCurrentStop.UseVisualStyleBackColor = true;
            this.btnBetCurrentStop.Click += new System.EventHandler(this.btnBetCurrentStop_Click);
            // 
            // tpPoll
            // 
            this.tpPoll.Controls.Add(this.gbPollPrevious);
            this.tpPoll.Controls.Add(this.gbPoll);
            this.tpPoll.Controls.Add(this.gbPollCurrent);
            this.tpPoll.Location = new System.Drawing.Point(4, 25);
            this.tpPoll.Name = "tpPoll";
            this.tpPoll.Size = new System.Drawing.Size(780, 449);
            this.tpPoll.TabIndex = 4;
            this.tpPoll.Text = "Umfrage";
            // 
            // gbPollPrevious
            // 
            this.gbPollPrevious.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbPollPrevious.Controls.Add(this.lbRecentPolls);
            this.gbPollPrevious.Location = new System.Drawing.Point(5, 235);
            this.gbPollPrevious.Name = "gbPollPrevious";
            this.gbPollPrevious.Size = new System.Drawing.Size(444, 207);
            this.gbPollPrevious.TabIndex = 33;
            this.gbPollPrevious.TabStop = false;
            this.gbPollPrevious.Text = "Vorige Umfragen";
            // 
            // lbRecentPolls
            // 
            this.lbRecentPolls.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.lbRecentPolls.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbRecentPolls.FormattingEnabled = true;
            this.lbRecentPolls.ItemHeight = 19;
            this.lbRecentPolls.Location = new System.Drawing.Point(6, 26);
            this.lbRecentPolls.Name = "lbRecentPolls";
            this.lbRecentPolls.Size = new System.Drawing.Size(432, 175);
            this.lbRecentPolls.TabIndex = 3;
            this.lbRecentPolls.UseTabStops = false;
            this.lbRecentPolls.DoubleClick += new System.EventHandler(this.lbRecentPolls_DoubleClick);
            // 
            // gbPoll
            // 
            this.gbPoll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbPoll.Controls.Add(this.tbPollOption1);
            this.gbPoll.Controls.Add(this.lblPollOption1);
            this.gbPoll.Controls.Add(this.tbPollOption2);
            this.gbPoll.Controls.Add(this.lblPollOption2);
            this.gbPoll.Controls.Add(this.tbPollOption8);
            this.gbPoll.Controls.Add(this.tbPollOption6);
            this.gbPoll.Controls.Add(this.lblPollOption8);
            this.gbPoll.Controls.Add(this.lblPollOption6);
            this.gbPoll.Controls.Add(this.tbPollOption7);
            this.gbPoll.Controls.Add(this.lblPollOption7);
            this.gbPoll.Controls.Add(this.tbPollOption5);
            this.gbPoll.Controls.Add(this.lblPollOption5);
            this.gbPoll.Controls.Add(this.tbPollOption4);
            this.gbPoll.Controls.Add(this.lblPollOption4);
            this.gbPoll.Controls.Add(this.tbPollOption3);
            this.gbPoll.Controls.Add(this.lblPollOption3);
            this.gbPoll.Controls.Add(this.btnPollDelete);
            this.gbPoll.Controls.Add(this.btnPollCreate);
            this.gbPoll.Controls.Add(this.tbPollQuestion);
            this.gbPoll.Controls.Add(this.lblPollQuestion);
            this.gbPoll.Controls.Add(this.tbPollName);
            this.gbPoll.Controls.Add(this.lblPollName);
            this.gbPoll.Location = new System.Drawing.Point(455, 5);
            this.gbPoll.Name = "gbPoll";
            this.gbPoll.Size = new System.Drawing.Size(283, 438);
            this.gbPoll.TabIndex = 32;
            this.gbPoll.TabStop = false;
            this.gbPoll.Text = "Erstellen";
            // 
            // tbPollOption1
            // 
            this.tbPollOption1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbPollOption1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbPollOption1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbPollOption1.Location = new System.Drawing.Point(83, 139);
            this.tbPollOption1.Name = "tbPollOption1";
            this.tbPollOption1.Size = new System.Drawing.Size(191, 27);
            this.tbPollOption1.TabIndex = 52;
            // 
            // lblPollOption1
            // 
            this.lblPollOption1.AutoSize = true;
            this.lblPollOption1.Location = new System.Drawing.Point(6, 141);
            this.lblPollOption1.Name = "lblPollOption1";
            this.lblPollOption1.Size = new System.Drawing.Size(68, 19);
            this.lblPollOption1.TabIndex = 51;
            this.lblPollOption1.Text = "Option 1";
            // 
            // tbPollOption2
            // 
            this.tbPollOption2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbPollOption2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbPollOption2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbPollOption2.Location = new System.Drawing.Point(83, 172);
            this.tbPollOption2.Name = "tbPollOption2";
            this.tbPollOption2.Size = new System.Drawing.Size(191, 27);
            this.tbPollOption2.TabIndex = 52;
            // 
            // lblPollOption2
            // 
            this.lblPollOption2.AutoSize = true;
            this.lblPollOption2.Location = new System.Drawing.Point(6, 174);
            this.lblPollOption2.Name = "lblPollOption2";
            this.lblPollOption2.Size = new System.Drawing.Size(68, 19);
            this.lblPollOption2.TabIndex = 51;
            this.lblPollOption2.Text = "Option 2";
            // 
            // tbPollOption8
            // 
            this.tbPollOption8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbPollOption8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbPollOption8.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbPollOption8.Location = new System.Drawing.Point(83, 370);
            this.tbPollOption8.Name = "tbPollOption8";
            this.tbPollOption8.Size = new System.Drawing.Size(191, 27);
            this.tbPollOption8.TabIndex = 50;
            // 
            // tbPollOption6
            // 
            this.tbPollOption6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbPollOption6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbPollOption6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbPollOption6.Location = new System.Drawing.Point(83, 304);
            this.tbPollOption6.Name = "tbPollOption6";
            this.tbPollOption6.Size = new System.Drawing.Size(191, 27);
            this.tbPollOption6.TabIndex = 50;
            // 
            // lblPollOption8
            // 
            this.lblPollOption8.AutoSize = true;
            this.lblPollOption8.Location = new System.Drawing.Point(6, 372);
            this.lblPollOption8.Name = "lblPollOption8";
            this.lblPollOption8.Size = new System.Drawing.Size(68, 19);
            this.lblPollOption8.TabIndex = 49;
            this.lblPollOption8.Text = "Option 8";
            // 
            // lblPollOption6
            // 
            this.lblPollOption6.AutoSize = true;
            this.lblPollOption6.Location = new System.Drawing.Point(6, 306);
            this.lblPollOption6.Name = "lblPollOption6";
            this.lblPollOption6.Size = new System.Drawing.Size(68, 19);
            this.lblPollOption6.TabIndex = 49;
            this.lblPollOption6.Text = "Option 6";
            // 
            // tbPollOption7
            // 
            this.tbPollOption7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbPollOption7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbPollOption7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbPollOption7.Location = new System.Drawing.Point(83, 337);
            this.tbPollOption7.Name = "tbPollOption7";
            this.tbPollOption7.Size = new System.Drawing.Size(191, 27);
            this.tbPollOption7.TabIndex = 48;
            // 
            // lblPollOption7
            // 
            this.lblPollOption7.AutoSize = true;
            this.lblPollOption7.Location = new System.Drawing.Point(6, 339);
            this.lblPollOption7.Name = "lblPollOption7";
            this.lblPollOption7.Size = new System.Drawing.Size(68, 19);
            this.lblPollOption7.TabIndex = 47;
            this.lblPollOption7.Text = "Option 7";
            // 
            // tbPollOption5
            // 
            this.tbPollOption5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbPollOption5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbPollOption5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbPollOption5.Location = new System.Drawing.Point(83, 271);
            this.tbPollOption5.Name = "tbPollOption5";
            this.tbPollOption5.Size = new System.Drawing.Size(191, 27);
            this.tbPollOption5.TabIndex = 48;
            // 
            // lblPollOption5
            // 
            this.lblPollOption5.AutoSize = true;
            this.lblPollOption5.Location = new System.Drawing.Point(6, 273);
            this.lblPollOption5.Name = "lblPollOption5";
            this.lblPollOption5.Size = new System.Drawing.Size(68, 19);
            this.lblPollOption5.TabIndex = 47;
            this.lblPollOption5.Text = "Option 5";
            // 
            // tbPollOption4
            // 
            this.tbPollOption4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbPollOption4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbPollOption4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbPollOption4.Location = new System.Drawing.Point(83, 238);
            this.tbPollOption4.Name = "tbPollOption4";
            this.tbPollOption4.Size = new System.Drawing.Size(191, 27);
            this.tbPollOption4.TabIndex = 46;
            // 
            // lblPollOption4
            // 
            this.lblPollOption4.AutoSize = true;
            this.lblPollOption4.Location = new System.Drawing.Point(6, 240);
            this.lblPollOption4.Name = "lblPollOption4";
            this.lblPollOption4.Size = new System.Drawing.Size(68, 19);
            this.lblPollOption4.TabIndex = 45;
            this.lblPollOption4.Text = "Option 4";
            // 
            // tbPollOption3
            // 
            this.tbPollOption3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbPollOption3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbPollOption3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbPollOption3.Location = new System.Drawing.Point(83, 205);
            this.tbPollOption3.Name = "tbPollOption3";
            this.tbPollOption3.Size = new System.Drawing.Size(191, 27);
            this.tbPollOption3.TabIndex = 44;
            // 
            // lblPollOption3
            // 
            this.lblPollOption3.AutoSize = true;
            this.lblPollOption3.Location = new System.Drawing.Point(4, 207);
            this.lblPollOption3.Name = "lblPollOption3";
            this.lblPollOption3.Size = new System.Drawing.Size(68, 19);
            this.lblPollOption3.TabIndex = 43;
            this.lblPollOption3.Text = "Option 3";
            // 
            // btnPollDelete
            // 
            this.btnPollDelete.AutoSize = true;
            this.btnPollDelete.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnPollDelete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnPollDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnPollDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPollDelete.Location = new System.Drawing.Point(154, 404);
            this.btnPollDelete.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnPollDelete.Name = "btnPollDelete";
            this.btnPollDelete.Size = new System.Drawing.Size(120, 31);
            this.btnPollDelete.TabIndex = 42;
            this.btnPollDelete.Text = "Abbrechen";
            this.btnPollDelete.UseVisualStyleBackColor = true;
            this.btnPollDelete.Click += new System.EventHandler(this.btnPollDelete_Click);
            // 
            // btnPollCreate
            // 
            this.btnPollCreate.AutoSize = true;
            this.btnPollCreate.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnPollCreate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnPollCreate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnPollCreate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPollCreate.Location = new System.Drawing.Point(28, 404);
            this.btnPollCreate.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnPollCreate.Name = "btnPollCreate";
            this.btnPollCreate.Size = new System.Drawing.Size(120, 31);
            this.btnPollCreate.TabIndex = 36;
            this.btnPollCreate.Text = "Erstellen";
            this.btnPollCreate.UseVisualStyleBackColor = true;
            this.btnPollCreate.Click += new System.EventHandler(this.btnPollCreate_Click);
            // 
            // tbPollQuestion
            // 
            this.tbPollQuestion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbPollQuestion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbPollQuestion.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Italic);
            this.tbPollQuestion.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbPollQuestion.Location = new System.Drawing.Point(83, 64);
            this.tbPollQuestion.Multiline = true;
            this.tbPollQuestion.Name = "tbPollQuestion";
            this.tbPollQuestion.Size = new System.Drawing.Size(191, 68);
            this.tbPollQuestion.TabIndex = 30;
            // 
            // lblPollQuestion
            // 
            this.lblPollQuestion.AutoSize = true;
            this.lblPollQuestion.Location = new System.Drawing.Point(6, 62);
            this.lblPollQuestion.Name = "lblPollQuestion";
            this.lblPollQuestion.Size = new System.Drawing.Size(42, 19);
            this.lblPollQuestion.TabIndex = 29;
            this.lblPollQuestion.Text = "Frage";
            // 
            // tbPollName
            // 
            this.tbPollName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbPollName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbPollName.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbPollName.Location = new System.Drawing.Point(83, 24);
            this.tbPollName.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbPollName.Name = "tbPollName";
            this.tbPollName.Size = new System.Drawing.Size(191, 27);
            this.tbPollName.TabIndex = 28;
            // 
            // lblPollName
            // 
            this.lblPollName.AutoSize = true;
            this.lblPollName.Location = new System.Drawing.Point(6, 26);
            this.lblPollName.Name = "lblPollName";
            this.lblPollName.Size = new System.Drawing.Size(44, 19);
            this.lblPollName.TabIndex = 27;
            this.lblPollName.Text = "Name";
            // 
            // gbPollCurrent
            // 
            this.gbPollCurrent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbPollCurrent.Controls.Add(this.tbPollCurrent);
            this.gbPollCurrent.Controls.Add(this.btnPollCurrentEdit);
            this.gbPollCurrent.Controls.Add(this.btnPollCurrentStart);
            this.gbPollCurrent.Controls.Add(this.btnPollCurrentStop);
            this.gbPollCurrent.Location = new System.Drawing.Point(5, 5);
            this.gbPollCurrent.Name = "gbPollCurrent";
            this.gbPollCurrent.Size = new System.Drawing.Size(444, 225);
            this.gbPollCurrent.TabIndex = 31;
            this.gbPollCurrent.TabStop = false;
            this.gbPollCurrent.Text = "Aktuelle Umfrage";
            // 
            // tbPollCurrent
            // 
            this.tbPollCurrent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbPollCurrent.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbPollCurrent.Location = new System.Drawing.Point(6, 18);
            this.tbPollCurrent.Multiline = true;
            this.tbPollCurrent.Name = "tbPollCurrent";
            this.tbPollCurrent.Size = new System.Drawing.Size(306, 199);
            this.tbPollCurrent.TabIndex = 28;
            // 
            // btnPollCurrentEdit
            // 
            this.btnPollCurrentEdit.AutoSize = true;
            this.btnPollCurrentEdit.Enabled = false;
            this.btnPollCurrentEdit.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnPollCurrentEdit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnPollCurrentEdit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnPollCurrentEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPollCurrentEdit.Location = new System.Drawing.Point(318, 100);
            this.btnPollCurrentEdit.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnPollCurrentEdit.Name = "btnPollCurrentEdit";
            this.btnPollCurrentEdit.Size = new System.Drawing.Size(120, 31);
            this.btnPollCurrentEdit.TabIndex = 25;
            this.btnPollCurrentEdit.Text = "Bearbeiten";
            this.btnPollCurrentEdit.UseVisualStyleBackColor = true;
            this.btnPollCurrentEdit.Click += new System.EventHandler(this.btnPollCurrentEdit_Click);
            // 
            // btnPollCurrentStart
            // 
            this.btnPollCurrentStart.AutoSize = true;
            this.btnPollCurrentStart.Enabled = false;
            this.btnPollCurrentStart.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnPollCurrentStart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnPollCurrentStart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnPollCurrentStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPollCurrentStart.Location = new System.Drawing.Point(318, 20);
            this.btnPollCurrentStart.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnPollCurrentStart.Name = "btnPollCurrentStart";
            this.btnPollCurrentStart.Size = new System.Drawing.Size(120, 31);
            this.btnPollCurrentStart.TabIndex = 24;
            this.btnPollCurrentStart.Text = "Starten";
            this.btnPollCurrentStart.UseVisualStyleBackColor = true;
            this.btnPollCurrentStart.Click += new System.EventHandler(this.btnPollCurrentStart_Click);
            // 
            // btnPollCurrentStop
            // 
            this.btnPollCurrentStop.AutoSize = true;
            this.btnPollCurrentStop.Enabled = false;
            this.btnPollCurrentStop.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnPollCurrentStop.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnPollCurrentStop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnPollCurrentStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPollCurrentStop.Location = new System.Drawing.Point(318, 60);
            this.btnPollCurrentStop.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnPollCurrentStop.Name = "btnPollCurrentStop";
            this.btnPollCurrentStop.Size = new System.Drawing.Size(120, 31);
            this.btnPollCurrentStop.TabIndex = 22;
            this.btnPollCurrentStop.Text = "Beenden";
            this.btnPollCurrentStop.UseVisualStyleBackColor = true;
            this.btnPollCurrentStop.Click += new System.EventHandler(this.btnPollCurrentStop_Click);
            // 
            // tpGiveaway
            // 
            this.tpGiveaway.Controls.Add(this.gbGiveawayWinner);
            this.tpGiveaway.Controls.Add(this.gbGiveaway);
            this.tpGiveaway.Controls.Add(this.gbGiveawayCurrent);
            this.tpGiveaway.Location = new System.Drawing.Point(4, 25);
            this.tpGiveaway.Name = "tpGiveaway";
            this.tpGiveaway.Size = new System.Drawing.Size(780, 449);
            this.tpGiveaway.TabIndex = 5;
            this.tpGiveaway.Text = "Giveaway";
            // 
            // gbGiveawayWinner
            // 
            this.gbGiveawayWinner.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbGiveawayWinner.CausesValidation = false;
            this.gbGiveawayWinner.Controls.Add(this.cbWinnerSecondChance);
            this.gbGiveawayWinner.Controls.Add(this.lblsecondChance);
            this.gbGiveawayWinner.Controls.Add(this.chkdLbWinnerList);
            this.gbGiveawayWinner.Controls.Add(this.cbWinnerNewWinner);
            this.gbGiveawayWinner.Controls.Add(this.lblnewWinner);
            this.gbGiveawayWinner.Controls.Add(this.btnWinnerChooseNew);
            this.gbGiveawayWinner.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbGiveawayWinner.Location = new System.Drawing.Point(5, 235);
            this.gbGiveawayWinner.Name = "gbGiveawayWinner";
            this.gbGiveawayWinner.Size = new System.Drawing.Size(444, 207);
            this.gbGiveawayWinner.TabIndex = 33;
            this.gbGiveawayWinner.TabStop = false;
            this.gbGiveawayWinner.Text = "Gewinner";
            // 
            // cbWinnerSecondChance
            // 
            this.cbWinnerSecondChance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.cbWinnerSecondChance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbWinnerSecondChance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cbWinnerSecondChance.FormattingEnabled = true;
            this.cbWinnerSecondChance.Items.AddRange(new object[] {
            "Nein",
            "Ja"});
            this.cbWinnerSecondChance.Location = new System.Drawing.Point(234, 125);
            this.cbWinnerSecondChance.Name = "cbWinnerSecondChance";
            this.cbWinnerSecondChance.Size = new System.Drawing.Size(196, 27);
            this.cbWinnerSecondChance.TabIndex = 13;
            // 
            // lblsecondChance
            // 
            this.lblsecondChance.AutoSize = true;
            this.lblsecondChance.Location = new System.Drawing.Point(230, 103);
            this.lblsecondChance.Name = "lblsecondChance";
            this.lblsecondChance.Size = new System.Drawing.Size(210, 19);
            this.lblsecondChance.TabIndex = 50;
            this.lblsecondChance.Text = "Haben die eine zweite Chance?";
            // 
            // chkdLbWinnerList
            // 
            this.chkdLbWinnerList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.chkdLbWinnerList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.chkdLbWinnerList.ForeColor = System.Drawing.Color.RoyalBlue;
            this.chkdLbWinnerList.FormattingEnabled = true;
            this.chkdLbWinnerList.Items.AddRange(new object[] {
            "Gewinner 1"});
            this.chkdLbWinnerList.Location = new System.Drawing.Point(10, 25);
            this.chkdLbWinnerList.Name = "chkdLbWinnerList";
            this.chkdLbWinnerList.Size = new System.Drawing.Size(218, 176);
            this.chkdLbWinnerList.TabIndex = 11;
            // 
            // cbWinnerNewWinner
            // 
            this.cbWinnerNewWinner.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.cbWinnerNewWinner.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbWinnerNewWinner.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cbWinnerNewWinner.FormattingEnabled = true;
            this.cbWinnerNewWinner.Items.AddRange(new object[] {
            "Auswahl",
            "Alle"});
            this.cbWinnerNewWinner.Location = new System.Drawing.Point(234, 48);
            this.cbWinnerNewWinner.Name = "cbWinnerNewWinner";
            this.cbWinnerNewWinner.Size = new System.Drawing.Size(196, 27);
            this.cbWinnerNewWinner.TabIndex = 12;
            // 
            // lblnewWinner
            // 
            this.lblnewWinner.AutoSize = true;
            this.lblnewWinner.Location = new System.Drawing.Point(230, 26);
            this.lblnewWinner.Name = "lblnewWinner";
            this.lblnewWinner.Size = new System.Drawing.Size(208, 19);
            this.lblnewWinner.TabIndex = 47;
            this.lblnewWinner.Text = "Wer soll neu bestimmt werden?";
            // 
            // btnWinnerChooseNew
            // 
            this.btnWinnerChooseNew.AutoSize = true;
            this.btnWinnerChooseNew.Enabled = false;
            this.btnWinnerChooseNew.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnWinnerChooseNew.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnWinnerChooseNew.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnWinnerChooseNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWinnerChooseNew.Location = new System.Drawing.Point(318, 174);
            this.btnWinnerChooseNew.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnWinnerChooseNew.Name = "btnWinnerChooseNew";
            this.btnWinnerChooseNew.Size = new System.Drawing.Size(120, 31);
            this.btnWinnerChooseNew.TabIndex = 14;
            this.btnWinnerChooseNew.Text = "Neu bestimmen";
            this.btnWinnerChooseNew.UseVisualStyleBackColor = true;
            this.btnWinnerChooseNew.Click += new System.EventHandler(this.btnWinnerChooseNew_Click);
            // 
            // gbGiveaway
            // 
            this.gbGiveaway.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbGiveaway.Controls.Add(this.lblGiveawayMinInputCurrency);
            this.gbGiveaway.Controls.Add(this.tbGiveawayWin);
            this.gbGiveaway.Controls.Add(this.lblGiveawayWinners);
            this.gbGiveaway.Controls.Add(this.tbGiveawayKeyword);
            this.gbGiveaway.Controls.Add(this.lblGiveawayKeyword);
            this.gbGiveaway.Controls.Add(this.chkbGiveawaySubBonus);
            this.gbGiveaway.Controls.Add(this.btnGiveawayCreate);
            this.gbGiveaway.Controls.Add(this.chkbGiveawaMinInput);
            this.gbGiveaway.Controls.Add(this.tbGiveawayWinners);
            this.gbGiveaway.Controls.Add(this.tbGiveawayMinInput);
            this.gbGiveaway.Controls.Add(this.lblGiveawayText);
            this.gbGiveaway.Location = new System.Drawing.Point(455, 5);
            this.gbGiveaway.Name = "gbGiveaway";
            this.gbGiveaway.Size = new System.Drawing.Size(283, 438);
            this.gbGiveaway.TabIndex = 32;
            this.gbGiveaway.TabStop = false;
            this.gbGiveaway.Text = "Erstellen";
            // 
            // lblGiveawayMinInputCurrency
            // 
            this.lblGiveawayMinInputCurrency.AutoSize = true;
            this.lblGiveawayMinInputCurrency.Location = new System.Drawing.Point(164, 211);
            this.lblGiveawayMinInputCurrency.Name = "lblGiveawayMinInputCurrency";
            this.lblGiveawayMinInputCurrency.Size = new System.Drawing.Size(55, 19);
            this.lblGiveawayMinInputCurrency.TabIndex = 48;
            this.lblGiveawayMinInputCurrency.Text = "Punkte";
            // 
            // tbGiveawayWin
            // 
            this.tbGiveawayWin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbGiveawayWin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbGiveawayWin.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbGiveawayWin.Location = new System.Drawing.Point(83, 24);
            this.tbGiveawayWin.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbGiveawayWin.Multiline = true;
            this.tbGiveawayWin.Name = "tbGiveawayWin";
            this.tbGiveawayWin.Size = new System.Drawing.Size(191, 71);
            this.tbGiveawayWin.TabIndex = 4;
            // 
            // lblGiveawayWinners
            // 
            this.lblGiveawayWinners.AutoSize = true;
            this.lblGiveawayWinners.Location = new System.Drawing.Point(6, 164);
            this.lblGiveawayWinners.Name = "lblGiveawayWinners";
            this.lblGiveawayWinners.Size = new System.Drawing.Size(152, 19);
            this.lblGiveawayWinners.TabIndex = 46;
            this.lblGiveawayWinners.Text = "Anzahl der Gewinner";
            // 
            // tbGiveawayKeyword
            // 
            this.tbGiveawayKeyword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbGiveawayKeyword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbGiveawayKeyword.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbGiveawayKeyword.Location = new System.Drawing.Point(83, 115);
            this.tbGiveawayKeyword.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbGiveawayKeyword.Name = "tbGiveawayKeyword";
            this.tbGiveawayKeyword.Size = new System.Drawing.Size(191, 27);
            this.tbGiveawayKeyword.TabIndex = 5;
            // 
            // lblGiveawayKeyword
            // 
            this.lblGiveawayKeyword.AutoSize = true;
            this.lblGiveawayKeyword.Location = new System.Drawing.Point(6, 117);
            this.lblGiveawayKeyword.Name = "lblGiveawayKeyword";
            this.lblGiveawayKeyword.Size = new System.Drawing.Size(69, 19);
            this.lblGiveawayKeyword.TabIndex = 44;
            this.lblGiveawayKeyword.Text = "Codewort";
            // 
            // btnGiveawayCreate
            // 
            this.btnGiveawayCreate.AutoSize = true;
            this.btnGiveawayCreate.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnGiveawayCreate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnGiveawayCreate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnGiveawayCreate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGiveawayCreate.Location = new System.Drawing.Point(154, 404);
            this.btnGiveawayCreate.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnGiveawayCreate.Name = "btnGiveawayCreate";
            this.btnGiveawayCreate.Size = new System.Drawing.Size(120, 31);
            this.btnGiveawayCreate.TabIndex = 10;
            this.btnGiveawayCreate.Text = "Erstellen";
            this.btnGiveawayCreate.UseVisualStyleBackColor = true;
            this.btnGiveawayCreate.Click += new System.EventHandler(this.btnGiveawayCreate_Click);
            // 
            // chkbGiveawaMinInput
            // 
            this.chkbGiveawaMinInput.AutoSize = true;
            this.chkbGiveawaMinInput.Location = new System.Drawing.Point(10, 210);
            this.chkbGiveawaMinInput.Name = "chkbGiveawaMinInput";
            this.chkbGiveawaMinInput.Size = new System.Drawing.Size(89, 23);
            this.chkbGiveawaMinInput.TabIndex = 7;
            this.chkbGiveawaMinInput.Text = "Einsatz ?";
            this.chkbGiveawaMinInput.UseVisualStyleBackColor = true;
            // 
            // tbGiveawayWinners
            // 
            this.tbGiveawayWinners.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbGiveawayWinners.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbGiveawayWinners.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbGiveawayWinners.Location = new System.Drawing.Point(164, 162);
            this.tbGiveawayWinners.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbGiveawayWinners.Name = "tbGiveawayWinners";
            this.tbGiveawayWinners.Size = new System.Drawing.Size(40, 27);
            this.tbGiveawayWinners.TabIndex = 6;
            this.tbGiveawayWinners.Text = "1";
            this.tbGiveawayWinners.Leave += new System.EventHandler(this.tbGiveawayWinners_Leave);
            // 
            // tbGiveawayMinInput
            // 
            this.tbGiveawayMinInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbGiveawayMinInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbGiveawayMinInput.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbGiveawayMinInput.Location = new System.Drawing.Point(105, 209);
            this.tbGiveawayMinInput.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbGiveawayMinInput.Name = "tbGiveawayMinInput";
            this.tbGiveawayMinInput.Size = new System.Drawing.Size(40, 27);
            this.tbGiveawayMinInput.TabIndex = 8;
            this.tbGiveawayMinInput.Text = "0";
            this.tbGiveawayMinInput.TextChanged += new System.EventHandler(this.tbGiveawayMinInput_TextChanged);
            this.tbGiveawayMinInput.Leave += new System.EventHandler(this.tbGiveawayMinInput_Leave);
            // 
            // lblGiveawayText
            // 
            this.lblGiveawayText.AutoSize = true;
            this.lblGiveawayText.Location = new System.Drawing.Point(6, 26);
            this.lblGiveawayText.Name = "lblGiveawayText";
            this.lblGiveawayText.Size = new System.Drawing.Size(60, 19);
            this.lblGiveawayText.TabIndex = 29;
            this.lblGiveawayText.Text = "Gewinn";
            // 
            // gbGiveawayCurrent
            // 
            this.gbGiveawayCurrent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.gbGiveawayCurrent.Controls.Add(this.tbGiveawayCurrent);
            this.gbGiveawayCurrent.Controls.Add(this.btnGiveawayCurrentStart);
            this.gbGiveawayCurrent.Controls.Add(this.btnGiveawayCurrentReset);
            this.gbGiveawayCurrent.Controls.Add(this.btnGiveawayCurrentStop);
            this.gbGiveawayCurrent.Controls.Add(this.btnGiveawayCurrentDelete);
            this.gbGiveawayCurrent.Location = new System.Drawing.Point(5, 5);
            this.gbGiveawayCurrent.Name = "gbGiveawayCurrent";
            this.gbGiveawayCurrent.Size = new System.Drawing.Size(444, 225);
            this.gbGiveawayCurrent.TabIndex = 31;
            this.gbGiveawayCurrent.TabStop = false;
            this.gbGiveawayCurrent.Text = "Aktuelles Giveaway";
            // 
            // tbGiveawayCurrent
            // 
            this.tbGiveawayCurrent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbGiveawayCurrent.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbGiveawayCurrent.Location = new System.Drawing.Point(6, 19);
            this.tbGiveawayCurrent.Multiline = true;
            this.tbGiveawayCurrent.Name = "tbGiveawayCurrent";
            this.tbGiveawayCurrent.Size = new System.Drawing.Size(306, 199);
            this.tbGiveawayCurrent.TabIndex = 43;
            // 
            // btnGiveawayCurrentStart
            // 
            this.btnGiveawayCurrentStart.AutoSize = true;
            this.btnGiveawayCurrentStart.Enabled = false;
            this.btnGiveawayCurrentStart.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnGiveawayCurrentStart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnGiveawayCurrentStart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnGiveawayCurrentStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGiveawayCurrentStart.Location = new System.Drawing.Point(318, 20);
            this.btnGiveawayCurrentStart.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnGiveawayCurrentStart.Name = "btnGiveawayCurrentStart";
            this.btnGiveawayCurrentStart.Size = new System.Drawing.Size(120, 31);
            this.btnGiveawayCurrentStart.TabIndex = 0;
            this.btnGiveawayCurrentStart.Text = "Starten";
            this.btnGiveawayCurrentStart.UseVisualStyleBackColor = true;
            this.btnGiveawayCurrentStart.Click += new System.EventHandler(this.btnGiveawayCurrentStart_Click);
            // 
            // btnGiveawayCurrentReset
            // 
            this.btnGiveawayCurrentReset.AutoSize = true;
            this.btnGiveawayCurrentReset.Enabled = false;
            this.btnGiveawayCurrentReset.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnGiveawayCurrentReset.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnGiveawayCurrentReset.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnGiveawayCurrentReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGiveawayCurrentReset.Location = new System.Drawing.Point(318, 141);
            this.btnGiveawayCurrentReset.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnGiveawayCurrentReset.Name = "btnGiveawayCurrentReset";
            this.btnGiveawayCurrentReset.Size = new System.Drawing.Size(120, 31);
            this.btnGiveawayCurrentReset.TabIndex = 3;
            this.btnGiveawayCurrentReset.Text = "Zurücksetzen";
            this.btnGiveawayCurrentReset.UseVisualStyleBackColor = true;
            this.btnGiveawayCurrentReset.Click += new System.EventHandler(this.btnGiveawayCurrentReset_Click);
            // 
            // btnGiveawayCurrentStop
            // 
            this.btnGiveawayCurrentStop.AutoSize = true;
            this.btnGiveawayCurrentStop.Enabled = false;
            this.btnGiveawayCurrentStop.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnGiveawayCurrentStop.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnGiveawayCurrentStop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnGiveawayCurrentStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGiveawayCurrentStop.Location = new System.Drawing.Point(318, 60);
            this.btnGiveawayCurrentStop.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnGiveawayCurrentStop.Name = "btnGiveawayCurrentStop";
            this.btnGiveawayCurrentStop.Size = new System.Drawing.Size(120, 31);
            this.btnGiveawayCurrentStop.TabIndex = 1;
            this.btnGiveawayCurrentStop.Text = "Beenden";
            this.btnGiveawayCurrentStop.UseVisualStyleBackColor = true;
            this.btnGiveawayCurrentStop.Click += new System.EventHandler(this.btnGiveawayCurrentStop_Click);
            // 
            // btnGiveawayCurrentDelete
            // 
            this.btnGiveawayCurrentDelete.AutoSize = true;
            this.btnGiveawayCurrentDelete.Enabled = false;
            this.btnGiveawayCurrentDelete.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnGiveawayCurrentDelete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnGiveawayCurrentDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnGiveawayCurrentDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGiveawayCurrentDelete.Location = new System.Drawing.Point(318, 100);
            this.btnGiveawayCurrentDelete.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnGiveawayCurrentDelete.Name = "btnGiveawayCurrentDelete";
            this.btnGiveawayCurrentDelete.Size = new System.Drawing.Size(120, 31);
            this.btnGiveawayCurrentDelete.TabIndex = 2;
            this.btnGiveawayCurrentDelete.Text = "Löschen";
            this.btnGiveawayCurrentDelete.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.CancelButton = this.btnBSCancel;
            this.ClientSize = new System.Drawing.Size(800, 535);
            this.ControlBox = false;
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.btnMinimize);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblCreator);
            this.Controls.Add(this.lblMain);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmMain";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmMain_MouseDown);
            this.TabControl.ResumeLayout(false);
            this.tpSetup.ResumeLayout(false);
            this.gbStream.ResumeLayout(false);
            this.gbStream.PerformLayout();
            this.tpRewardsystem.ResumeLayout(false);
            this.gbViewerrankings.ResumeLayout(false);
            this.gbViewerrankings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvViewerankings)).EndInit();
            this.gbBasicSettings.ResumeLayout(false);
            this.gbBasicSettings.PerformLayout();
            this.tpChat.ResumeLayout(false);
            this.gbControl.ResumeLayout(false);
            this.gbControl.PerformLayout();
            this.gbChatDisplay.ResumeLayout(false);
            this.gbChatDisplay.PerformLayout();
            this.tpCommands.ResumeLayout(false);
            this.gbCreateCommand.ResumeLayout(false);
            this.gbCreateCommand.PerformLayout();
            this.gbCommandList.ResumeLayout(false);
            this.gbCommandList.PerformLayout();
            this.tpQuote.ResumeLayout(false);
            this.gbEditQuote.ResumeLayout(false);
            this.gbEditQuote.PerformLayout();
            this.gbCreateQuote.ResumeLayout(false);
            this.gbCreateQuote.PerformLayout();
            this.gbQuotes.ResumeLayout(false);
            this.gbQuotes.PerformLayout();
            this.tpBet.ResumeLayout(false);
            this.gbBetPast.ResumeLayout(false);
            this.gbBet.ResumeLayout(false);
            this.gbBet.PerformLayout();
            this.gbBetCurrent.ResumeLayout(false);
            this.gbBetCurrent.PerformLayout();
            this.tpPoll.ResumeLayout(false);
            this.gbPollPrevious.ResumeLayout(false);
            this.gbPoll.ResumeLayout(false);
            this.gbPoll.PerformLayout();
            this.gbPollCurrent.ResumeLayout(false);
            this.gbPollCurrent.PerformLayout();
            this.tpGiveaway.ResumeLayout(false);
            this.gbGiveawayWinner.ResumeLayout(false);
            this.gbGiveawayWinner.PerformLayout();
            this.gbGiveaway.ResumeLayout(false);
            this.gbGiveaway.PerformLayout();
            this.gbGiveawayCurrent.ResumeLayout(false);
            this.gbGiveawayCurrent.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

			}
		#endregion


		#region Main
		#endregion

		#region Tabpages
		#endregion

		#region Setup
		#endregion

		#region Rewardsystem
		#endregion

		#region Chat
		#endregion

		#region Commands
		#endregion

		#region Quotes
		
		#endregion

		#region Bet
		#endregion

		#region Poll
		#endregion

		#region Giveaway
		#endregion
		public Dotnetrix_Samples.tc TabControl;
		public TabPage tpSetup;
		public TabPage tpRewardsystem;
		public gb gbViewerrankings;
		public Button btnValueEdit;
		public Button btnRankDelete;
		public Button btnRankEdit;
		public Button btnRankAdd;
		public DataGridView dgvViewerankings;
		public DataGridViewTextBoxColumn clmnRank;
		public DataGridViewTextBoxColumn clmnPointsNeeded;
		public DataGridViewTextBoxColumn clmnCount;
		public gb gbBasicSettings;
		public ComboBox cbCurrencyUpdateUnit;
		public tb tbCurrencyCommandMessage;
		public Label lblCommandAnswer;
		public tb tbCurrencyCommand;
		public Label lblCurrencyCommand;
		public Label lblCurrency2;
		public Label lblCurrency;
		public Label lblCurrencyReward;
		public Label lblCurrencyUpdate;
		public Label lblCurrencyStart;
		public Label lblCurrencyName;
		public tb_int tbCurrencyIncrease;
		public tb_int tbCurrencyUpdate;
		public tb_int tbCurrencyStart;
		public tb tbCurrencyName;
		public TabPage tpChat;
		public TabPage tpCommands;
		public TabPage tpQuote;
		public TabPage tpBet;
		public TabPage tpPoll;
		public TabPage tpGiveaway;
		public Button btnMinimize;
		public Button btnClose;
		public Label lblCreator;
		public Label lblMain;
		public Button btnDgvSave;
		public Button btnDgvCancel;
		public Button btnBSSave;
		public Button btnBSCancel;
		public gb gbStream;
		public Button btnStreamSave;
		public Button btnStreamCancel;
		public tb tbInfo;
		public Label lblInfo;
		public LinkLabel llblOauthTokenHelp;
		public tb tbDBon;
		public tb tbDBoff;
		public tb tbToken;
		public tb tbBotchannel;
		public tb tbStreamchannel;
		public Label lblDatabaseWeb;
		public Label lblDatabaseFile;
		public Label lblOauthToken;
		public Label lblBotchannel;
		public Label lblStreamchannel;
		public gb gbControl;
		public Button btnUndefined;
		public Button btnSpam;
		public Button btnBlacklist;
		public Button btnViewerBan;
		public Button btnViewerTimeout;
		public Button btnViewerWarning;
		public Button btnDisconnect;
		public Button btnConnect;
		public gb gbChatDisplay;
		public Button btnMessageSend;
		public tb tbMessageSend;
		public gb gbCreateCommand;
		public Button btnCommandSave;
		public Label lblCreateCommandUserlvl;
		public tb_int tbCreateCommandCooldown;
		public Label lblCreateCommandCooldown;
		public tb tbCreateCommandMessage;
		public Label lblCreateCommandMessage;
		public tb tbCreateCommandName;
		public Label lblCreateCommandName;
		public gb gbCommandList;
		public Button btnDeleteSelectedCommand;
		public Button btnEditSelectedCommand;
		public gb gbEditQuote;
		public Button btnEditQuoteFinished;
		public tb tbEditQuoteText;
		public Label lblEditQuoteText;
		public gb gbCreateQuote;
		public Button btnCreateQuote;
		public tb tbCreateQuoteText;
		public Label lblCreateQuoteText;
		public gb gbQuotes;
		public Button btnDeleteSelectedQuote;
		public Button btnEditSelectedQuote;
		public gb gbBetPast;
		public gb gbBet;
		public CheckBox chkbBetSubbonus;
		public Button btnBetDelete;
		public CheckBox chkbBetMinInput;
		public tb_int tbBetMinInput;
		public Button btnBetCreate;
		public tb_int tbBetWin;
		public Label lblBetWin;
		public tb tbBetText;
		public Label lblBetText;
		public tb tbBetName;
		public Label lblBetName;
		public gb gbBetCurrent;
		public gb gbPollPrevious;
		public gb gbPoll;
		public tb tbPollOption1;
		public Label lblPollOption1;
		public tb tbPollOption2;
		public Label lblPollOption2;
		public tb tbPollOption8;
		public tb tbPollOption6;
		public Label lblPollOption8;
		public Label lblPollOption6;
		public tb tbPollOption7;
		public Label lblPollOption7;
		public tb tbPollOption5;
		public Label lblPollOption5;
		public tb tbPollOption4;
		public Label lblPollOption4;
		public tb tbPollOption3;
		public Label lblPollOption3;
		public Button btnPollDelete;
		public Button btnPollCreate;
		public tb tbPollQuestion;
		public Label lblPollQuestion;
		public tb tbPollName;
		public Label lblPollName;
		public gb gbPollCurrent;
		public Button btnPollCurrentEdit;
		public Button btnPollCurrentStart;
		public Button btnPollCurrentStop;
		public gb gbGiveawayWinner;
		public ComboBox cbWinnerSecondChance;
		public Label lblsecondChance;
		public CheckedListBox chkdLbWinnerList;
		public ComboBox cbWinnerNewWinner;
		public Label lblnewWinner;
		public Button btnWinnerChooseNew;
		public gb gbGiveaway;
		public Label lblGiveawayWinners;
		public tb tbGiveawayKeyword;
		public Label lblGiveawayKeyword;
		public CheckBox chkbGiveawaySubBonus;
		public Button btnGiveawayCreate;
		public CheckBox chkbGiveawaMinInput;
		public tb tbGiveawayWinners;
		public tb_int tbGiveawayMinInput;
		public Label lblGiveawayText;
		public gb gbGiveawayCurrent;
		public Button btnGiveawayCurrentStart;
		public Button btnGiveawayCurrentReset;
		public Button btnGiveawayCurrentStop;
		public Button btnGiveawayCurrentDelete;
		public ToolTip ttHelp;
		public Button btnCommandCancel;
		public tb tbCreateCommandAlias;
		public Label lblCreateCommandAlias;
		public CheckBox chkbCreateCommandEditable;
		public ComboBox cbCreateCommandUserlvl;
		public ComboBox cbCreateCommandUnit;
		public tb tbChat;
		public ListBox lbQuotes;
		public ComboBox cbBetDurationUnit;
		public tb_int tbBetDuration;
		public CheckBox chkbBetDuration;
		public tb_int tbBetSubbonusAmount;
		public Label label1;
		public Button btnBetCurrentWon;
		public Button btnBetCurrentEdit;
		public Button btnBetCurrentStart;
		public Button btnBetCurrentLost;
		public Button btnBetCurrentStop;
		public TextBox tbBetCurrent;
		public ListBox lbRecentBets;
		public TextBox tbPollCurrent;
		public ListBox lbRecentPolls;
		public tb tbGiveawayWin;
		public Label lblGiveawayMinInputCurrency;
		public TextBox tbGiveawayCurrent;
		public ListBox lbCommandlist;
		public ComboBox cbBetWin;
		public Button btnViewerGiveCurrency;
		public Button btnDgvSortOrder;
        public Button btnViewerUnban;
    }
	}