﻿namespace Sinister_Bot
{
    partial class frmPunishViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        public System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.lblViewer = new System.Windows.Forms.Label();
            this.lblDuration = new System.Windows.Forms.Label();
            this.btnPunish = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tbDuration = new Sinister_Bot.Custom_Controls.tb_int();
            this.tbViewer = new Sinister_Bot.Custom_Controls.tb();
            this.cbDurationUnit = new System.Windows.Forms.ComboBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblViewer
            // 
            this.lblViewer.AutoSize = true;
            this.lblViewer.Location = new System.Drawing.Point(14, 38);
            this.lblViewer.Name = "lblViewer";
            this.lblViewer.Size = new System.Drawing.Size(53, 19);
            this.lblViewer.TabIndex = 21;
            this.lblViewer.Text = "Viewer";
            // 
            // lblDuration
            // 
            this.lblDuration.AutoSize = true;
            this.lblDuration.Location = new System.Drawing.Point(14, 81);
            this.lblDuration.Name = "lblDuration";
            this.lblDuration.Size = new System.Drawing.Size(48, 19);
            this.lblDuration.TabIndex = 23;
            this.lblDuration.Text = "Dauer";
            // 
            // btnPunish
            // 
            this.btnPunish.AutoSize = true;
            this.btnPunish.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnPunish.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnPunish.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnPunish.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPunish.Location = new System.Drawing.Point(170, 121);
            this.btnPunish.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnPunish.Name = "btnPunish";
            this.btnPunish.Size = new System.Drawing.Size(146, 31);
            this.btnPunish.TabIndex = 26;
            this.btnPunish.Text = "Warnen";
            this.btnPunish.UseVisualStyleBackColor = true;
            this.btnPunish.Click += new System.EventHandler(this.btnPunish_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AutoSize = true;
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btnCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Location = new System.Drawing.Point(18, 121);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(146, 31);
            this.btnCancel.TabIndex = 25;
            this.btnCancel.Text = "Abbrechen";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tbDuration
            // 
            this.tbDuration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbDuration.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDuration.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbDuration.Location = new System.Drawing.Point(80, 79);
            this.tbDuration.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbDuration.Name = "tbDuration";
            this.tbDuration.Size = new System.Drawing.Size(109, 27);
            this.tbDuration.TabIndex = 24;
            this.tbDuration.TextChanged += new System.EventHandler(this.tbDuration_TextChanged);
            // 
            // tbViewer
            // 
            this.tbViewer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tbViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbViewer.ForeColor = System.Drawing.Color.RoyalBlue;
            this.tbViewer.Location = new System.Drawing.Point(80, 38);
            this.tbViewer.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.tbViewer.Name = "tbViewer";
            this.tbViewer.Size = new System.Drawing.Size(236, 27);
            this.tbViewer.TabIndex = 20;
            this.tbViewer.TextChanged += new System.EventHandler(this.tbViewer_TextChanged);
            this.tbViewer.Leave += new System.EventHandler(this.tbViewer_Leave);
            // 
            // cbDurationUnit
            // 
            this.cbDurationUnit.AccessibleName = "Aktualisierungseinheit";
            this.cbDurationUnit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.cbDurationUnit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbDurationUnit.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cbDurationUnit.FormattingEnabled = true;
            this.cbDurationUnit.Items.AddRange(new object[] {
            "Sekunden",
            "Minuten"});
            this.cbDurationUnit.Location = new System.Drawing.Point(195, 78);
            this.cbDurationUnit.Name = "cbDurationUnit";
            this.cbDurationUnit.Size = new System.Drawing.Size(121, 27);
            this.cbDurationUnit.TabIndex = 27;
            this.cbDurationUnit.SelectedIndexChanged += new System.EventHandler(this.cbDurationUnit_SelectedIndexChanged);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(137, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(66, 19);
            this.lblTitle.TabIndex = 28;
            this.lblTitle.Text = "Warnung";
            // 
            // frmPunishViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.ClientSize = new System.Drawing.Size(328, 165);
            this.ControlBox = false;
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.cbDurationUnit);
            this.Controls.Add(this.btnPunish);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.tbDuration);
            this.Controls.Add(this.lblDuration);
            this.Controls.Add(this.lblViewer);
            this.Controls.Add(this.tbViewer);
            this.Font = new System.Drawing.Font("Segoe Script", 9F);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmPunishViewer";
            this.Text = "frmRankEdit";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmRankEdit_MouseDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label lblViewer;
        public Custom_Controls.tb tbViewer;
        public System.Windows.Forms.Label lblDuration;
        public Custom_Controls.tb_int tbDuration;
        public System.Windows.Forms.Button btnPunish;
        public System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.ComboBox cbDurationUnit;
        public System.Windows.Forms.Label lblTitle;
    }
}