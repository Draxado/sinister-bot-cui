﻿using CurrencySystem;
using System;
using Twitch;
using Sinister_Bot;
using System.Text.RegularExpressions;

namespace Quotesystem
	{
	public class Quote
		{
		public string text;
		public string viewer;
		public string game;

		public Quote(string viewer, string text)
			{
			this.text = text;
			this.viewer = viewer;
			this.game = (StreamData.game == null)? "unkown game" : StreamData.game ;
			}

		public Quote(string viewer, string text, string game)
			{
			this.text = text;
			this.viewer = viewer;
			this.game = game;
			}

		public static void handleMessage(string content, Viewer sender)
			{
			if (Regex.IsMatch(content, @"^#\d") || Regex.IsMatch(content, @"^#r"))
				{
				sendQuote(content);
				}
			else if (content.Contains("#add") || content.Contains("#edit") || content.Contains("#del"))
				{
				editQuotes(content, sender);
				}
			}

		private static void sendQuote(string content)
			{
			string quoteNumber = content.Substring(content.IndexOf('#') + 1, 1);
			int number = 0;
			if (Bot.quote.quotes.Count > 0)
				{
				if (quoteNumber == "r")
					{
					Random n = new Random();
					number = n.Next() % Bot.quote.quotes.Count;
					}
				else
					{
					number = Convert.ToInt32(quoteNumber);
					}
				if (Bot.quote.quotes[number].text != "")
					{
					Bot.irc.sendChatMessage($"#{number}: {Bot.quote.quotes[number].text} ({Bot.quote.quotes[number].viewer},{Bot.quote.quotes[number].game})"); 
					}
                }
			}

		private static void editQuotes(string content, Viewer sender)
			{
			if (content.Contains("add"))
				{
				Bot.quote.addQuote(sender.name, content.Remove(0, 4).Trim(), StreamData.game);
				}
			else if (content.Contains("edit") && content.Length >= 9)
				{
				Bot.quote.editQuote(Convert.ToInt32(content[6]), content.Remove(0, 8));
				}
			else if (content.Contains("del"))
				{
				if (sender.status <= 1)
					{
					Bot.quote.deleteQuote(Convert.ToInt32(content.Split()[1]));
					}
				else
					{
					Bot.irc.sendChatMessage("Du darfst keine Quotes löschen");
					}
				}
			}
		}
	}