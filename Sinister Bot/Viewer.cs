﻿using System;
using System.Collections.Generic;
using MessageHandling;
using Sinister_Bot;
using Sinister_Bot.Properties;
using System.Threading.Tasks;
using System.Threading;

namespace CurrencySystem
{
    public class Viewer
    {
        public bool inChat = false;
        public int currency;
        public List<ChatMessage> messagesSended;
        public string name = string.Empty;
        public int status = 3; //0 = streamer, 1 = mod, 2 = subscriber, 3 = viewer
        public bool timeout = false;
        public string rank = "";
        public Task increaseCurrency;

        public Viewer(string name, int currency, string rank, int status)
        {
            this.name = name;
            if (name.Equals(Settings.Default.gbStream.Split('#')[0])) //streamer
            {
                this.status = 0;
            }
            else if (name.Equals(Settings.Default.gbStream.Split('#')[1])) //bot
            {
                this.status = 1;
            }
            else
            {
                this.status = status;
            }
            this.currency = currency;
            messagesSended = new List<ChatMessage>();
            if (rank != null)
            {
                this.rank = rank;
            }
            else
            {
                if (frmMain.dgvAscending)
                {
                    string temp = Bot.main.dgvViewerankings[Bot.main.dgvViewerankings.RowCount - 2, 0].Value.ToString();
                    this.rank = (Bot.main.dgvViewerankings[0, 0].Value.ToString() != "") ? Bot.main.dgvViewerankings[0, 0].Value.ToString() : "";
                    Bot.main.dgvViewerankings[2, 0].Value = Convert.ToInt32(Bot.main.dgvViewerankings[2, 0].Value) + 1;
                }
                else
                {
                    string temp = Bot.main.dgvViewerankings[Bot.main.dgvViewerankings.RowCount - 2, 0].Value.ToString();
                    this.rank = (Bot.main.dgvViewerankings[Bot.main.dgvViewerankings.RowCount - 2, 0].Value.ToString() != "") ? Bot.main.dgvViewerankings[Bot.main.dgvViewerankings.RowCount - 2, 0].Value.ToString() : "";
                    Bot.main.dgvViewerankings[Bot.main.dgvViewerankings.RowCount - 2, 2].Value = Convert.ToInt32(Bot.main.dgvViewerankings[Bot.main.dgvViewerankings.RowCount - 2, 2].Value) + 1;
                }
            }
            createTask();
        }

        private void createTask()
        {
            increaseCurrency = new Task(() =>
            {
                while (inChat)
                {
                    int sleeptime = Convert.ToInt32(Settings.Default.gbBasicSettings.Split('#')[5]) * 1000 * Convert.ToInt32((Settings.Default.gbBasicSettings.Split('#')[6] == "Sekunden") ? 1 : 60);
                    Thread.Sleep(sleeptime);
                    lock (this)
                    {
                        Bot.viewer.updateCurrency(this, Convert.ToInt32(Settings.Default.gbBasicSettings.Split('#')[4]));
                    }
                }
            }, Bot.tokenIrc);
        }
    }
}