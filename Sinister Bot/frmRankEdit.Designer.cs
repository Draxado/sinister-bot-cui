﻿namespace Sinister_Bot
	{
	partial class frmRankEdit
		{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		public System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
			{
			if (disposing && (components != null))
				{
				components.Dispose();
				}
			base.Dispose(disposing);
			}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		public void InitializeComponent()
			{
			this.lblRankName = new System.Windows.Forms.Label();
			this.lblPointsRequired = new System.Windows.Forms.Label();
			this.btnBSSave = new System.Windows.Forms.Button();
			this.btnBSCancel = new System.Windows.Forms.Button();
			this.tbPointsRequired = new Sinister_Bot.Custom_Controls.tb_int();
			this.tbRankName = new Sinister_Bot.Custom_Controls.tb();
			this.SuspendLayout();
			// 
			// lblRankName
			// 
			this.lblRankName.AutoSize = true;
			this.lblRankName.Location = new System.Drawing.Point(5, 11);
			this.lblRankName.Name = "lblRankName";
			this.lblRankName.Size = new System.Drawing.Size(42, 19);
			this.lblRankName.TabIndex = 21;
			this.lblRankName.Text = "Rang";
			// 
			// lblPointsRequired
			// 
			this.lblPointsRequired.AutoSize = true;
			this.lblPointsRequired.Location = new System.Drawing.Point(5, 52);
			this.lblPointsRequired.Name = "lblPointsRequired";
			this.lblPointsRequired.Size = new System.Drawing.Size(118, 19);
			this.lblPointsRequired.TabIndex = 23;
			this.lblPointsRequired.Text = "benötigte Punkte";
			// 
			// btnBSSave
			// 
			this.btnBSSave.AutoSize = true;
			this.btnBSSave.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
			this.btnBSSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
			this.btnBSSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
			this.btnBSSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBSSave.Location = new System.Drawing.Point(174, 92);
			this.btnBSSave.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
			this.btnBSSave.Name = "btnBSSave";
			this.btnBSSave.Size = new System.Drawing.Size(146, 31);
			this.btnBSSave.TabIndex = 26;
			this.btnBSSave.Text = "Speichern";
			this.btnBSSave.UseVisualStyleBackColor = true;
			this.btnBSSave.Click += new System.EventHandler(this.btnBSSave_Click);
			// 
			// btnBSCancel
			// 
			this.btnBSCancel.AutoSize = true;
			this.btnBSCancel.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
			this.btnBSCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
			this.btnBSCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
			this.btnBSCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBSCancel.Location = new System.Drawing.Point(22, 92);
			this.btnBSCancel.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
			this.btnBSCancel.Name = "btnBSCancel";
			this.btnBSCancel.Size = new System.Drawing.Size(146, 31);
			this.btnBSCancel.TabIndex = 25;
			this.btnBSCancel.Text = "Abbrechen";
			this.btnBSCancel.UseVisualStyleBackColor = true;
			this.btnBSCancel.Click += new System.EventHandler(this.btnBSCancel_Click);
			// 
			// tbPointsRequired
			// 
			this.tbPointsRequired.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
			this.tbPointsRequired.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPointsRequired.ForeColor = System.Drawing.Color.RoyalBlue;
			this.tbPointsRequired.Location = new System.Drawing.Point(129, 50);
			this.tbPointsRequired.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
			this.tbPointsRequired.Name = "tbPointsRequired";
			this.tbPointsRequired.Size = new System.Drawing.Size(191, 27);
			this.tbPointsRequired.TabIndex = 24;
			// 
			// tbRankName
			// 
			this.tbRankName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
			this.tbRankName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbRankName.ForeColor = System.Drawing.Color.RoyalBlue;
			this.tbRankName.Location = new System.Drawing.Point(129, 9);
			this.tbRankName.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
			this.tbRankName.Name = "tbRankName";
			this.tbRankName.Size = new System.Drawing.Size(191, 27);
			this.tbRankName.TabIndex = 20;
			// 
			// frmRankEdit
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
			this.ClientSize = new System.Drawing.Size(328, 135);
			this.ControlBox = false;
			this.Controls.Add(this.btnBSSave);
			this.Controls.Add(this.btnBSCancel);
			this.Controls.Add(this.tbPointsRequired);
			this.Controls.Add(this.lblPointsRequired);
			this.Controls.Add(this.lblRankName);
			this.Controls.Add(this.tbRankName);
			this.Font = new System.Drawing.Font("Segoe Script", 9F);
			this.ForeColor = System.Drawing.Color.RoyalBlue;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "frmRankEdit";
			this.Text = "frmRankEdit";
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmRankEdit_MouseDown);
			this.ResumeLayout(false);
			this.PerformLayout();

			}

		#endregion

		public System.Windows.Forms.Label lblRankName;
		public Custom_Controls.tb tbRankName;
		public System.Windows.Forms.Label lblPointsRequired;
		public Custom_Controls.tb_int tbPointsRequired;
		public System.Windows.Forms.Button btnBSSave;
		public System.Windows.Forms.Button btnBSCancel;
		}
	}