﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using CurrencySystem;
using Sinister_Bot.Custom_Controls;
using System.Data.SQLite;
using Sinister_Bot.Properties;

namespace Sinister_Bot
	{
	public partial class frmRankEdit : Form
		{
		private Color _defForeColor = Color.RoyalBlue;
		private Color _defBackColor = Color.FromArgb(28, 28, 28);

		public frmRankEdit(Button sender)
			{
			InitializeComponent();
			if (sender.Text.Contains("hinzufügen"))
				{
				btnBSSave.Text = "Hinzufügen";
				}
			else
				{
				if (Bot.main.dgvViewerankings.RowCount > 0)
					{
					btnBSSave.Text = "Ändern";
					tbRankName.Text = Bot.main.dgvViewerankings.SelectedRows[0].Cells[0].Value.ToString();
					tbPointsRequired.Text = Bot.main.dgvViewerankings.SelectedRows[0].Cells[1].Value.ToString();
					}
				}
			}

		private void btnBSCancel_Click(object sender, EventArgs e)
			{
			Dispose();
			}

		private void btnBSSave_Click(object sender, EventArgs e)
			{
			if (tbRankName.Text != "" && tbPointsRequired.Text != "")
				{
				if (lblRankName.Text == "Viewer") //frmGiveViewerPoints
					{
					foreach (Viewer v in Bot.viewer.list)
						{
						if (v.name == tbRankName.Text)
							{
							if (Bot.viewer != null)
								{
								Bot.viewer.updateCurrency(v, Convert.ToInt32(tbPointsRequired.Text));
								}
							else
								{
								updateRankOffline();
								}
							Dispose();
							return;
							}
						}
					MessageBox.Show("Dieser Viewer existiert nicht!");
					return;
					}
				else if (btnBSSave.Text == "Hinzufügen") //frmRankAdd
					{
					foreach (DataGridViewRow row in Bot.main.dgvViewerankings.Rows)
						{
						if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(tbPointsRequired.Text))
							{
							MessageBox.Show("Es gibt bereits einen Rang mit dieser Mindestpunktzahl! Bitt wähle eine andere.");
							return;
							}
						if (row.Cells[0].Value.ToString() == tbRankName.Text)
							{
							MessageBox.Show("Es gibt bereits einen Rang mit diesem Namen! Bitt wähle einen anderen.");
							return;
							}
						}
					Bot.main.dgvViewerankings.Rows.Add(tbRankName.Text, tbPointsRequired.Text, 0);
					Bot.main.dgvViewerankings.Update();
					Bot.main.sortDgvViewerrankings();
					frmMain.changedSettings = true;
					}
				else //frmRankEdit
					{
					foreach (DataGridViewRow rowPoints in Bot.main.dgvViewerankings.Rows)
						{
						if (Convert.ToInt32(rowPoints.Cells[1].Value) == Convert.ToInt32(tbPointsRequired.Text) && rowPoints != Bot.main.dgvViewerankings.SelectedRows[0])
							{
							MessageBox.Show("Es gibt bereits einen Rang mit dieser Mindestpunktzahl! Bitt wähle eine andere.");
							return;
							}
						}
					foreach (DataGridViewRow rowRank in Bot.main.dgvViewerankings.Rows)
						{
						if (rowRank.Cells[0].Value.ToString() == tbRankName.Text && rowRank != Bot.main.dgvViewerankings.SelectedRows[0])
							{
							MessageBox.Show("Es gibt bereits einen Rang mit diesem Namen! Bitt wähle einen anderen.");
							return;
							}
						}
					Bot.main.dgvViewerankings.SelectedRows[0].SetValues(tbRankName.Text, tbPointsRequired.Text, 0);
					Bot.main.dgvViewerankings.Update();
					}
				Bot.main.sortDgvViewerrankings();
				frmMain.changedSettings = true;
				Dispose();
				}
			else
				{
				MessageBox.Show("Bitte fülle erst alle Informationen aus!");
				}
			}

		private void updateRankOffline()
			{
			SQLiteConnection dbViewer = new SQLiteConnection("Data Source = " + ViewerDB.path);
			dbViewer.Open();
			string ncr = "select name, currency, rank from Viewerlist";
			SQLiteCommand cmd = new SQLiteCommand(ncr, dbViewer);
			SQLiteDataReader reader = cmd.ExecuteReader();
			while (reader.Read())
				{
				for (int i = 0; i < Bot.main.dgvViewerankings.Rows.Count; i++)
					{
					if (reader.GetInt32(1) >= Convert.ToInt32(Bot.main.dgvViewerankings.Rows[i].Cells[1].Value) && (i + 1 == Bot.main.dgvViewerankings.Rows.Count) ? true : reader.GetInt32(1) < Convert.ToInt32(Bot.main.dgvViewerankings.Rows[i + 1].Cells[1].Value)) //found rank
						{
						if (reader.GetString(2) != Bot.main.dgvViewerankings.Rows[i].Cells[0].Value.ToString()) //rank has changed -> increase count by 1 (new rank)
							{
							Bot.main.dgvViewerankings.Rows[i].Cells[2].Value = Convert.ToInt32(Bot.main.dgvViewerankings.Rows[i].Cells[2].Value) + 1;
							for (int j = 0; j < Bot.main.dgvViewerankings.Rows.Count; j++) //reduce count by 1 from the rank he was last in
								{
								if (Bot.main.dgvViewerankings.Rows[j].Cells[0].Value.ToString() == reader.GetString(2))
									{
									Bot.main.dgvViewerankings.Rows[j].Cells[2].Value = Convert.ToInt32(Bot.main.dgvViewerankings.Rows[j].Cells[2].Value) - 1;
									}
								}
							Bot.main.sortDgvViewerrankings();
							//change rank to new one
							string rankUpdate = "update Viewerlist set rank='" + Bot.main.dgvViewerankings.Rows[i].Cells[0].Value.ToString() + "' where rank='" + reader.GetString(2) + "'";
							SQLiteCommand cmdUpdate = new SQLiteCommand(rankUpdate, dbViewer);
							cmdUpdate.ExecuteNonQuery();
							dbViewer.Close();
							//save the changes in the settings
							Control.ControlCollection coll = Bot.main.tpRewardsystem.Controls;
							string bs = "";
							foreach (Control r in coll)
								{
								if (r is DataGridView)
									{
									DataGridView d = (DataGridView) r;
									int dCount = d.RowCount;
									for (int a = 0; a < dCount; a++)
										{
										bs += $"{d[0, a].Value}|{d[1, a].Value}|{d[2, a].Value}#";
										}
									bs = (bs.Length >= 1) ? bs.Substring(0, bs.Length - 1) : "";
									Settings.Default.gbViewerrankings = bs;
									Settings.Default.Save();
									if (!frmMain.alreadySaveViewerrankings)
										{
										frmMain.alreadySaveViewerrankings = true;
										}
									break;
									}
								}
							}
						else //rank hasn't changed
							{
							return;
							}
						}
					}
				}
			}

		#region design & control

		//////////////////////////////////////////// move frmMain /////////////////////////////////////////////
		public const int WM_NCLBUTTONDOWN = 0xA1;

		public const int HT_CAPTION = 0x2;

		[DllImportAttribute("user32.dll")]
		public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

		[DllImportAttribute("user32.dll")]
		public static extern bool ReleaseCapture();

		private void frmRankEdit_MouseDown(object sender, MouseEventArgs e)
			{
			if (e.Button == MouseButtons.Left)
				{
				ReleaseCapture();
				SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
				}
			}

		///////////////////////////////////////////// Draw Border /////////////////////////////////////////////
		protected override void OnPaint(PaintEventArgs e)
			{
			base.OnPaint(e);
			DrawControl(e.Graphics);
			ForeColor = _defForeColor;
			}

		private void DrawControl(Graphics graphics)
			{
			if (!Visible)
				{
				return;
				}

			Rectangle MainControlArea = ClientRectangle;
			Rectangle MainArea = DisplayRectangle;

			//fill background area frmMain
			Brush b = new SolidBrush(_defBackColor);
			graphics.FillRectangle(b, MainControlArea);
			b.Dispose();

			ControlPaint.DrawBorder(graphics, MainControlArea,
			_defForeColor, 2, ButtonBorderStyle.Solid,    //left
			_defForeColor, 2, ButtonBorderStyle.Solid,    //top
			_defForeColor, 2, ButtonBorderStyle.Solid,    //right
			_defForeColor, 2, ButtonBorderStyle.Solid);   //bottonm
			}

		#endregion design & control
		}
	}